package wiktionary;

import java.io.*;
import java.util.*;

import processing.GeneralUtils;

public class Wiktionary {
	
	String directory;
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		String directory = "";
		
		try {
			Properties prop = GeneralUtils.getProperties("config/config.properties");
			directory = prop.getProperty("wiktionary_directory");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		//String directory = "/proj/nlp/users/sara/corpora/sentiment";
		//String input = directory + "/wiktionary-full/wiktionary-10-30-10.tsv";
		//String output = directory + "/wiktionary/";
		
		//Wiktionary.createDatabase(input, output);
		Wiktionary wiktionary = new Wiktionary(directory);
		System.out.println(wiktionary.query("hello"));
		System.out.println("----------");
		System.out.println(wiktionary.query("lol").get(0).getFormsOf());
		System.out.println("----------");
		System.out.println(wiktionary.query("LOL").get(0).getFormsOf());
		System.out.println("----------");
		System.out.println(wiktionary.query("orgasmic"));
		System.out.println("----------");
		System.out.println(wiktionary.query("'s","Suffix"));
		System.out.println("----------");
		System.out.println(wiktionary.query("doesnt","Verb").get(0).getFormsOf());
		System.out.println("----------");
		System.out.println(wiktionary.query("tonite","Noun").get(0).getFormsOf());
	}
	
	/**
	 * Load wiktionary dictionary
	 * @param directory
	 */
	public Wiktionary(String directoryIn) {
		directory = directoryIn;
	}

	public static void createDatabase(String input, String output) {
		
		try {
        	BufferedReader in = new BufferedReader(new FileReader(input));
        	
        	String character = "_";
        	new File(output).mkdirs();
        	BufferedWriter out = new BufferedWriter(new FileWriter(output + "/" + character,true));
        	
            String inputLine;
            
            while ((inputLine = in.readLine()) != null) {
            	// language\tword\tpos\tdefinition
            	String [] data = inputLine.split("\t");
            	
            	String newChar = data[1].substring(0,1);
            	
            	if ((!character.equals("_") && !character.equals(newChar)) ||
            			(character.equals("_") && newChar.matches("[A-Za-z0-9]")))  {
            		character = newChar.matches("[A-Za-z0-9]") ? newChar : "_";
            		out.close();
            		out = new BufferedWriter(new FileWriter(output + "/" + character,true));
            	}
            	out.write(data[1] + "\t" + data[2] + "\t" + data[3] + "\n");
            }
            in.close();
            out.close();
    	} catch (Exception e) {
    		System.err.println("[Wiktionary.createDatabase] " + e);
    		e.printStackTrace();
    	}
	}
	
	public List<Definition> query(String term) {
		return query(term,false);
	}
	
	public List<Definition> query(String term, boolean ignoreCase) {
		return query(term,ignoreCase,null);
	}
	
	public List<Definition> query(String term, String POS) {
		return query(term,false,POS);
	}

	
	public List<Definition> query(String term, boolean ignoreCase, String POS) {
		
		List<Definition> definitions = new ArrayList<Definition>();
		
		try {
			String t = term.substring(0,1);
			String character = t.matches("[A-Za-z0-9]") ? t : "_";
        	BufferedReader in = new BufferedReader(new FileReader(directory + "/" + character));
        	
        	String inputLine;
            boolean done = false;
            //boolean found = false;
            
            while (!done && (inputLine = in.readLine()) != null) {
            	String[] data = inputLine.split("\t");
            	if ((data[0].equals(term) || (ignoreCase && data[0].equalsIgnoreCase(term)))
            			&& (POS == null || POS.equals(data[1]))) {
            		definitions.add(Definition.processDefinition(data));
            		//found = true;
            	}
            	else if (data[0].compareTo(term) >= 1) {
            		done = true;
            	}
            }
            in.close();
            
            if (!definitions.isEmpty() || !ignoreCase || !t.matches("[A-Za-z]")) return definitions;
            
            t = term.substring(0,1);
			character = t.matches("[A-Z]") ? t.toLowerCase() : t.toUpperCase();
        	in = new BufferedReader(new FileReader(directory + "/" + character));
        	
        	done = false;
            //found = false;
            
            while (!done && (inputLine = in.readLine()) != null) {
            	String[] data = inputLine.split("\t");
              	if ((data[0].equals(term) || (ignoreCase && data[0].equalsIgnoreCase(term)))
            			&& (POS == null || POS.equals(data[1]))) {
              		definitions.add(Definition.processDefinition(data));
            		//found = true;
            	}
            	else if (data[0].compareTo(term) >= 1) {
            		done = true;
            	}
            }
            in.close();
            
    	} catch (Exception e) {
    		System.err.println("[Wiktionary.query] " + e);
    		e.printStackTrace();
    	}

    	//	return query(term.matches("[A-Z]?.*") ? term.toLowerCase() : term.toUpperCase(),ignoreCase);
    	return definitions;
		
	}
}
