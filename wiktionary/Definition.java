/**
 * 
 */
package wiktionary;

import java.util.regex.*;
import java.util.*;
/**
 * @author sara
 *
 */
public class Definition {

	String term;
	String POS;
	String definition;
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	public Definition(String termIn, String POSin, String definitionIn) {
		term = termIn;
		POS = POSin.replaceAll("[\\{\\}]", "");
		definition = definitionIn.substring(1).trim();
	}
	
	public static Definition processDefinition(String tabDefinition) {
		String [] data = tabDefinition.split("\t");
		return new Definition(data[0],data[1],data[2]);
	}
	
	public static Definition processDefinition(String [] data) {
		return new Definition(data[0],data[1],data[2]);
	}
	
	public String getTerm() {
		return term;
	}
	
	public String getPOS() {
		return POS;
	}
	
	public String getDefinition() {
		return definition.replaceAll("\\{\\{[a-z]+\\|","(").
				replaceAll("\\{\\{","(").replaceAll(
				"\\}\\}", ")").replaceAll("[\\[\\]]", "").replaceAll("\\|",", ");
	}
	
	public String getOriginalDefinition() {
		return definition;
	}
	
	public String toString() {
		return term + " (" + POS + "): " + getDefinition();
	}
	
	public List<String> getFormsOf() {
		Pattern p = Pattern.compile("\\{\\{[a-z\\- ]+ of\\|(.*?)\\}\\}");
		
		Matcher m = p.matcher(definition);
		
		List<String> list = new ArrayList<String>();
		
		while (m.find()) {
			list.add(m.group(1).replaceAll("[\\[\\]]", ""));
		}
		
		return list;
	}
	
	public List<String> getRelatedTerms() {
		Pattern p = Pattern.compile("\\[\\[(.*?)\\]\\]");
		Matcher m = p.matcher(definition);
		
		List<String> list = new ArrayList<String>();
		
		while (m.find()) {
			list.add(m.group().substring(2,m.group().length()-2));
		}
		
		return list;
	}
	
	public HashMap<String,String> getCategories() {
		Pattern p = Pattern.compile("\\{\\{(.*?)\\}\\}");
		Matcher m = p.matcher(definition);
		
		HashMap<String,String> list = new HashMap<String,String>();
		
		while (m.find()) {
			String category = m.group().substring(2,m.group().length()-2);
			String categoryName = category.substring(0,category.indexOf("|"));
			String categoryValue = category.substring(category.indexOf("|")+1); 
			list.put(categoryName,categoryValue);
		}
		
		return list;
	}		
}
