package document;

import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;
import java.io.*;

public abstract class Document {

	protected Hashtable<String,String> attributes;
	protected Vector<Sentence> sentences;
	// seperate by sentences
	/*protected Hashtable entities;
	protected Hashtable times;
	protected Hashtable values;
	protected Hashtable events;
	protected Hashtable relations;*/
	protected String documentText;
	protected String documentId;
	protected String tag;
	protected File file;
	
	/**
	 * Create the document
	 * @param fileIn
	 * @param tagIn
	 */
	public Document(String docid, File fileIn, String tagIn) {
		file = fileIn;
		documentId = docid.trim();
		tag = tagIn;

		sentences = new Vector<Sentence>();
		attributes = new Hashtable<String,String>();
		
		this.setText(fileIn);
		attributes.put("sentences","sentences");
	}
	
	/**
	 * Create the document
	 * @param fileIn
	 * @param tagIn
	 */
	public Document(String docid, String fileIn, String tagIn) {
		this(docid, new File(fileIn.trim()),tagIn);
	}

	/**
	 * Read in file to store as-is
	 * 
	 * @param file
	 * @return
	 */
	private void setText(File fileIn) {
		documentText = "";
		
		try {
			int SIZE = 10 * 1024;
			BufferedInputStream f = new BufferedInputStream(
				    new FileInputStream( fileIn ) );
				byte[] barray = new byte[SIZE];
				
				while ( (f.read( barray, 0, SIZE )) != -1 ) {
					documentText += new String(barray);
					barray = new byte[SIZE];
				}
				
		} catch (Exception e) {
			System.err.println("Error occurred in Document.setText: " + e);
		}
		documentText = documentText.trim();
	}
	
	/**
	 * Return the text of the original document as read in ex: apf format
	 * 
	 * @return String
	 */
	public String getOriginalText() {
		return documentText;
	}
	
	/**
	 * get the document ID (required)
	 * @return String
	 */
	public String getDocumentId() {
		return documentId;
	}
	
	/**
	 * Print out the tag associated with the document. ex: RWC-X-ch.se.to.en
	 * @return String
	 */
	public String getTag() {
		return tag;
	}
	
	/**
	 * Print out the location or path of the file of the original document
	 * @return
	 */
	public String getPath() {
		return file.toString();
	}
	
	/**
	 * Print out the document
	 * 
	 * @return String
	 */
	public String toString() {
		String doc = "Document Id: " + documentId + "\n";
		for (int index = 0; index < sentences.size(); index ++) {
			doc += sentences.elementAt(index).toString() + "\n";
		}
		return doc;
	}
	
	/**
	 * get sentence based on position in sentences
	 * 
	 * @param index
	 * @return Sentence
	 */
	public Sentence getSentence(int index) {
		return sentences.elementAt(index);
	}
	
	/**
	 * get sentence using id, null if id does not exist
	 * 
	 * @param id
	 * @return Sentence
	 */
	public Sentence getSentenceById(String id) {
		// if id = position, makes it faster in these cases.
		if (Integer.parseInt(id) < sentences.size() && sentences.elementAt(Integer.parseInt(id)).getId().equals(id)) {
			return sentences.elementAt(Integer.parseInt(id));
		}
		
		for (int index = 0; index < sentences.size(); index++) {
			if (sentences.elementAt(index).getId().equals(id)) {
				return sentences.elementAt(index);
			}
		}
		
		return null;
	}
	
	/**
	 *  The number of sentences in the document
	 * @return int number of sentences
	 */
	public int getSize() {
		return sentences.size();
	}
	
	/**
	 * returns true if attribute exists
	 * 
	 * @param attribute
	 * @return boolean
	 */
	public boolean hasAttribute(String attribute) {
		return attributes.containsKey(attribute);
	}
	
	/**
	 * Create the document based on it's tag by calling the proper document sub-class
	 * Currently handles sgm, apf, inline, and ntg. 
	 * Contact ss3067 [at] columbia [dot] edu to get more versions added.
	 * 
	 * @param document
	 * @param tag docment tag ex: ENG-X-ch.se.to.en
	 * @see http://coral6.cs.columbia.edu:8080/galewiki/Year3/Tools/Pipeline/DocumentFormats
	 * @return Document processed document
	 */
	/*public static Document createDocument(String docid, String document, String tag) {
		Document d = null;
		
		if (tag.matches(".*-X-ch.se.to.en") && document.endsWith(".apf")) {
			d = new APFDocument(docid, document, tag);
		}
		else if (tag.matches(".*-X-se.en.in")) {
			d = new IAPFDocument(docid, document, tag);
		}
		else if ((tag.matches(".*-X-se$") || tag.matches(".*-X-se.ed$") || tag.matches(".*-X-se.to.cc$")) && document.endsWith(".sgm")) {
			d = new SGMDocument(docid, document, tag);
		}
		else if (tag.matches(".*-X-se.to.en") || tag.matches(".*-X-se.to.en")) {
			d = new NTGDocument(docid, document, tag);
		}
		else if (tag.matches(".*-T-se.pa.tb")) {
			d = new PennTreebankDocument(docid, document, tag);
		}
		else if (tag.matches(".*-X-se.pa")) {
			d = new POSTagsDocument(docid, document, tag);
		}
		if (d == null) {
			System.err.println("Error occured in createDocument, Invalid Document Format: " + tag);
		
			if (tag.matches(".*-X-se..*")) {
				d = new SGMDocument(docid, document, tag);
			}
		}
		
		return d;
	}*/
	
	/**
	 * Print out a hash as a string
	 */
	@SuppressWarnings("rawtypes")
	protected String hashToString(Hashtable ht,String name) {
		Iterator htIter = ht.keySet().iterator();
		String htString = "";
		
		while (htIter.hasNext()) {
			htString += name + ": [" + ht.get(htIter.next()) + "]\n";
		}
		
		return htString;
	}

	/**
	 * Get all the attributes of the given type in the document 
	 * @param attribute
	 * @return
	 */
	public abstract Hashtable<String,Attribute> getAttribute(String attribute);
	
	/**
	 * Get all the attribute mentions that match the given term. Attributes can include entities, time, values etc..
	 * 
	 * @param term term to find matches for
	 * @return
	 */
	public Hashtable<String,Attribute.Mention> getAttributeMatches(Word term) {
		Hashtable<String,Attribute.Mention> matches = new Hashtable<String,Attribute.Mention>();
		
		Iterator<String> it = attributes.keySet().iterator();
		
		while (it.hasNext()) {
			String attribute = (String)it.next();
			matches.putAll(getAttributeMatches(term,attribute));
		}
	
		return matches;
	}
	
	/**
	 * Get all the attributes that contain mentions in the given sentence for the given attribute.
	 * @param sentenceId id of sentence to find matches for
	 * @return
	 */
	public Hashtable<String,Attribute.Mention> getAttributeMatches(String sentenceId) {
		Sentence s = this.getSentenceById(sentenceId);
		Hashtable<String,Attribute.Mention> matches = new Hashtable<String, Attribute.Mention>();
		for (int index = 0; index < s.length(); index++) {
			matches.putAll(getAttributeMatches(s.getWord(index)));
		}
		return matches;
	}
	
	/**
	 * Get all the attribute mentions that match the given term for the given attribute.
	 * 
	 * @param term term to find matches for
	 * @param attribute get matches for this attribute only
	 * @return
	 */
	public Hashtable<String,Attribute.Mention> getAttributeMatches(Word term, String attribute) {
		Hashtable<String,Attribute.Mention> matches = new Hashtable<String,Attribute.Mention>();
		
		Hashtable<String,Attribute> items = getAttribute(attribute);
			
		if (items != null) {
			Iterator<String> attr = items.keySet().iterator();
			
			while (attr.hasNext()) {
				Object key = attr.next();
				Attribute item = (Attribute)items.get(key);
				if (item.matches(term)) {
					matches.putAll(item.getMentions());
				}			
			}
		}
		return matches;
	}
	
	/**
	 * Get all the attributes that contain mentions in the given sentence for the given attribute.
	 * @param sentenceId id of sentence to find matches for
	 * @param attribute get matches for this attribute only
	 * @return
	 */
	public Hashtable<String,Attribute.Mention> getAttributeMatches(String sentenceId, String attribute) {
		Sentence s = this.getSentenceById(sentenceId);
		Hashtable<String,Attribute.Mention> matches = new Hashtable<String, Attribute.Mention>();
		for (int index = 0; index < s.length(); index++) {
			matches.putAll(getAttributeMatches(s.getWord(index),attribute));
		}
		return matches;
	}
	
	/**
	 * Get all the attributes that contain mentions that match the given term for the given attribute.
	 * @param term term to find matches for
	 * @param attribute get matches for this attribute only
	 * @return
	 */
	public Hashtable<String,Attribute> getMatches(Word term, String attribute) {
		Hashtable<String,Attribute> matches = new Hashtable<String,Attribute>();
		
		Hashtable<String,Attribute> items = getAttribute(attribute);
			
		if (items != null) {
			Iterator<String> attr = items.keySet().iterator();
			
			while (attr.hasNext()) {
				Object key = attr.next();
				Attribute item = (Attribute)items.get(key);
				if (item.matches(term)) {
					matches.put(item.getId(), item);
				}			
			}
		}
		return matches;
	}
	
	/**
	 * Get all the attributes that contain mentions in the given sentence for the given attribute.
	 * @param sentenceId id of sentence to find matches for
	 * @param attribute get matches for this attribute only
	 * @return
	 */
	public Hashtable<String,Attribute> getMatches(String sentenceId, String attribute) {
		Sentence s = this.getSentenceById(sentenceId);
		Hashtable<String,Attribute> matches = new Hashtable<String, Attribute>();
		for (int index = 0; index < s.length(); index++) {
			matches.putAll(getMatches(s.getWord(index),attribute));
		}
		return matches;
	}

	/**
	 * Get all the attribute that contain mentions that match the given term. Attributes can include entities, time, values etc..
	 * 
	 * @param term
	 * @return
	 */
	public Hashtable<String,Attribute> getMatches(Word term) {
		Hashtable<String,Attribute> matches = new Hashtable<String,Attribute>();
		
		Iterator<String> it = attributes.keySet().iterator();
		
		while (it.hasNext()) {
			String attribute = (String)it.next();
			matches.putAll(getMatches(term,attribute));
		}
	
		return matches;
	}
	
	/**
	 * Get all the attributes that contain mentions in the given sentence for the given attribute.
	 * @param sentenceId id of sentence to find matches for
	 * @return
	 */
	public Hashtable<String,Attribute> getMatches(String sentenceId) {
		Sentence s = this.getSentenceById(sentenceId);
		Hashtable<String,Attribute> matches = new Hashtable<String, Attribute>();
		for (int index = 0; index < s.length(); index++) {
			matches.putAll(getMatches(s.getWord(index)));
		}
		return matches;
	}
	
	public static void main(String[] args) {
		//Document d = new Document("/proj/gale2/LDC/gale-y3-train/EnglishDistill/eng-ie/nw/AFP_ENG_20070201.0411.sgm.apf");
		//Document d = new Document("/proj/gale2/LDC/gale-y3-train/EnglishDistill/eng-ie/wt/eng-NG-31-100254-8819178_1.sgm.apf");
		
		//d.toString();

		//Document d = new Document("/proj/gale2/LDC/gale-y3-train/EnglishDistill/eng-ie/nw/AFP_ENG_20070103.0408.sgm.apf");
		//Document d = new Document("/proj/gale2/LDC/gale-y3-train/EnglishDistill/eng-ie/nw/AFP_ENG_20070103.0408.sgm.iapf");
		//Document d = Document.createDocument("CNN_LARRYKING_ENG_20070201_205800_02","/proj/gale2/LDC/gale-y3-train/task2/Dev/English/Training/eng-ccap/bc/CNN_LARRYKING_ENG_20070201_205800_02.sgm","ENG-X-se.to.cc");
		//System.out.println(((XMLDocument)d).toXML());
	}

}

