package document;

import java.util.Comparator;
import java.util.Hashtable;
import java.util.Iterator;

/**
 * This is the abstract class for any attributes a document can have. Examples: Entity, Time, Event, etc..
 * @author ss3067
 *
 */
public abstract class Attribute {

	protected String id;
	protected Hashtable<String,String> attributes;
	protected Hashtable<String,Mention> mentions;

	/**
	 * A Mention of the attribute
	 * @author ss3067
	 *
	 */
	public abstract class Mention {
		protected String id;
		protected Hashtable<String,String> attributes;
		
		/**
		 * 
		 *
		 */
		public Mention() {
			attributes = new Hashtable<String,String>();
		}
		
		/**
		 * mention id
		 * @return
		 */
		public String getId() {
			return id;
		}
		
		/**
		 * Get the mentions value for this attribute. It is not case sensitive.
		 * 
		 * @param attribute
		 * @return
		 */
		public String getAttribute(String attribute) {
			if (!attributes.containsKey(attribute.toLowerCase()) &&
					!attributes.containsKey(attribute.toUpperCase())) return null;
			if (attributes.get(attribute.toLowerCase()) != null) {
					return attributes.get(attribute.toLowerCase());
			}
			return attributes.get(attribute.toUpperCase());
		}
		
		/**
		 * True if the mention has this attribute. It is not case sensitive.
		 * @param attribute
		 * @return
		 */
		public boolean hasAttribute(String attribute) {
			if (attribute == null) return false;
			return attributes.containsKey(attribute.toLowerCase()) ||
			attributes.containsKey(attribute.toUpperCase());
		}
		
		/**
		 * Update the attribute
		 * @param attribute
		 * @param value
		 * @return
		 */
		public boolean updateAttribute(String attribute, String value) {
			if (attributes.containsKey(attribute.toLowerCase())) {
				attributes.put(attribute.toLowerCase(), value);
				return true;
			}
			else if (attributes.containsKey(attribute.toUpperCase())) {
				attributes.put(attribute.toUpperCase(), value);
				return true;
			}
			return false;
		}
		
		/**
		 * Return mention as String
		 */
		public abstract String toString();
		
		/**
		 * Return mention as XML
		 * @return
		 */
		public abstract String toXML();
		
		/**
		 * True if mention matches the term passed in
		 * @param term
		 * @return
		 */
		public abstract boolean matches(Word term);
	}
	
	/**
	 * 
	 *
	 */
	public Attribute() {
		attributes = new Hashtable<String,String>();
		mentions = new Hashtable<String,Mention>();
	}
	
	/**
	 * get the ID of the attribute
	 * @return
	 */
	public String getId() {
		return id;
	}
	
	/**
	 * Get the mention for given id
	 * @param id
	 * @return
	 */
	public Mention getMention(String id) {
		return mentions.get(id);
	}
	
	/**
	 * This is risky. Just returns first mention in the hash.
	 * @return
	 */
	public Mention getFirstMention() {
		if (mentions.isEmpty()) return null;
		return mentions.get(mentions.keySet().iterator().next());
	}
	
	/**
	 * True if this attribute contains some mention of the given term
	 * @param term
	 * @return
	 */
	public boolean containsMatchingMention(Word term) {
		Iterator<String> it = mentions.keySet().iterator();
		
		while (it.hasNext()) {
			Object key = it.next();
			Mention mention = mentions.get(key);
			if (mention.matches(term) == true) return true;
		}
		
		return false;
	}
	
	/**
	 * Returns the first mention that matches the given term
	 * @param term
	 * @return
	 */
	public Attribute.Mention getMatchingMention(Word term) {
		Iterator<String> it = mentions.keySet().iterator();
		
		while (it.hasNext()) {
			Object key = it.next();
			Mention mention = mentions.get(key);
			if (mention.matches(term) == true) return mention;
		}
		
		return null;
	}
	
	/**
	 * Get all the mentions
	 * @return
	 */
	public Hashtable<String,Attribute.Mention> getMentions() {
		return mentions;
	}
	
	/**
	 * Return all the attributes (xml attributes) for this attribute (document attribute) as a String
	 * @return
	 */
	protected String attributesToString() {
		String attributeString = "";
		
		Iterator<String> iterator = attributes.keySet().iterator();
		
		while (iterator.hasNext()) {
			Object key = iterator.next();
			attributeString += key + ": " + attributes.get(key) + " ";
		}
		return attributeString;
	}
	
	/**
	 * Return all the mentions as a String
	 * @return
	 */
	public String mentionsToString() {
		String mentionsString = "";
		Iterator<String> mentionsIter = mentions.keySet().iterator();
		
		while (mentionsIter.hasNext()) {
			mentionsString += mentions.get(mentionsIter.next());
		}
		
		return "mentions [" + mentionsString + "]";
	}
	
	/**
	 * True if the word matches some mention
	 * @param term
	 * @return
	 */
	public boolean matches(Word term) {
		Iterator<String> mentionsIter = mentions.keySet().iterator();
		
		while (mentionsIter.hasNext()) {
			Object key = mentionsIter.next();
			
			if (mentions.get(key).matches(term)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Get the attributes (document attribute) value for this attribute (xml attribute). It is not case sensitive.
	 * 
	 * @param attribute
	 * @return
	 */
	public String getAttribute(String attribute) {
		if (!hasAttribute(attribute)) return null;
		if (attributes.get(attribute.toLowerCase()) != null) {
				return attributes.get(attribute.toLowerCase());
		}
		return attributes.get(attribute.toUpperCase());
	}
	
	/**
	 * True if it has this attribute (xml attribute). It is not case sensitive.
	 * @param attribute
	 * @return
	 */	
	public boolean hasAttribute(String attribute) {
		if (attribute == null) return false;
		return attributes.containsKey(attribute.toLowerCase()) ||
		attributes.containsKey(attribute.toUpperCase());
	}
	
	/**
	 * returns the number of mentions
	 * @return
	 */
	public int mentionSize() {
		return mentions.size();
	}
	
	protected class IDComparator implements Comparator<Object>{
		public int compare(Object key1, Object key2){
			Integer k1 = Integer.parseInt(key1.toString().substring(key1.toString().lastIndexOf('-')+1).replaceAll("[^0-9]", ""));
			Integer k2 = Integer.parseInt(key2.toString().substring(key2.toString().lastIndexOf('-')+1).replaceAll("[^0-9]", ""));
			return k1.compareTo(k2);
		}
	}
	
	/**
	 * Returns attribute as String
	 */
	public abstract String toString();
	
	/**
	 * Returns attribute in XML format
	 * @return
	 */
	public abstract String toXML();
}
