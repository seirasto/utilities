package document;

import java.io.File;
import java.util.*;

public class StringDocument extends Document {
	protected static String sentence = "sentence";
	protected static String parse = "parse";

	/**
	 * @param documentIdIn
	 * @param file
	 * @param tag
	 */
	public StringDocument(String documentIdIn, File file, String tag) {
		super(documentIdIn, file, tag);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param docid
	 * @param file
	 * @param tag
	 */
	public StringDocument(String docid, String file, String tag) {
		super(docid, file, tag);
		// TODO Auto-generated constructor stub
	}
	
	public StringDocument(String docid, String file, String tag, String text) {
		super(docid,file,tag);
	}

	/**
	 * In this case there is one sentence per paragraph 
	 * because we can't safely split it more than that.
	 * Also, it is needed that way for n-gram's.
	 * @return documentId
	 */
	protected String processDocument(String document) {
		
		String [] s = document.split("\n");
	
		for (int index = 0; index < s.length; index++) {
			sentences.add(processSentence(s[index]));
		}
		return documentId;
	}
	
	protected Sentence processSentence(String sent) {
		Vector<Word> processedWords = new Vector<Word>();

		if (sent == null) {
			sent = ""; 
		}
		
		String [] words = sent.split(" ");
		
		for (int index = 0; index < words.length; index ++) {
			String [] word = words[index].split("/");
			if (word.length <= 1) {
				processedWords.add(new Word(word[0]));	
			}
			else {
				processedWords.add(new Word(word[0],null,word[1]));
			}
		}
		return new Sentence(processedWords,null);
	}

	public String toString() {
		String string = documentId + "\n";
		
		for (int index = 0; index < sentences.size(); index++) {
			string += sentences.elementAt(index) + "\n";
		}
		return string + "\n";
	}

	/* (non-Javadoc)
	 * @see gale.documents.Document#getAttribute(java.lang.String)
	 */
	@Override
	public Hashtable<String,Attribute> getAttribute(String attribute) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//POSTagsDocument d = new POSTagsDocument("APW_ENG_20070817.0133","/proj/gale2/LDC/gale-y3-train/task2/Dev/English/Training/eng-parsing/nw/APW_ENG_20070817.0133.tags","ENG-X-se.pa");
		StringDocument d = new StringDocument("eng-WL-11-99241-9833362-DS1","/proj/gale2/LDC/gale-y3-train/task2/Eval/English/eng-parsing/wt/eng-WL-11-99241-9833362-DS1.tags","ENG-X-se.pa");
		
		System.out.println(d);
	}

}
