package document;

import java.util.Iterator;
import java.util.Vector;
import java.util.Hashtable;
import java.util.regex.*;

/**
 * Sentence class referring to a sentence within a body of text and possibly it's parses, if available.
 * @author ss3067
 *
 */
public class Sentence {
	
	private Vector <Word> words;
	private String sentenceId;
	private String sourceSentenceId;
	private String startOffset;
	private String endOffset;
	// heirarchy of parses for this sentence
	private Parse parses;
	private String sentence;
	// just all the words in the sentence joined together, seperated by spaces.
	private String sentenceWord;
	private int charLength;
	private Hashtable<String,String> attributes;
	
	/**
	 * 
	 * @param sentenceIdIn the id for this sentence within the text
	 * @param sourceSentenceIdIn the source id, or id in the original version of this document,
	 *  for this sentence - for translated sentences.
	 * @param wordsIn words in the sentence
	 * @param startOffsetIn the position of the first character of the sentence in relation to the document
	 * @param endOffsetIn the position of the last character of the sentence in relation to the document
	 * @param parsesIn the parses for this sentence
	 */
	public Sentence(String sentenceIdIn, String sourceSentenceIdIn, Vector <Word> wordsIn, String startOffsetIn, String endOffsetIn, Parse parsesIn) {
		this(sentenceIdIn, sourceSentenceIdIn, wordsIn, startOffsetIn, endOffsetIn, parsesIn, null);
	}
	
	/**
	 * 
	 * @param sentenceIdIn the id for this sentence within the text
	 * @param sourceSentenceIdIn the source id, or id in the original version of this document,
	 *  for this sentence - for translated sentences.
	 * @param wordsIn words in the sentence
	 * @param startOffsetIn the position of the first character of the sentence in relation to the document
	 * @param endOffsetIn the position of the last character of the sentence in relation to the document
	 * @param parsesIn the parses for this sentence
	 * @param attributesIn attributes for this sentence
	 */
	public Sentence(String sentenceIdIn, String sourceSentenceIdIn, Vector <Word> wordsIn, String startOffsetIn, String endOffsetIn, Parse parsesIn,Hashtable<String,String> attributesIn) {
		try {
			sentenceId = sentenceIdIn;
			sourceSentenceId = sourceSentenceIdIn;
			words = wordsIn;
			startOffset = startOffsetIn;
			endOffset = endOffsetIn;
			parses = parsesIn;			
			sentenceWord = "";			
			sentence = "";
			attributes = attributesIn;
			
			// because sentence id's can only be #'s
			if (attributes != null) {
				if (getAttribute("sourcesentid") != null)
					setAttribute("sourcesentid", getAttribute("sourcesentid").replaceAll("[^0-9]", ""));
				if (getAttribute("id") != null)
					setAttribute("id", getAttribute("id").replaceAll("[^0-9]", ""));
			}
			
			if (sentenceId == null) sentenceId = getAttribute("id");
			if (sourceSentenceId == null) sourceSentenceId = getAttribute("sourcesentid");
			if (attributes == null) {
				attributes = new Hashtable<String,String>();
				if (sourceSentenceId != null) attributes.put("sourcesentid", sourceSentenceId);
				if (sentenceId != null) attributes.put("id", sentenceId);
			}
			
			// clean the sentences
			for (int index = 0; index < words.size(); index++) {
				
				// set the pos for each word
				Parse parse = getParse(words.get(index));
				if (parse != null && parse.getPartOfSpeech() != null) {
					words.get(index).setPOS(parse.getPartOfSpeech());
				}
				
				if (sentence.matches(".*[-\\(\\[\\$@]$")) {
					sentence += words.get(index).getToken();
				}
				else if (words.get(index).getToken().matches("[A-Za-z0-9\\(\\) ]*")) {
					sentence += " " + words.get(index).getToken();
				} else if (words.get(index).getToken().matches("\\p{Punct}|'s|n't")) {
					sentence += words.get(index).getToken();
				} else {
					sentence += " " + words.get(index).getToken();
				}
				sentenceWord += (words.get(index).getToken() != null ? words.get(index).getToken() + " " : "");
				charLength += words.get(index).length() + 1;
			}
			charLength -= 1;
			sentenceWord = sentenceWord.trim();
			sentence = sentence.trim();
		}
		catch (Exception e){
			System.err.println("Error occurred in Sentence Constructor: " + e);
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 * @param sentenceIdIn the id for this sentence within the text
	 * @param wordsIn words in the sentence
	 * @param startOffsetIn the position of the first character of the sentence in relation to the document
	 * @param endOffsetIn the position of the last character of the sentence in relation to the document
	 * @param parsesIn the parses for this sentence	 */
	public Sentence(String sentenceIdIn, Vector <Word> wordsIn, String startOffsetIn, String endOffsetIn, Parse parsesIn) {
		this(sentenceIdIn,null,wordsIn,startOffsetIn,endOffsetIn,parsesIn);
	}
	
	/**
	 * 
	 * @param sentenceIdIn the id for this sentence within the text
	 * @param wordsIn words in the sentence
	 * @param startOffsetIn the position of the first character of the sentence in relation to the document
	 * @param endOffsetIn the position of the last character of the sentence in relation to the document
	 * @param parsesIn the parses for this sentence
	 * @param attributesIn attributes for this sentence
	 */
	public Sentence(String sentenceIdIn, Vector <Word> wordsIn, String startOffsetIn, String endOffsetIn, Parse parsesIn, Hashtable<String,String> attributesIn) {
		this(sentenceIdIn,null,wordsIn,startOffsetIn,endOffsetIn,parsesIn,attributesIn);
	}
	
	/**
	 * 
	 * @param sentenceIdIn the id for this sentence within the text
	 * @param sourceSentenceIdIn the source id, or id in the original version of this document,
	 *  for this sentence - for translated sentences.
	 * @param wordsIn words in the sentence
	 * @param startOffsetIn the position of the first character of the sentence in relation to the document
	 * @param endOffsetIn the position of the last character of the sentence in relation to the document
	 */
	public Sentence(String sentenceIdIn, String sourceSentenceId, Vector <Word> wordsIn, int startOffsetIn, int endOffsetIn) {
		this(sentenceIdIn,sourceSentenceId,wordsIn,Integer.valueOf(startOffsetIn).toString(),Integer.valueOf(endOffsetIn).toString(),null);
	}
	
	/**
	 * 
	 * @param sentenceIdIn the id for this sentence within the text
	 * @param sourceSentenceIdIn the source id, or id in the original version of this document,
	 *  for this sentence - for translated sentences.
	 * @param wordsIn words in the sentence
	 * @param startOffsetIn the position of the first character of the sentence in relation to the document
	 * @param endOffsetIn the position of the last character of the sentence in relation to the document
	 * @param attributesIn attributes for this sentence
	 */
	public Sentence(String sentenceIdIn, String sourceSentenceId, Vector <Word> wordsIn, int startOffsetIn, int endOffsetIn, Hashtable<String,String> attributesIn) {
		this(sentenceIdIn,sourceSentenceId,wordsIn,Integer.valueOf(startOffsetIn).toString(),Integer.valueOf(endOffsetIn).toString(),null,attributesIn);
	}
	
	/**
	 * 
	 * @param sentenceIdIn the id for this sentence within the text
	 * @param wordsIn words in the sentence
	 */
	public Sentence(String sentenceIdIn, Vector <Word> wordsIn) {
		this(sentenceIdIn,null,wordsIn,wordsIn.isEmpty() ? "" : Integer.valueOf(wordsIn.firstElement().getStartOffset()).toString(),wordsIn.isEmpty() ? "" : Integer.valueOf(wordsIn.lastElement().getEndOffset()).toString(),null);
	}
	
	/**
	 * 
	 * @param sentenceIdIn the id for this sentence within the text
	 * @param wordsIn words in the sentence
	 * @param parsesIn the parses for this sentence
	 */
	public Sentence(String sentenceIdIn, Vector <Word> wordsIn, Parse parsesIn) {
		this(sentenceIdIn,wordsIn,wordsIn.isEmpty() ? "" : Integer.valueOf(wordsIn.firstElement().getStartOffset()).toString(),wordsIn.isEmpty() ? "" : Integer.valueOf(wordsIn.lastElement().getEndOffset()).toString(),parsesIn);
	}
	
	/**
	 * 
	 * @param sentenceIdIn the id for this sentence within the text
	 * @param wordsIn words in the sentence
	 * @param parsesIn the parses for this sentence
	 * @param attributesIn the attributes for this sentence
	 */
	public Sentence(String sentenceIdIn, Vector <Word> wordsIn, Parse parsesIn, Hashtable<String,String> attributesIn) {
		this(sentenceIdIn,wordsIn,wordsIn.isEmpty() ? "" : Integer.valueOf(wordsIn.firstElement().getStartOffset()).toString(),wordsIn.isEmpty() ? "" : Integer.valueOf(wordsIn.lastElement().getEndOffset()).toString(),parsesIn,attributesIn);
	}
	
	/**
	 * 
	 * @param sentenceIdIn the id for this sentence within the text
	 * @param sourceSentenceIdIn the source id, or id in the original version of this document,
	 *  for this sentence - for translated sentences.
	 * @param wordsIn words in the sentence
	 */
	public Sentence(String sentenceIdIn, String sourceSentenceIdIn, Vector <Word> wordsIn) {
		this(sentenceIdIn,sourceSentenceIdIn,wordsIn,wordsIn.isEmpty() ? "" : Integer.valueOf(wordsIn.firstElement().getStartOffset()).toString(),wordsIn.isEmpty() ? "" : Integer.valueOf(wordsIn.lastElement().getEndOffset()).toString(),null);
	}
	
	/**
	 * 
	 * @param sentenceIdIn the id for this sentence within the text
	 * @param sourceSentenceIdIn the source id, or id in the original version of this document,
	 *  for this sentence - for translated sentences.
	 * @param attributesIn the attributes for this sentence
	 */
	public Sentence(String sentenceIdIn, String sourceSentenceIdIn, Vector <Word> wordsIn, Hashtable<String,String> attributesIn) {
		this(sentenceIdIn,sourceSentenceIdIn,wordsIn,wordsIn.isEmpty() ? "" : Integer.valueOf(wordsIn.firstElement().getStartOffset()).toString(),wordsIn.isEmpty() ? "" : Integer.valueOf(wordsIn.lastElement().getEndOffset()).toString(),null,attributesIn);
	}
	
	/**
	 * 
	 * @param wordsIn words in the sentence
	 * @param attributesIn the attributes for this sentence
	 */
	public Sentence(Vector <Word> wordsIn, Hashtable<String,String> attributesIn) {
		this(null,null,wordsIn,wordsIn.isEmpty() ? "" : Integer.valueOf(wordsIn.firstElement().getStartOffset()).toString(),wordsIn.isEmpty() ? "" : Integer.valueOf(wordsIn.lastElement().getEndOffset()).toString(),null,attributesIn);
	}
	
	/**
	 * 
	 * @param sentenceIdIn the id for this sentence within the text
	 * @param sourceSentenceIdIn the source id, or id in the original version of this document,
	 * @param wordsIn words in the sentence
	 * @param parsesIn the parses for this sentence
	 * @param attributesIn the attributes for this sentence
	 */
	public Sentence(String sentenceIdIn, String sourceSentenceIdIn, Vector <Word> wordsIn, Parse parsesIn, Hashtable<String,String> attributesIn) {
		this(sentenceIdIn,
				sourceSentenceIdIn,
				wordsIn,
				wordsIn.isEmpty() ? "" : Integer.valueOf(wordsIn.firstElement().getStartOffset()).toString(),
				wordsIn.isEmpty() ? "" : Integer.valueOf(wordsIn.lastElement().getEndOffset()).toString(),parsesIn,attributesIn);
	}
	
	/**
	 * 
	 * @param sentenceIdIn the id for this sentence within the text
	 * @param sourceSentenceIdIn the source id, or id in the original version of this document,
	 * @param wordsIn words in the sentence
	 * @param parsesIn the parses for this sentence
	 */
	public Sentence(String sentenceIdIn, String sourceSentenceIdIn, Vector <Word> wordsIn, Parse parsesIn) {
		this(sentenceIdIn,sourceSentenceIdIn,wordsIn,parsesIn,null);
	}
	
	/**
	 * 
	 * @param sentenceIdIn the id for this sentence within the text
	 * @param sourceSentenceIdIn the source id, or id in the original version of this document,
	 * @param sentenceIn the complete sentence
	 */
	public Sentence(String sentenceIdIn, String sourceSentenceIdIn, String sentenceIn) {
		sentenceId = sentenceIdIn;
		sourceSentenceId = sourceSentenceIdIn;
		sentence = sentenceIn;
	}
	
	/**
	 * 
	 * @param sentenceIdIn the id for this sentence within the text
	 * @param sentenceIn the complete sentence
	 */
	public Sentence(String sentenceIdIn, String sentenceIn) {
		this(sentenceIdIn,null,sentenceIn);
	}
	
	/**
	 * get the sentence
	 * @return String sentence
	 */
	public String getSentence() {
		return sentence;
	}
	
	/**
	 * Get the word at position index
	 * @param index
	 * @return Word
	 */
	public Word getWord(int index) {
		return words.get(index);
	}
	
	/**
	 * Get the sentence based on the words. This will be partitioned better than the original sentence. It will not have extra spaces
	 * @return
	 */
	public String getWordString() {
		return sentenceWord;
	}
	
	/**
	 * Get word that has the given id
	 * @param id
	 * @return Word
	 */
	public Word getWordById(int id) {
		if (!words.get(0).hasId()) return null;
		
		for (int index = 0; index < words.size(); index++) {
			if (words.get(index).hasId() && words.get(index).getId().equals(id)) {
				return words.get(index);
			}
		}
		return null;
	}
	
	/**
	 * Get the id for the sentence. sourcesentenceid is returned for a translation since that is the accurate id.
	 * @return
	 */
	public String getId() {
		if (sourceSentenceId != null && !sourceSentenceId.trim().equals("")) return sourceSentenceId.replaceAll("T.|SENT-|S.", ""); 
		return sentenceId.replaceAll("[^0-9]", "");
	}
	
	/**
	 * True if the sentence has offsets
	 * @return
	 */
	public boolean hasOffsets() {
		return startOffset == "";
	}
	
	/**
	 * get the starting offset - position of first character in the sentence
	 * @return
	 */
	public String getStartOffset() {
		return startOffset;
	}
	
	/**
	 * get the ending offset - position of last character in the sentence
	 * @return
	 */
	public String getEndOffset() {
		return endOffset;
	}
	
	/**
	 * return the sentence as a string - including all information 
	 * @return String
	 */
	public String toString() {
		String wordString = ""; 

		for (int index=0; words != null && index < words.size(); index++) {
			wordString += words.elementAt(index).toString() + " "; 
		}
		
		if (wordString == "") {
			wordString = sentence;
		}
		
		return (sentenceId + " (" + startOffset + "," + endOffset + "): " + wordString + (parses != null ? "\n" + parses.toString() : "")).trim();
	}
	
	/**
	 * Returns sentence in XML format. A sentence consists of words and parses. 
	 * This is different depending on the tag given.
	 * @param tag version of sentence xml to return
	 * @return String
	 */
	public String toXML(String tag) {
		String xml = "";
		
		if (tag.equals("X-se.en.in") || tag.matches(".*-X-se.en.in")) {
			xml = "<sentence ";
			Iterator<String> attrIter = attributes.keySet().iterator();
			
			while (attrIter.hasNext()) {
				Object key = attrIter.next();
				xml += key + "=\"" + attributes.get(key) + "\" ";
			}
			xml += ">";
			
			String text = "";
			
			for (int index=0; words != null && index < words.size(); index++) {
				text += words.elementAt(index).getToken() + " "; 
			}
			xml += web.HTML.convertToValidHtml(text.replaceAll("\\s", " "));
			xml += "</sentence>\n";
		}
		else if (tag.equals("X-ch.se.to.en") || tag.matches(".*-X-ch.se.to.en$")) {
			xml = "<sentence ";
			Iterator<String> attrIter = attributes.keySet().iterator();
			
			while (attrIter.hasNext()) {
				Object key = attrIter.next();
				xml += key + "=\"" + attributes.get(key) + "\" ";
			}
			xml = xml.trim() + ">\n";
			
			xml += "  <charseq START=\"" + startOffset + "\" END=\"" + endOffset + "\"></charseq>\n  ";
	
			for (int index=0; words != null && index < words.size(); index++) {
				xml += words.elementAt(index).toXML() + " "; 
			}		
		
			xml += parses != null ? "<parse>\n" + parses.toXML() + "</parse>\n" : "";
			
			xml += "</sentence>\n";			
		}
		else if (tag.equals("X-se.pa") || tag.matches(".*-X-se.pa$")) {
			xml = "<sentence ";
			
			Iterator<String> attrIter = attributes.keySet().iterator();
			
			while (attrIter.hasNext()) {
				Object key = attrIter.next();
				xml += key + "=\"" + attributes.get(key) + "\" ";
			}
			xml = xml.trim() + ">";

			for (int index=0; words != null && index < words.size(); index++) {
				xml += words.elementAt(index).getToken() + "/" + words.elementAt(index).getPOS() + " "; 
			}
			
			xml = xml.trim() + "</sentence>\n";
		}
		else if (tag.matches("X-.*|.*X-.*")) { // if (tag.equals("X-se") || tag.equals("X-se.ed") || tag.equals("X-se.to.cc")) {
			xml = "<sentence ";
			
			Iterator<String> attrIter = attributes.keySet().iterator();
			
			while (attrIter.hasNext()) {
				Object key = attrIter.next();
				xml += key + "=\"" + attributes.get(key) + "\" ";
			}
			xml += ">";
			xml += this.sentence.replaceAll("\\s", " ");			
			xml += "</sentence>\n";
		}
		return xml;
	}
	
	/**
	 * Get the words in the sentence from start up to end
	 * @param start
	 * @param end
	 * @return
	 */
	public Sentence subSentence(int start,int end) {
		//System.err.println(start + "-" + end);
		if (start < 0) {
			System.err.println("Error occurred in Sentence.subSentence: Index must be >= 0, " + start + " < 0");
			return null;
		}
		else if (end > words.size()) {
			System.err.println("Error occurred in Sentence.subSentence: Exceeded size of sentence, " + words.size() + "<" + end );
			return null;
		}
		else if (end < start) { 
			System.err.println("Error occurred in Sentence.subSentence: end must be greater than start.");
			return null;
		}
		
		Vector<Word> wordSubset = new Vector<Word>();
		
		if (start == 0 && end == words.size()) {
			return this;
		}
		
		for (int index = start; index < end; index++) {
				wordSubset.add(words.elementAt(index));
		}
		
		return new Sentence(this.getId() + "-" + words.elementAt(start).getId() + "-" + words.elementAt(end-1).getId(), wordSubset,
				Integer.valueOf(wordSubset.elementAt(0).getStartOffset()).toString(), Integer.valueOf(wordSubset.elementAt(wordSubset.size()-1).getEndOffset()).toString(),
				parses != null ? parses.getParse(wordSubset) : null);
	}
	
	/**
	 * Get the position of the current phrase in the sentence. This is the position based on word position
	 * @param phrase
	 * @return
	 */
	public int tokenIndexOf(String phrase) {
		try {	
			String thisPhrase = this.documentVersion(phrase);
			
			if (thisPhrase == null) return -1;
			int	index = sentenceWord.indexOf(thisPhrase);

			if (index < 0) return -1;
			if (index == 0) return 0;
		
			String [] before = sentenceWord.substring(0, index).split(" ");
			
			if (before == null) return -1;
			
			int size = 0;
			int i = 0;
			
			for (i = 0; size < before.length; i++) {
				size += words.elementAt(i).wordSize();
			}
			
			return i;
		}
		catch (Exception e) {
			System.err.println("Error occurred in Sentence.wordIndexOf: " + e);
			e.printStackTrace();
		}
		return -1;
	}
	
	/**
	 * Get the position of the current phrase in the sentence. This is the position based on characters
	 * @param phrase
	 * @return
	 */
	public int wordIndexOf(String phrase) {
		try {	
			String thisPhrase = this.documentVersion(phrase);
			
			if (thisPhrase == null) return -1;
			int	index = sentenceWord.indexOf(thisPhrase + " ") == 0 ? 0 : sentenceWord.indexOf(" " + thisPhrase) ;

			if (index < 0) return -1;
			if (index == 0) return 0;
		
			String [] before = sentenceWord.substring(0, index).split(" ");
			
			return before != null ? before.length : -1;
		}
		catch (Exception e) {
			System.err.println("Error occurred in Sentence.wordIndexOf: " + e);
			e.printStackTrace();
		}
		return -1;
	}
	
	/**
	 * Get the version of the sentence that we have. For example, it may include more/less punctuation.
	 * @param phrase
	 * @return
	 */
	public String documentVersion(String phrase) {
		String thisPhrase = phrase.toLowerCase().replaceAll("\\s+"," ").replaceAll("[\\p{Punct} ]+", "[\\\\p{Punct} ]+");

		//System.err.println("REGEX: " + thisPhrase);
		Pattern p = Pattern.compile(thisPhrase,Pattern.CASE_INSENSITIVE);
		Matcher m = p.matcher(sentenceWord);
		
		if (m.find()) {
			//System.err.println("MATCH: " + m.group());
			return m.group();
		}
		return null;
	}
	
	/**
	 * Return the phrase as a Sentence. This will give us character offsets and parses if available.
	 * @param phrase
	 * @return
	 */
	public Sentence align(String phrase) {
	
		phrase = phrase.trim();
		
		if (phrase.replaceAll("[^A-Za-z ]", "").replaceAll("\\s+"," ").trim().equalsIgnoreCase(sentence.replaceAll("[^A-Za-z ]", "").replaceAll("\\s+"," ").trim())) {
			return this;
		}
		
		int start  = this.wordIndexOf(phrase);
		String dPhrase = this.documentVersion(phrase);

		int end = (dPhrase != null ? dPhrase.split(" ").length : -1);

		if (start == -1 || end == -1) return null;
		
		end += (start == 0 ? start : start); 
		
		return this.subSentence(start, end);
	}
	
	/**
	 * Get the number of words in the sentence
	 * @return
	 */
	public int length() {
		return words.size();
	}
	
	/**
	 * Get the number of characters in the sentence
	 * @return
	 */
	public int charLength() {
		return charLength;
	}
	
	/**
	 * Get the parse that the word is in.
	 * @param word
	 * @return
	 */
	public Parse getParse(Word word) {
		try {
			if (parses == null)	return null;
			return parses.getParse(word);
		} catch (Exception e) {
			System.err.println("Error occurred in Sentence.getParse: " + e);
		}
		return null;
	}
	
	/**
	 * Return the parse for this sentence.
	 * @return
	 */
	public Parse getParse() {
		return parses;
	}
	
	/**
	 * Get the value of the attribute. It is not case sensitive.
	 * 
	 * @param attribute
	 * @return
	 */
	public String getAttribute(String attribute) {
		if (attribute == null || !hasAttribute(attribute)) return null;
		if (attributes.get(attribute.toLowerCase()) != null) {
				return attributes.get(attribute.toLowerCase());
		}
		return attributes.get(attribute.toUpperCase());
	}
	
	/**
	 * Change the value of the attribute or add the attribute if it does not exist
	 * @param attribute
	 * @param value
	 */
	public void setAttribute(String attribute, String value) {
		if (attribute == null) return;
		if (attributes.containsKey(attribute) 
				&& !attributes.containsKey(attribute.toLowerCase())
				&& !attributes.containsKey(attribute.toUpperCase())) {
			attributes.put(attribute,value);
		}
		else if (attributes.containsKey(attribute.toLowerCase())) {
			attributes.put(attribute.toLowerCase(),value);
		}
		else if (attributes.containsKey(attribute.toUpperCase())) {
			attributes.put(attribute.toUpperCase(),value);
		}
		else {
			attributes.put(attribute.toLowerCase(),value);
		}
	}
	
	/**
	 * Check if the attribute exists. It is not case sensitive.
	 * @param attribute
	 * @return
	 */
	public boolean hasAttribute(String attribute) {
		if (attributes == null) return false;
		return attributes.containsKey(attribute.toLowerCase()) ||
		attributes.containsKey(attribute.toUpperCase());
	}
	
	/**
	 * Get an iterator pointing to the attributes of the sentence.
	 * @return
	 */
	public Iterator<String> getAttributes() {
		return attributes.keySet().iterator();
	}
}