/**
 * 
 */
package document;

import java.io.ByteArrayInputStream;
import java.io.StringReader;
import java.io.File;
import java.util.*;
import java.util.regex.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * @author ss3067
 *
 */
public abstract class XMLDocument extends Document {

	protected org.w3c.dom.Document documentXML;
	
	/**
	 * 
	 * @param documentIdIn
	 * @param file
	 * @param tag
	 */
	public XMLDocument(String documentIdIn, File file, String tag) {
		super(documentIdIn, file, tag);

		documentId = documentIdIn;
		documentXML = this.setXML(file);
		attributes.put("xml","xml");
		
		if (documentXML != null) {
			this.processXML(documentXML,"");			
		}
	}

	/**
	 * 
	 * @param docid
	 * @param file
	 * @param tag
	 */
	public XMLDocument(String docid, String file, String tag) {
		this(docid, new File(file), tag);
	}
	
	/**
	 * Store XML Version of file
	 * @param file
	 * @return
	 */
	protected org.w3c.dom.Document setXML(File file) {
		try {
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			docFactory.setValidating(false);
			docFactory.setAttribute("http://apache.org/xml/properties/input-buffer-size", new Integer(1024*10));
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			
			docBuilder.setEntityResolver(new EntityResolver() {
		        public InputSource resolveEntity(java.lang.String publicId, java.lang.String systemId)
		               throws SAXException, java.io.IOException
		        {
		          return new InputSource(new ByteArrayInputStream("<?xml version='1.0' encoding='UTF-8'?>".getBytes()));		         
		        }
			});

			//return docBuilder.parse(new InputSource(new java.io.FileReader(file)));
			if (documentText.startsWith("<?xml")) {
				return docBuilder.parse(file);
			}
			else {
				try {
					StringReader reader = new StringReader( "<DOC id=\"" + documentId + "\">" + documentText + "</DOC>" );
					return docBuilder.parse(new InputSource(reader));
				} catch (Exception e) {
					// last chance -- get rid of any xml that is not sentence - this will only work for sgm files right now
					documentText = documentText.substring(documentText.indexOf("<sentence"),documentText.lastIndexOf("</sentence>") + 11);
					
					Pattern p = Pattern.compile("<[^>]+>");
					Matcher m = p.matcher(documentText);
					StringBuffer sb = new StringBuffer();
					
					while(m.find()) {
						if (m.group().indexOf("sentence") >= 0) {
							
							if (m.group().indexOf("</") < 0 &&
									m.group().indexOf("'") < 0 && m.group().indexOf("\"") < 0) {
								m.appendReplacement(sb, "");
								m.find();
								while (m.group().indexOf("sentence") < 0) {
									m.find();
									continue;
								}
								
								if (m.group().indexOf("/sentence") >= 0)
									m.appendReplacement(sb, "");
							}
							
							continue;
						}
						else {
							m.appendReplacement(sb, "");
						}
					}
					m.appendTail(sb);
					
					StringReader reader = new StringReader("<DOC id=\"" + documentId + "\">" + web.HTML.convertToValidXML(sb.toString()) + "</DOC>" );
					return docBuilder.parse(new InputSource(reader));
				}
			}
			
		} catch (Exception e) {
			
			System.err.println("Error occurred in Document.setXML: " + e.toString());
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * Get the attributes pertaining to the given element from within the xml file.
	 * 
	 * EX: <entity ID="1" TYPE="PER">, ID and TYPE are the two attributes
	 * @param e
	 * @return
	 */
	protected Hashtable<String,String> getAttributes(Element e) {
		NamedNodeMap attr = e.getAttributes();
		Hashtable<String,String> attributeHash = new Hashtable<String,String>();
		
		for (int index = 0; index < attr.getLength(); index++) {
			attributeHash.put(attr.item(index).getNodeName(), attr.item(index).getNodeValue());
		}
		return attributeHash;
	}
	
	/**
	 * 
	 * @param document
	 * @param space
	 * @return documentId
	 */
	protected abstract String processXML(org.w3c.dom.Document document, String space);
	
	protected class IDComparator implements Comparator<Object>{
		public int compare(Object key1, Object key2){
			Integer k1 = Integer.parseInt(key1.toString().substring(key1.toString().lastIndexOf('-')+1).replaceAll("[^0-9]", ""));
			Integer k2 = Integer.parseInt(key2.toString().substring(key2.toString().lastIndexOf('-')+1).replaceAll("[^0-9]", ""));
			return k1.compareTo(k2);
		}
	}
	
	/**
	 * 
	 * @return XML version of document as String
	 */
	public abstract String toXML();		
	
	/**
	 * print out the given hashtable, containing some attribute in its XML format (Generally based on APF style)
	 * @param ht
	 * @return
	 */
	public String attributeToXML(Hashtable<String,Attribute> ht) {

		String htString = "";
		
		Object keys [] = (Object [])ht.keySet().toArray();
		try {
			java.util.Arrays.sort(keys,new IDComparator());
		} catch (Exception e ) {
			System.err.println("Error occurred in XMLDocument.attributeToXML: couldn't sort attribute " + e);
		}
	
		for (int index = 0; index < keys.length; index++) {
		
			Attribute attr = (Attribute)ht.get(keys[index]);
			htString += attr.toXML() + "\n";
		}
		return htString;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
