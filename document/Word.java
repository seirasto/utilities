/**
 * 
 */
package document;

import java.util.Hashtable;
import java.util.Iterator;

/**
 * The Word class refers to a word consisting of one or many tokens (it could be a small phrase) within the sentence of a document.
 * @author ss3067
 *
 */
public class Word {

	private String token;
	private int startOffset;
	private int endOffset;
	private String partOfSpeech;
	private String id;
	private Hashtable<String,String> attributes;
	
	/**
	 * 
	 * @param tokenIn
	 * @param startOffsetIn
	 * @param endOffsetIn
	 * @param idIn
	 */
	public Word(String tokenIn, int startOffsetIn, int endOffsetIn, String idIn, Hashtable<String,String> attributesIn, String pos) {
		try {
			token = tokenIn;
			startOffset = startOffsetIn;
			endOffset = endOffsetIn;
			id = idIn;
			attributes = attributesIn;
			partOfSpeech = pos;
			
			if (attributes == null) {
				attributes = new Hashtable<String,String>();
				if (id != null)	attributes.put("id", id);
			}
			
			if (id == null) {
				id = getAttribute("id");
			}
		} catch (Exception e) {
			System.err.println("Error occurred in Word constructer: " + e);
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 * @param tokenIn
	 * @param startOffsetIn
	 * @param endOffsetIn
	 * @param idIn
	 */
	public Word(String tokenIn, int startOffsetIn, int endOffsetIn, String idIn, String pos) {
		this(tokenIn,startOffsetIn,endOffsetIn,idIn,null, pos);
	}
	
	/**
	 * 
	 * @param tokenIn
	 * @param startOffsetIn
	 * @param endOffsetIn
	 * @param idIn
	 */
	public Word(String tokenIn, String startOffsetIn, String endOffsetIn, String idIn, Hashtable<String,String> attributesIn, String pos) {
		this(tokenIn,
			startOffsetIn == null || startOffsetIn.trim() == "" ? -1 : Integer.valueOf(startOffsetIn),
			endOffsetIn == null || endOffsetIn.trim() == "" ? -1 : Integer.valueOf(endOffsetIn),
			idIn, 
			attributesIn,
			pos);
	}
	
	/**
	 * 
	 * @param tokenIn
	 * @param startOffsetIn
	 * @param endOffsetIn
	 * @param idIn
	 */
	public Word(String tokenIn, String startOffsetIn, String endOffsetIn, String idIn) {
		this(tokenIn,startOffsetIn,endOffsetIn,idIn,null,null);
	}
	
	/**
	 * 
	 * @param tokenIn
	 * @param startOffsetIn
	 * @param endOffsetIn
	 * @param idIn
	 */
	public Word(String tokenIn, int startOffsetIn, int endOffsetIn, String idIn) {
		this(tokenIn,startOffsetIn,endOffsetIn,idIn,null,null);
	}
	
	public Word(String tokenIn, String startOffsetIn, String endOffsetIn, String idIn, String pos) {
		this(tokenIn,startOffsetIn,endOffsetIn,idIn,null,pos);
	}
	
	public Word(String tokenIn, String startOffsetIn, String endOffsetIn, Hashtable<String,String> attributesIn) {
		this(tokenIn,startOffsetIn,endOffsetIn,null,attributesIn, null);
	}
	
	/**
	 * 
	 * @param tokenIn
	 * @param startOffsetIn
	 * @param endOffsetIn
	 */
	public Word(String tokenIn, int startOffsetIn, int endOffsetIn) {
		this(tokenIn,startOffsetIn,endOffsetIn,null,null,null);
	}
	
	/**
	 * 
	 * @param tokenIn
	 * @param startOffsetIn
	 * @param endOffsetIn
	 */
	public Word(String tokenIn, int startOffsetIn, int endOffsetIn, Hashtable<String,String> attributesIn, String pos) {
		this(tokenIn,startOffsetIn,endOffsetIn,null,attributesIn,pos);
	}
	
	/**
	 * 
	 * @param tokenIn
	 * @param startOffsetIn
	 * @param endOffsetIn
	 * @param attributesIn
	 */
	public Word(String tokenIn, int startOffsetIn, int endOffsetIn, Hashtable<String,String> attributesIn) {
		this(tokenIn,startOffsetIn,endOffsetIn,null,attributesIn,null);
	}
	
	/**
	 * 
	 * @param tokenIn
	 * @param idIn
	 */			
	public Word(String tokenIn, String idIn) {
		this(tokenIn,null,null,idIn);
	}
	
	/**
	 * 
	 * @param tokenIn
	 * @param idIn
	 */			
	public Word(String tokenIn, String idIn, String pos) {
		this(tokenIn,null,null,idIn,pos);
	}
	
	public Word(String tokenIn, String idIn, Hashtable<String,String> attributesIn) {
		this(tokenIn,null,null,idIn,attributesIn, null);
	}
	
	/**
	 * 
	 * @param tokenIn
	 * @param attributesIn
	 */
	public Word(String tokenIn, Hashtable<String,String> attributesIn) {
		this(tokenIn,null,null,null,attributesIn, null);
	}
	
	/**
	 * 
	 * @param tokenIn
	 */			
	public Word(String tokenIn) {
		this(tokenIn,null,null,null,null);
	}
	
	/**
	 * Get the number of words
	 * @return
	 */
	public int wordSize() {
		return token.split(" ").length;
	}
	
	/**
	 * get the length of the word
	 * @return
	 */
	public int length() {
		return token.length();
	}
	
	/**
	 * get the token
	 * @return
	 */
	public String getToken() {
		return token;
	}
	
	/**
	 * true if the word has an id
	 * @return
	 */
	public boolean hasId() {
		return id == "";
	}
	
	/**
	 * get the id
	 * @return
	 */
	public String getId() {
		return id;
	}
	
	public String getPOS() {
		return partOfSpeech;
	}
	
	public void setPOS(String pos) {
		partOfSpeech = pos;
	}
	
	/**
	 * true if the word has offsets
	 * @return
	 */
	public boolean hasOffsets() {
		if (startOffset == -1) return false;
		return true;
	}
	
	/**
	 * get the start offset - the starting position of the word in the document
	 * @return
	 */
	public int getStartOffset() {
		return startOffset;
	}
	
	/**
	 * get the end offset - the ending position of the word in the document
	 * @return
	 */
	public int getEndOffset() {
		return endOffset;
	}
	
	/**
	 * get the word and all its info as a string
	 * @return  
	 */
	public String toString() {
		return (id != null && id != "" ? id + ": " : " ") + token + 
			  (startOffset >= 0 ? " (" + startOffset + "," + endOffset + ")" : ""); 
	}

	/**
	 * get the word and all its info as a xml (Follows APF format)
	 * @return  
	 */
	public String toXML() {
		return (id != null && id != "" ? "<token ID=\"" + id + "\">\n" : "") +
	      "  <charseq " + (startOffset >= 0 ? "START=\"" + startOffset + "\" END=\"" + endOffset + "\"" : "") + ">" + web.HTML.convertToValidHtml(token) + "</charseq>"
	      + (id != null && id != "" ? "\n</token>\n" : "");
	}
	
	/**
	 * Two words are equal if their text is the same and they have the same charater offset (if applicable). 
	 * ID is not important. Returns true if the words are equal.
	 * @param word
	 * @return
	 */
	public boolean equals(Word word) {
		if (!word.getToken().equalsIgnoreCase(this.token)) return false;
		if (!word.hasOffsets() && !this.hasOffsets()) return true;
		if (!word.hasOffsets() || !this.hasOffsets()) return false;
		if (word.getStartOffset() != this.getStartOffset()) return false;
		if (word.getEndOffset() != this.getEndOffset()) return false;
		return true;
	}
	
	/**
	 * Two words are equal if their text is the same and they have the same charater offset (if applicable). 
	 * ID is not important. Returns true if the words are equal.
	 * @param word
	 * @return
	 */
	public boolean equals(String word) {
		return this.equals(new Word(word));
	}
	
	/**
	 * This approximate. They are mostly equal, but offsets can be within. Also, ignore case and punctuation.
	 * @param word
	 * @return
	 */
	public boolean equalsApprox(Word word) {
		String wordToken = word.getToken().toLowerCase().replaceAll("[\\p{Punct}]", "").replaceAll("\\s+"," ");
		String thisToken = this.getToken().toLowerCase().replaceAll("[\\p{Punct}]", "").replaceAll("\\s+"," ");
		//System.out.print(wordToken + " ? " + thisToken + ", ");
		if (!wordToken.equalsIgnoreCase(thisToken)) return false;
		if (!word.hasOffsets() && !this.hasOffsets()) return true;
		if (!word.hasOffsets() || !this.hasOffsets()) return false;
		if (word.getStartOffset() >= this.getStartOffset()
				&& word.getEndOffset() <= this.getEndOffset()) return true;
		return false;
	}
	
	/**
	 * This approximate. They are mostly equal, but offsets can be within. Also, ignore case and punctuation.
	 * @param word
	 * @return
	 */
	public boolean equalsApprox(String word) {
		return this.equalsApprox(new Word(word));
	}
	
	/**
	 * Get the percentage of overlap existing between the two words.
	 * @param word
	 * @return
	 */
	public double overlap(Word word) {
		String wordToken = word.getToken().toLowerCase().trim().replaceAll("[\\p{Punct}]", "").replaceAll("\\s+"," ");
		String thisToken = this.getToken().toLowerCase().trim().replaceAll("[\\p{Punct}]", "").replaceAll("\\s+"," ");
		
		String [] splitWord = wordToken.split(" ");
		int count = 0;
		
		if (splitWord.length == 0) return 0;
		
		for (int index = 0; index < splitWord.length; index ++) {
			if (thisToken.indexOf(splitWord[index]) >= 0) {
				thisToken = thisToken.replaceFirst(splitWord[index], " ");
				count++;
			}
		}
		return (double)count/(wordToken.split(" ").length + thisToken.split(" ").length);
	}
	
	/**
	 * Get the percentage of overlap existing between the two words.
	 * @param word
	 * @return
	 */
	public double overlap(String word) {
		return this.overlap(new Word(word));
	}
	
	/**
	 * Get the character position of the word. Within these words, but offsets can be within. Also, ignore case and punctuation.
	 * @param word
	 * @return
	 */
	public int indexOf(Word word) {
		String wordToken = word.getToken().toLowerCase().replaceAll("[\\p{Punct}]", "").replaceAll("\\s+"," ");
		String thisToken = this.getToken().toLowerCase().replaceAll("[\\p{Punct}]", "").replaceAll("\\s+"," ");
		if (wordToken.trim().equals("")) return 0;
		if (!word.hasOffsets() && !this.hasOffsets()) return thisToken.indexOf(wordToken);
		if (!word.hasOffsets() || !this.hasOffsets()) return -1;
		if (word.getStartOffset() >= this.getStartOffset()
				&& word.getEndOffset() <= this.getEndOffset()) return thisToken.indexOf(wordToken);
		return -1;		
	}
	
	/**
	 * Get the value of the given attribute for this word. It is not case sensitive.
	 * 
	 * @param attribute
	 * @return
	 */
	public String getAttribute(String attribute) {
		if (!hasAttribute(attribute)) return null;
		if (attributes.get(attribute.toLowerCase()) != null) {
				return attributes.get(attribute.toLowerCase());
		}
		return attributes.get(attribute.toUpperCase());
	}
	
	/**
	 * True if the word has the attribute. It is not case sensitive.
	 * @param attribute
	 * @return
	 */
	public boolean hasAttribute(String attribute) {
		if (attribute == null) return false;
		return attributes.containsKey(attribute.toLowerCase()) ||
		attributes.containsKey(attribute.toUpperCase());
	}
	
	/**
	 * Get all the attributes.
	 * @return
	 */
	public Iterator<String> getAttributes() {
		return attributes.keySet().iterator();
	}
}
