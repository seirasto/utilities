package document;

import java.util.Vector;

public class Parse {
	private String parseId;
	private String partOfSpeech;
	int position = -1;
	//private Vector<Word> words
	Word word;
	private Vector<Parse> parses;
	
	public Parse(String id, String partOfSpeechIn, Vector<Parse> parsesIn) {
		this(id,partOfSpeechIn,null,parsesIn);
	}
	
	public Parse(String id, String partOfSpeechIn, Word wordIn ) {
		this(id, partOfSpeechIn, wordIn, null);
	}
	
	public Parse(String id, String partOfSpeechIn, Word wordIn, int positionIn ) {
		this(id, partOfSpeechIn, wordIn, null, positionIn);
	}
	
	public Parse(String id, String partOfSpeechIn, Word wordIn, Vector<Parse> parsesIn ) {
		this(id, partOfSpeechIn, wordIn, parsesIn, -1);
	}
	
	public Parse(String id, String partOfSpeechIn, Word wordIn, Vector<Parse> parsesIn, int positionIn ) {
		parseId = id;
		partOfSpeech = partOfSpeechIn;
		word= wordIn;
		parses = parsesIn;
		position = positionIn;
	}
	
	public String getPartOfSpeech() {
		return partOfSpeech;
	}
	
	public Word getWord() {
		if (this.isLeaf() || word != null || (word != null && !word.equals(""))) {
			return word;
		}
		
		String phrase = "";
		int start = 0;
		int end = 0;
		
		for (int index = 0; index < parses.size(); index++) {
			Word thisWord = parses.elementAt(index).getWord();
			if (index == 0) {
				start = thisWord.getStartOffset(); 
			}
			if (index == parses.size() - 1) {
				end = thisWord.getEndOffset();
			}
			phrase += thisWord.getToken() + " ";			
		}
		word = new Word(phrase,start,end,parseId);
		return word;
	}
	
	public String toString() {
		
		String wordString = "";
		
		/*for (int index= 0; words != null && index < words.size(); index++) {
			wordString += words.elementAt(index).getToken();
		}*/
		
		for (int index = 0; parses != null && index < parses.size(); index++) {
			wordString += "[" + parses.elementAt(index).toString() + "]";
		}
		
		return (parseId != "" ? parseId + ": " : "") +
		(partOfSpeech == "" ? "" : partOfSpeech + " ") +
			(word != null ? word.toString() : "") + wordString; 
			   
	}
	
	public String toXML() {
		String xml = "";
		if (partOfSpeech != null && !partOfSpeech.equals("")) {
			xml = "<node cat=\"" + partOfSpeech + "\" ID=\"" + parseId + "\">\n";
		}
		
		if (this.isLeaf()) {
			xml +=  "  <charseq START=\"" + word.getStartOffset() + "\" END=\"" + word.getEndOffset() + "\">" + word.getToken() + "</charseq>\n";
		}
		
		for (int index=0; parses != null && index < parses.size(); index++) {
			xml += " " + parses.elementAt(index).toXML();
		}
		if (partOfSpeech != null && !partOfSpeech.equals("")) {
			xml += "</node>\n";
		}
		
		return xml;
	}
	
	public boolean isLeaf() {
		if (parses != null && parses.size() > 0) {
			return false;
		}
		return true;
	}
	
	/**
	 * Get parse for one word (lowest level)
	 * 
	 * @param wordIn
	 * @return
	 */
	public Parse getParse(Word wordIn) {
		Parse thisParse = null;
		
		try {
			
			if (this.isLeaf()) {
				if (word != null && word.equals(wordIn)) {
					return this;
				}
				return null;
			}
			for (int index = 0; index < parses.size(); index++ ) {
				thisParse = parses.elementAt(index).getParse(wordIn);
				
				if (thisParse != null) break;
			}
		} catch (Exception e) {
			System.err.println("Error occurred in Parse.getParse (for single word): " + e);
		}
		return thisParse;
	}
	
	/**
	 * Get parse for a group of words, aka a subsentence
	 * @param words
	 * @return
	 */
	public Parse getParse(Vector<Word> words) {
		Vector<Parse> thisParse = new Vector<Parse>();
		
		if (this.isLeaf()) {
			for (int index = 0; index < words.size(); index++) {
				if (word.indexOf(words.elementAt(index)) >= 0) {
					return this;
				}
			}
			return null;
		}
		for (int index = 0; index < parses.size(); index++ ) {
			Parse p = parses.elementAt(index).getParse(words);
			if (p != null) {
				thisParse.addElement(p);
			}
		}
		
		if (thisParse.size() > 0) {
			return new Parse(this.parseId,this.partOfSpeech,thisParse);
		}
		return null;
	}
	
	public Parse getParse(Word wordIn, int height) {
		Parse thisParse = null;
		
		try {
			
			if (this.isLeaf()) {
				if (word != null && word.equals(wordIn)) {
					return this;
				}
				return null;
			}
			for (int index = 0; index < parses.size(); index++ ) {
				thisParse = parses.elementAt(index).getParse(wordIn,height);
				
				if (thisParse != null) {
					if (height <= 0) break;
					else {
						thisParse = this;
						height--;
					}
				}
			}
		} catch (Exception e) {
			System.err.println("Error occurred in Parse.getParse (for word at height): " + e);
			e.printStackTrace();
		}
		return thisParse;
	}
	
	public Parse getParse(int positionIn, int height) {
		Parse thisParse = null;
		
		try {

			if (this.isLeaf() && position >= 0 && positionIn == position && height == 0) {
				return this;
			}
			if (this.getDistance(positionIn, height, 0) == height) {
				return this;
			}
			if (this.isLeaf()) return null;
			
			for (int index = 0; index < parses.size(); index++ ) {

				thisParse = parses.elementAt(index).getParse(positionIn,height);

				if (thisParse != null) {
					return thisParse;
				}
			}
		} catch (Exception e) {
			System.err.println("Error occurred in Parse.getParse (for position and height): " + e);
			e.printStackTrace();
		}
		return thisParse;
	}	
	
	public int getHeight() {
		try {
			
			if (this.isLeaf()) {
				return 0;
			}
			
			int height = 0;
			
			for (int index = 0; index < parses.size(); index++ ) {
				int h = parses.elementAt(index).getHeight() ;
				height = h > height ? h : height;  
			}
			return height + 1;
		} catch (Exception e) {
			System.err.println("Error occurred in Parse.getHeight: " + e);
		}
		return -1;
	}
	
	public int getDistance(int positionIn, int maxDistance,int down) {
		try {

			if (this.isLeaf() && position == positionIn) {
				return 0;
			}
			else if (this.isLeaf()) {
				return -1;
			}

			for (int index = 0; index < parses.size(); index++ ) {
				int d = parses.elementAt(index).getDistance(positionIn,maxDistance,down+1);
				
				if (d >= 0) {
					return d+1;
				}		
				if (d == -2) {
					return -2;
				}
			}
		} catch (Exception e) {
			System.err.println("Error occurred in Parse.getHeight: " + e);
		}
		return -1;
	}
	
	/**
	 * Get sub parses of this parse
	 * @return
	 */
	public Vector<Parse> getParses() {
		return this.parses;
	}
}
