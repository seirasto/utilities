/**
 * 
 */
package bot;

import java.util.ArrayList;
import java.util.Hashtable;

/**
 * @author sara
 *
 */
public abstract class Robot {

	private Hashtable<String,String> visitedUrls;
	
	public Robot() {}	
	
	// run the robot
	public abstract boolean run(ArrayList<String> data);
	public abstract boolean run(ArrayList<String> data, Hashtable<String,String> blacklist);
	
	// get a list of "urls" that robot found to use in crawler
	public abstract ArrayList<String> urls();
	public abstract void clearUrls();
	public abstract int getNumDownloads();
	
	public boolean visited(String url) {
		return visitedUrls.containsKey(url);
	}
}