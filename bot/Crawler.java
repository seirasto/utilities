/**
 * 
 */
package bot;

import java.net.*;
import java.util.*;
import java.io.*;
/**
 * @author sara
 * A simple crawler
 */
public class Crawler {

	String domain;
	Robot robot;
	// id's that have been visited during this crawl
	Hashtable<String,String> visited;
	// for downloading pages. these should not be downloaded (they already have)
	Hashtable<String,String> blacklist;
	
	/**
	 * Given a url, extract all data that matches a given regexp
	 * Do something to each expression and return true if it should be crawled 
	 * Crawl domain + expression
	 * 
	 * Ex:
	 * 
	 * domain: www.somewebsite.com?user=
	 * start: johndoe
	 * 
	 * crawl: domain + start = www.somewebsite.com?user=johndoe
	 * get data: ex: janedoe, return true
	 * crawl: www.somewebsite.com?user=janedoe
	 * etc...
	 * 
	 * @param url
	 */
	public Crawler(String domainIn, Robot robotIn) {
		domain = domainIn;
		robot = robotIn;
		visited = new Hashtable<String,String>();
		blacklist = new Hashtable<String,String>();		
	}
	
	public Crawler(String domainIn, Robot robotIn, String blacklistIn) {
		this(domainIn,robotIn);
		
		try {
			
			if (!new File(blacklistIn).exists()) {
				return;
			}
			
			BufferedReader in = new BufferedReader(
					new FileReader(blacklistIn));
	
			String inputLine;
			while ((inputLine = in.readLine()) != null) {
				blacklist.put(inputLine,inputLine);
			}
		} catch (Exception e) {
			System.err.println("Error occurred in Crawler constructor: " + e);
			e.printStackTrace();
		}
	}
	
	/**
	 * Keep crawling until degree is reached or max profiles have been downloaded
	 * @param start
	 * @param degree
	 * @param max
	 * @return
	 */
	public boolean crawl(String start, int degree, int max) {
		
		if (degree <= 0) return false; 
		if (robot.getNumDownloads() >= max) return false;
		
		try {
			
			System.err.println("[Crawler.crawl] crawling " + start + ", degree=" + degree + ", # downloaded=" + robot.getNumDownloads());
			
			if (robot.run(web.Data.getPage(new URL(domain + start)),blacklist)) {
				ArrayList<String> urls = robot.urls();
				robot.clearUrls();
				
				if (degree <= 1) return false; 
				if (robot.getNumDownloads() >= max) return false;
				
				for (int index = 0; index < urls.size(); index++) {
					if (visited.containsKey(urls.get(index))) continue;
					visited.put(urls.get(index), urls.get(index));
					blacklist.put(urls.get(index), urls.get(index));
					this.crawl(urls.get(index), degree-1, max);					
				}
				
				return true;
			}
			return false;
		} catch (Exception e) {
			System.err.println("Error occurred in Crawler.crawl on " + domain + start + ": " + e);
			e.printStackTrace();
		}
		
		return false;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
