package social_media;

import java.util.*;
import java.io.*;

import processing.GeneralUtils;

//import wiktionary.Wiktionary;

/**
 * Given a sentence, compute all social media features requested
 * 
 * @author sara
 *
 */
public class Features {

	public static final String NUM_WORDS = "word_count";
	public static final String SLANG = "slang";
	public static final String CAPITAL_WORDS = "capital_words";
	public static final String EMOTICONS = "emoticons";
	public static final String ACRONYMS = "acronyms";
	public static final String LINKS = "links";
	public static final String PUNCTUATION = "punctuation";
	public static final String PUNCTUATION_R = "repeated_punctuation";
	public static final String PUNCTUATION_NUM = "punctuation_count";
	public static final String EXCLAMATION = "!";
	public static final String EXCLAMATION_R = "!!!";
	public static final String QUESTION = "?";
	public static final String QUESTION_R = "???";
	public static final String ELLIPSES = "...";
	public static final String AVG_WORD_LENGTH = "average_word_length";
	public static final String ALL_CAPS_WORDS = "all_caps_words";
	public static final String QUOTES = "quotes";
	public static final String HONORIFICS = "honorifics";
	/*public static final String HASHTAG = "#";
	public static final String ASTERISK = "*";
	public static final String AT = "@";
	public static final String PERCENT = "%";
	public static final String DOLLAR = "$";*/
	public static final String WORD_LENGTHENING = "word_lengthening";
	
	HashSet<String> _dictionary;
	HashSet<String> _emoticons;
	HashSet<String> _acronyms;
	
	public Features(String dictionaryFile, String emoticonFile, String acronymFile) {
		_emoticons = populateDictionary(emoticonFile);
		_acronyms = populateDictionary(acronymFile);
		_dictionary = populateDictionary(dictionaryFile);
	}
	
	private HashSet<String> populateDictionary(String file) {
		// populate dictionary
		HashSet<String> dictionary = new HashSet<String>();
		try {
   			BufferedReader in = new BufferedReader(
					new FileReader(file));
   			
   			String word = "";
   			
   			while ((word = in.readLine()) != null) {
	        	dictionary.add(word.toLowerCase());
   			}
   			in.close();
		} catch (Exception e) {
			System.err.println("Error processing dictionary: " + e);
			e.printStackTrace();
		}
		return dictionary;
	}
	
	public Hashtable<String,Double> compute(String [] words) {
		Hashtable<String,Double> features = new Hashtable<String,Double>();
		
		features.put(NUM_WORDS, (double)words.length);
		features.put(SLANG, 0.0);
		features.put(CAPITAL_WORDS,0.0);
		features.put(EMOTICONS, 0.0);
		features.put(ACRONYMS,0.0);
		features.put(LINKS, 0.0);
		features.put(PUNCTUATION, 0.0);
		features.put(PUNCTUATION_R, 0.0);
		features.put(PUNCTUATION_NUM, 0.0);
		features.put(EXCLAMATION, 0.0);
		features.put(EXCLAMATION_R, 0.0);
		features.put(QUESTION, 0.0);
		features.put(QUESTION_R, 0.0);
		features.put(ELLIPSES, 0.0);
		/*features.put(HASHTAG,0);
		features.put(ASTERISK,0);
		features.put(AT,0);
		features.put(PERCENT,0);
		features.put(DOLLAR,0);*/
		features.put(WORD_LENGTHENING,0.0);
		features.put(ALL_CAPS_WORDS, 0.0);
		features.put(AVG_WORD_LENGTH,0.0);
		features.put(QUOTES, 0.0);
		
		ArrayList<String> wordsArray = new ArrayList<String>();
		wordsArray.addAll(Arrays.asList(words));
		
		double avgWordLength = 0;
	
		for (int index = 0; index < wordsArray.size(); index++) {
			if (_emoticons.contains(wordsArray.get(index).toLowerCase())) 
				features.put(EMOTICONS,features.get(EMOTICONS)+1);
			else if (_acronyms.contains(wordsArray.get(index).toLowerCase())) 
				features.put(ACRONYMS,features.get(ACRONYMS)+1);
		}
		
		for (int index = 0; index < wordsArray.size(); index++) {
			
			while (wordsArray.get(index).length() > 1 && !wordsArray.get(index).matches("\\p{Punct}+") 
					&& String.valueOf(wordsArray.get(index).charAt(wordsArray.get(index).length()-1)).matches("\\p{Punct}")) {
				wordsArray.add(index+1, wordsArray.get(index).substring(wordsArray.get(index).length()-1,wordsArray.get(index).length()));
				wordsArray.set(index,wordsArray.get(index).substring(0,wordsArray.get(index).length()-1));
			}
			
			avgWordLength += wordsArray.get(index).length();
			
			if (wordsArray.get(index).matches("\"|''")) features.put(QUOTES,features.get(QUOTES)+1);
			
			if (wordsArray.get(index).toUpperCase().contains("LLLINKKK") || wordsArray.get(index).toUpperCase().contains("IIIMAGEEE")) {
				features.put(LINKS,features.get(LINKS)+1);
			}
			if (wordsArray.get(index).toUpperCase().contains("UUUSERRR") || wordsArray.get(index).toUpperCase().contains("HHHASHTAGG")) {
				continue;
			}
			// to capture normal non-emoticon punctuation such as: . and ,
			/*else if (wordsArray.get(index).length() > 1 && !wordsArray.get(index).matches("\\p{Punct}+") 
					&& String.valueOf(wordsArray.get(index).charAt(wordsArray.get(index).length()-1)).matches("\\p{Punct}")) {
				
				wordsArray.get(index) = wordsArray.get(index).substring(0,wordsArray.get(index).length()-1);
				features.put(PUNCTUATION,features.get(PUNCTUATION)+1);
				features.put(PUNCTUATION_NUM,features.get(PUNCTUATION_NUM)+1);
			}*/
			
			// at least two letters to avoid I,A
			else if (wordsArray.get(index).matches("[A-Z][A-Z']+")) features.put(ALL_CAPS_WORDS,features.get(ALL_CAPS_WORDS)+1);
			else if (wordsArray.get(index).matches("^[A-Z].*")) features.put(CAPITAL_WORDS, features.get(CAPITAL_WORDS)+1);
			/*else if (_emoticons.contains(wordsArray.get(index).toLowerCase())) 
				features.put(EMOTICONS,features.get(EMOTICONS)+1);
			else if (_acronyms.contains(wordsArray.get(index).toLowerCase())) 
				features.put(ACRONYMS,features.get(ACRONYMS)+1);*/
			else if (wordsArray.get(index).toLowerCase().matches("\\p{Punct}+")) {
				
				String punctuation = wordsArray.get(index);
				
				while (index+1 < wordsArray.size() && wordsArray.get(index+1).matches("\\p{Punct}+")) {
					punctuation += wordsArray.get(index+1);
					index++;
				}
				
				features.put(PUNCTUATION_NUM,features.get(PUNCTUATION_NUM)+punctuation.length());

				if (punctuation.equals("...")) features.put(ELLIPSES, features.get(ELLIPSES)+1);
				else if (punctuation.equals("?")) features.put(QUESTION, features.get(QUESTION)+1);
				else if (punctuation.matches("\\?+")) features.put(QUESTION_R, features.get(QUESTION_R)+1);
				else if (punctuation.equals("!")) features.put(EXCLAMATION, features.get(EXCLAMATION)+1);
				else if (punctuation.matches("\\!+")) features.put(EXCLAMATION_R, features.get(EXCLAMATION_R)+1);
				else if (punctuation.length() == 1) features.put(PUNCTUATION,features.get(PUNCTUATION)+1);
				else features.put(PUNCTUATION_R,features.get(PUNCTUATION_R)+1);
			}
			else if (_dictionary.contains(wordsArray.get(index).replaceAll("\\p{Punct}","").toLowerCase())
					|| _dictionary.contains(wordsArray.get(index).toLowerCase())
					|| wordsArray.get(index).matches("[0-9]+")) continue;		
			// repeated characters
			else if (wordsArray.get(index).matches(".*(\\p{Alpha})(\\1)+.*")) {
				if (!_dictionary.contains(wordsArray.get(index).replaceAll("(\\p{Alpha})(\\1)+", "$1")) 
						&& !wordsArray.get(index).matches(".*(\\p{Alpha})(\\1)(\\1)+.*")) continue;
				// skip websites
				if (wordsArray.get(index).indexOf("www.") >= 0) continue;
	    		//System.out.println("REPEAT:" + wordsArray.get(index));
				features.put(WORD_LENGTHENING, features.get(WORD_LENGTHENING)+1);
			}
			else features.put(SLANG,features.get(SLANG)+1);
		}
		
		features.put(AVG_WORD_LENGTH, avgWordLength/wordsArray.size());
		
		//System.out.println(features);
		return features;		
	}
	
	public String createPunctuationString(String [] words) {
		
		String punctuation = "";
		boolean previous_punctuation = false;
		
		for (String word : words) {
			
			if (_emoticons.contains(word.toLowerCase()) || _acronyms.contains(word.toLowerCase())) {
				if (previous_punctuation) punctuation += " ";
				punctuation += word + " ";
				previous_punctuation = true;
			}
			else {
				punctuation += word.replaceAll("[^\\p{Punct}++]"," ");
			}
			// entire word is punctuation
			/*else if (word.matches("\\p{Punct}+")) {
				punctuation += word;
				previous_punctuation = true;
			}
			else {
				if (previous_punctuation) punctuation += " ";
				previous_punctuation = false;
			}*/
		}
		return punctuation.trim().replaceAll("\\s+", " ");
	}
	
	public static void main(String[] args) throws Exception {
		String emoticonDirectory = "";
		
		try {
			Properties prop = GeneralUtils.getProperties("config/config.properties");
			emoticonDirectory = prop.getProperty("emoticons_directory");
		} catch (Exception e) {
			e.printStackTrace();
		}
    	
    	Features socialMedia = new Features("/proj/nlp/users/sara/dictionaries/standard/words",emoticonDirectory + "emoticon_sub.txt", emoticonDirectory + "acronyms.txt");
    	socialMedia.compute(new String [] {"Wow", "!", "!"});
    	socialMedia.compute(new String [] {"Lady", "Gaga", "is", "actually", "at", "the", "Britney", "Spears", "Femme", "Fatale", "Concert", "tonight", "!", "!", "!", "She"});
    	socialMedia.compute(new String [] {"still"});
    	socialMedia.compute(new String [] {"listens", "to", "her","music","?","?","?","?"});
    	socialMedia.compute(new String [] {"WOW", "!", "!", "!"});
    	socialMedia.compute(new String [] {"Has", "spent", "a", "morning", ","});
    	socialMedia.compute(new String [] {"admiring"});
    	socialMedia.compute(new String [] {"Beyonce","!", "Now"});
    	socialMedia.compute(new String [] {"feel","like","I","run","the","world","!"});
    	socialMedia.compute(new String [] {"Has", "spent", "a", "morning.", ".","."});
    	socialMedia.compute(new String [] {"I","will","nott","be","back","online",".","I","am","gonna","watch","the","last","two","episodes","of","Vampire","Diaries","tomorrow",".",":P","Nighty","night",",","tweethearts","<3"});
    	Hashtable<String,Double> ht = socialMedia.compute(new String [] {"Ravens","sign","Ricky","Williams","ohhhhh","leeeeee","dueeetttttt","#","blackbirdgang",".",".","."});
    	
    	Enumeration<String> keys = ht.keys(); 
    	while (keys.hasMoreElements()) {
    		String key = keys.nextElement();
    		System.out.println(key + " : " + ht.get(key));
    	}
    	
    	System.out.println(socialMedia.createPunctuationString(new String [] {"listens", "to", "her","music","?","?","?","?"}));
    	System.out.println(socialMedia.createPunctuationString(new String [] {"I","will","not","be","back","online",".","I","am","gonna","watch","the","last","two","episodes","of","Vampire","Diaries","tomorrow",".",":P","Nighty","night",",","tweethearts","<3"}));
    	System.out.println(socialMedia.createPunctuationString(new String [] {"Ravens","sign","Ricky","Williams","ohhhhh","leeeeee","dueeetttttt","#","blackbirdgang",".",".",".",":)"}));
	}
}
