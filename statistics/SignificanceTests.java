package statistics;

import java.lang.Math;
import weka.classifiers.Evaluation;

public class SignificanceTests {

	public static void ttest(Evaluation [] V1, Evaluation [] V2) {
		double [] DV1 = new double[V1.length];
  	  	for (int i = 0; i < V1.length; i++) DV1[i] = V1[i].pctCorrect();
		double [] DV2 = new double[V2.length];
  	  	for (int i = 0; i < V2.length; i++) DV2[i] = V2[i].pctCorrect();
  	  	ttest(DV1,DV2);
	}
	
	public static void ttest(String [] V1, String [] V2) {
		double [] DV1 = new double[V1.length];
  	  	for (int i = 0; i < V1.length; i++) DV1[i] = Double.parseDouble(V1[i]);
		double [] DV2 = new double[V2.length];
  	  	for (int i = 0; i < V2.length; i++) DV2[i] = Double.parseDouble(V2[i]);
  	  	ttest(DV1,DV2);
	}
	
	public static void ttest(double [] V1, double [] V2) {
		
		double X1 = average(V1);
		double X2 = average(V2);
		
		double variance1 = variance(V1);
		double variance2 = variance(V2);
		
		double top = (X1 - X2);
		double bottom = Math.sqrt((variance1/V1.length) + (variance2/V2.length));
		
		double value = top/bottom;		
		
		//assumes length = 10 for each group, so that df = 18 (degrees of freedom)
		if (value >= 3.92) {System.out.println("Significant at .0005"); return;}
		if (value >= 2.88) {System.out.println("Significant at .005"); return;}
		if (value >= 2.55) {System.out.println("Significant at .01"); return;}
		if (value >= 2.10) {System.out.println("Significant at .025"); return;}
		if (value >= 1.73) {System.out.println("Significant at .05"); return;}
		System.out.println("Not Significant (" + value + ")");
	}
	
	private static double variance (double [] values) {
		double average = average(values);
		
		double total = 0;
		
		for (int index = 0; index < values.length; index++) {
			double distance = values[index] - average;
			total += distance * distance;
		}
		
		return total/(values.length -1);
	}
	
	private static double average(double [] values) {
		
		double total = 0;
		
		for (int index = 0; index < values.length; index++) {
			total += values[index];
		}
		return total/values.length;
	}
}
