package web;

import org.apache.commons.lang.StringEscapeUtils;

public class HTML {

	/**
	 * Strip all html tags and replace br with \n
	 * @param text
	 * @return
	 */
	public static String stripTags(String text) {
		text = text.replaceAll("&lt;br /&gt;", "\\n");
		text = text.replaceAll("&lt;.*?&gt;", " ");
		text = text.replaceAll("&nbsp;", " ");
		text = text.replaceAll("\\<.*?>", " ");
		return text;
	}
	
	public static String convertToValidHtml(String input) {
		String result = "";
		
		
		for (int index = 0; index < input.length(); index++) {
			switch (input.charAt(index)) {
				case '&':
					if (input.charAt(index+1) == ' ') {
						result += "&amp;";
					}
					else { 
						result += input.charAt(index);
					}
					break;
				case ';':
					result += "&#059;";
					break;
				case '<':
					result += "&lt;";
					break;
				case '>':
					result += "&gt;";
					break;
				case '"':
					result += "&quot;";
					break;
				case '\'':
					result += "&apos;";
					break;
				case '(':
					result += "&#040;";
					break;
				case ')':
					result += "&#041;";
					break;	
				case '#':
					result += "&#035;";
					break;
				case '%':
					result += "&#037;";
					break;
				case '+':
					result += "&#043;";
					break;
				case '-':
					result += "&#045;";
					break;	
				default:
					result += input.charAt(index);
			}
		}
		return result;
	}

	/**
	 * Assumes we are only looking at text outside <>. ie. CDATA 
	 * @param input
	 * @return
	 */	
	public static String fixSpeciaXMLlCharacters(String input) {
		String result = "";

		for (int index = 0; index < input.length(); index++) {
			switch (input.charAt(index)) {
				case '&':
					if (input.length() == index+1 || input.charAt(index+1) == ' ') {
						result += "&amp;";
					}
					else { 
						result += input.charAt(index);
					}
					break;
				case '<':
					result += "&lt;";
					break;
				case '>':
					result += "&gt;";
					break;
				default:
					result += input.charAt(index);
			}
		}
		return result;
	}
	
	public static String htmlToText(String input) {
		
		try {
			input = StringEscapeUtils.unescapeHtml(input);
			input = input.replaceAll("&amp;", "&");
			input = input.replaceAll("&#059;", ";");
			input = input.replaceAll("&lt;", "<");
			input = input.replaceAll("&gt;", ">");
			input = input.replaceAll("&quot;", "\"");
			input = input.replaceAll("&#039;", ""); //\\ doesn't work?
			input = input.replaceAll("&#040;", "(");
			input = input.replaceAll("&#041;", ")");
			input = input.replaceAll("&#035;", "#");
			input = input.replaceAll("&#037;", "%");
			input = input.replaceAll("&#043;", "+");
			input = input.replaceAll("&#045;", "-");
			input = input.replaceAll("&hearts;|&9829;|♥", "<3");  
			input = input.replaceAll("<br />|<br>", "\n");
			input = input.replaceAll("<center>|</center>", "");
			input = input.replaceAll("<b>|</b>|<i>|</i>|<u>|</u>|<em>|</em>|</a>", "");
			input = input.replaceAll("<p [^>]*>|</p>", "");
			//input = input.replaceAll("<img src=\\p{Graph}+>", "[IMAGE]");
			//input = input.replaceAll("<img src=\\p{Graph}+ alt=\"[^>]+\"/>", "[IMAGE]");
			//input = input.replaceAll("<img src=\\p{Graph}+ border=\"[^>]+\" alt=\"[^>]+\"/>", "[IMAGE]");
			input = input.replaceAll("<img [^>]+/>", "IIIMAGEEE");
			//input = input.replaceAll("<a href=\\p{Graph}+>|</a>", "[LINK]");
			input = input.replaceAll("<a [^>]+>", "LLLINKKK ");
			// catch all
			input = input.replaceAll("<[^>]*>", "");
		} catch (Exception e) {
			System.err.println("[HTML.htmlToText] error converting " + input + ": " + e);
			e.printStackTrace();
		}
		return input;
	}

	public static String convertToValidXML(String input) {
		if (input == null) return null;
		
		String result = "";
		
		for (int index = 0; index < input.length(); index++) {
			
			// avoid < XML > areas
			if (input.charAt(index) == '<') {
				int oldindex = index;
				index = input.indexOf('>',index);
				result += input.substring(oldindex,index+1);
				index++;
				continue;
			}
			switch (input.charAt(index)) {
			
				case '&':
					result += "&amp;";
					break;
				case '\'':
					result += "&apos;";
					break;
				case '"':
					result += "&quot;";
					break;	
				case ';':
					result += "&#059;";
					break;				
				case '(':
					result += "&#040;";
					break;
				case ')':
					result += "&#041;";
					break;	
				case '#':
					result += "&#035;";
					break;
				case '%':
					result += "&#037;";
					break;
				case '+':
					result += "&#043;";
					break;
				case '-':
					result += "&#045;";
					break;
				default:
					result += input.charAt(index);
			}
		}
		return result;
	}
	
	public static void main(String [] args) {
		
		//String test = "&lt;center&gt;&lt;br /&gt;&lt;br /&gt;&lt;a href=&quot;http://photobucket.com&quot; target=&quot;_blank&quot;&gt;&lt;img src=&quot;http://i168.photobucket.com/albums/u169/pea3nut/z63995157.png&quot; border=&quot;0&quot; alt=&quot;Photo Sharing and Video Hosting at Photobucket&quot;&gt;&lt;/a&gt;&lt;br /&gt;&lt;br /&gt;&lt;br /&gt;&lt;br /&gt;&lt;br /&gt;&lt;font color=&quot;#FD9BD0&quot;&gt; name: &lt;/font&gt; tiny ♥ panda&lt;br /&gt;&lt;font color=&quot;#FD9BD0&quot;&gt; age: &lt;/font&gt; 17&lt;br /&gt;&lt;font color=&quot;#FD9BD0&quot;&gt; lives: &lt;/font&gt; canada&lt;br /&gt;&lt;font color=&quot;#FD9BD0&quot;&gt; taken?: &lt;/font&gt; yes!&lt;br /&gt;&lt;font color=&quot;#FD9BD0&quot;&gt; likes: &lt;/font&gt; rainbows, pink, kittens, puppies, cuddles, music, concerts, cupcakes, baking, fun, parties, drinking, laughing, fashion, makeup, clothing, shopping, small things, world, love, boyfriend, candies, painting, reading, anime, ddr, videogames, movies, super nintendo, snow, stuffed animals, japanese street fashion, couture, the l word, headphones, cute, unicorns, my prince.&lt;br /&gt;&lt;font color=&quot;#FD9BD0&quot;&gt; dislikes: &lt;/font&gt; rain &amp; storms, sleeping alone, mayonaise, bad hair, stalkers.&lt;br /&gt;&lt;br /&gt;&lt;br /&gt;&lt;a href=&quot;http://photobucket.com&quot; target=&quot;_blank&quot;&gt;&lt;img src=&quot;http://i168.photobucket.com/albums/u169/pea3nut/63vhzme.jpg&quot; border=&quot;0&quot; alt=&quot;Photo Sharing and Video Hosting at Photobucket&quot;&gt;&lt;/a&gt;&lt;br /&gt;&lt;br /&gt;&lt;br /&gt;&lt;br /&gt;&lt;br clear=&quot;all&quot;&gt;&lt;br /&gt;&lt;img src=&quot;http://www.toxin.org/cgi-bin/count_hugs.cgi?hug=xamburr&quot; height=&quot;40&quot; width=&quot;240&quot; title=&quot;HUGS&quot;&gt;&lt;br /&gt;&lt;br clear=&quot;all&quot;&gt;&lt;br /&gt;*HUGS* TOTAL!&lt;br /&gt;&lt;a href=&quot;http://www.toxin.org/cgi-bin/hugs.cgi?&amp;amp;HUGS=yes&amp;amp;hug=xamburr&quot;&gt;give xamburr more *HUGS*&lt;/a&gt;&lt;br /&gt;&lt;br&gt;&lt;small&gt;&lt;a href=&quot;http://www.toxin.org/cgi-bin/gethugs.cgi&quot;&gt;Get hugs of your own&lt;/a&gt;&lt;/small&gt;&lt;br clear=&quot;all&quot;&gt;&lt;br /&gt;&lt;br /&gt;&lt;br /&gt;&lt;br /&gt;&lt;a href=&quot;http://www.last.fm/user/p3nut/?chartstyle=myspace-pink&quot;&gt;&lt;img src=&quot;http://imagegen.last.fm/myspace-pink/recenttracks/p3nut.gif&quot; border=&quot;0&quot; /&gt;&lt;/a&gt;&lt;br /&gt;&lt;br /&gt;&lt;br /&gt;&lt;br /&gt;&lt;/center&gt; &lt;br /&gt;&lt;br /&gt;";
		//String test = "hey its Dani~... i &lt;3 u liek whoah &lt;3 &amp;";
		//String test = "<img src=\"http://www.orlyowl.com/orly.jpg\"> So if you care, the url is: <a href=\"http://rockinrobyn389.xanga.com\">http://rockinrobyn389.xanga.com</a>";
		//String test = "<img src=\"http://i39.photobucket.com/albums/e171/its_so_shocking/bfffest48.jpg\" alt=\"Photobucket - Video and Image Hosting\"><br><br><br /><img src=\"http://i39.photobucket.com/albums/e171/its_so_shocking/bfffest46.jpg\" alt=\"Photobucket - Video and Image Hosting\"><br><br><br /><img src=\"http://i39.photobucket.com/albums/e171/its_so_shocking/bfffest45.jpg\" alt=\"Photobucket - Video and Image Hosting\"><br><br><br />";
		//String test = "Do I have to? ha j to the k. Added.<br />p.s. my fear before. icon &gt; yours.";
		//String test = "<b><a href=\"http://www.independent.ie/world-news/europe/devil-is-at-work-in-vatican-says-churchs-top-exorcist-2095048.html\">The growing clerical sex abuse scandals in the Roman Catholic Church" +
		//	"are proof that the Devil is at work inside the Vatican, according to the Holy See's chief exorcist.</a></b><br /><br />No point snarking this, really, is there?";
		//String test = "<a href=\"http://www.vimeo.com/1227202?pg=embed&sec=1227202\">Teaser</a> from <a href=\"http://www.vimeo.com/user542290?pg=embed&sec=1227202\">Dr. Horrible&#039;s Sing-Along Blog</a> on <a href=\"http://vimeo.com?pg=embed&sec=1227202\">Vimeo</a>.";
		//System.out.println(HTML.stripTags(test));
		String test = "History definitely has value outside of academics. It&#x27;s been said that &#x3C;i&#x3E;&#x22;those that cannot remember the past are destined to repeat it&#x22;&#x3C;/i&#x3E;. It is crucial for us to learn from past mistakes and successes so we can build on those learnings to help us make better decisions";
		System.out.println(HTML.htmlToText(test));
	}
}
