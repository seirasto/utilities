package web;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import org.apache.xerces.parsers.DOMParser;
import org.w3c.dom.*;
import org.xml.sax.*;

public class Data {

	/**
	 * Return the entire webpage into one string
	 * 
	 * @param url
	 * @return String
	 */
	public static ArrayList<String> getPage(URL url) {

		String inputLine;
		ArrayList<String> webPage = new ArrayList<String>();

		//int count = 0;
		try {
			BufferedReader in = new BufferedReader(
					new InputStreamReader(
							url.openStream()));
			// Don't get such large pages to avoid crashing (they are usually irrelevant pages)
			while ((inputLine = in.readLine()) != null) { // && webPage.legnth() < 200000) {
				webPage.add(inputLine);
			}
			
			in.close();
		}catch(Exception e) {
			System.err.println("Error occurred in in Crawler.getPage on " + url + ": " + e);
		}
		
		return webPage;
	}
	
	public static String getPageAsString(URL url) {
		
		String webPage = "";
		String inputLine;
		
		try {
			BufferedReader in = new BufferedReader(
					new InputStreamReader(
							url.openStream()));

			while ((inputLine = in.readLine()) != null) {
				webPage += inputLine + "\n";
			}
			return webPage;
    	} catch (Exception e) {
    		System.err.println("Error occurred in data.getXMLPage for url " + url + ": " + e);
    	}
    	return null;
	}
	
	public static Document getXMLPage(URL url) {
		DOMParser parser = new DOMParser();
		
		try {
			parser.setFeature("http://xml.org/sax/features/namespaces", false);
		} catch (Exception e) {
			System.err.println("Error occurred in Data.getXMLPage " + e);
		}
		
    	InputSource in;

    	try {
    		InputStream raw = url.openStream();
    		in = new InputSource(raw);
    		in.setSystemId(url.getFile());
    		parser.parse(in);
    		return parser.getDocument();
    	} catch (Exception e) {
    		System.err.println("Error occurred in data.getXMLPage for url " + url.getPath() + ": " + e);
    	}
    	return null;
	}
}
