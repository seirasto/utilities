package processing;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

/**
 * This class runs Weiwei's sentence similarity code and computes cosine similarity on the sentence vectors 
 * @author sara
 *
 */
public class SentenceSimilarity {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SentenceSimilarity ss = new SentenceSimilarity();
		//String file = "/proj/nlp/users/sara/SCIL/SCs/Influencer/data/sc-annotated-lj-october2012/temp/processed/processed-100960.xml/100960.xml.txt";
		String file = "/proj/nlp/users/sara/agreement//res//processed/processed-Gap_(clothing_retailer)___Suspicious_random_deletes.xml/Gap_(clothing_retailer)___Suspicious_random_deletes.xml.txt";
		ss.run(file);
		System.out.println(ss.process(file + ".ss"));

		//String sbj = "/proj/nlp/users/sara/agreement/res/train/agreement.sbj.txt";
		//String tar = "/proj/nlp/users/sara/agreement/res/train/agreement.tar.txt";
		
		//sbj = ss.run(sbj);
		//tar = ss.run(tar);
		//System.out.println(ss.getSentenceSimilarity(sbj + ".ss", tar + ".ss", 5,5));
		
		//System.out.println(ss.run("This is\nthe first sentence.","This is\nthe second sentence."));
		//System.out.println(ss.run("This is the first sentence.","This is the second sentence."));
	}
	
	public String run (String file) {
		try {
			
			String tmp = "/tmp/" + new File(file).getName().replaceAll("\\(", "").replaceAll("\\)", "");
			Files.move(Paths.get(file), Paths.get(tmp),StandardCopyOption.REPLACE_EXISTING);
			
			 ProcessBuilder pb = new ProcessBuilder("perl","bin/overarching.pl","models/05-09",tmp, tmp + ".ss");
			 Map<String, String> env = pb.environment();
			 env.put("PATH", env.get("PATH") + ":/proj/nlp/fluke/SCIL/tools/ss/standalone/tools/itpp/bin");
			 env.put("LD_LIBRARY_PATH", env.get("LD_LIBRARY_PATH") + ":/proj/nlp/fluke/SCIL/tools/ss/standalone/tools/itpp/lib");
			 pb.directory(new File("/proj/nlp/fluke/SCIL/tools/ss/standalone/"));
			 Process p = pb.start();
			//Process p = Runtime.getRuntime().exec(,new File());
			BufferedReader bri = new BufferedReader(new InputStreamReader(p.getInputStream()));
			BufferedReader bre = new BufferedReader(new InputStreamReader(p.getErrorStream()));
		    
			String line;
			
			while ((line = bri.readLine()) != null) {
		        System.out.println(line);
		    }
		    bri.close();
		     
		    while ((line = bre.readLine()) != null) {
		    	System.out.println(line);
		    }
		    bre.close();
		    p.waitFor();
		    p.destroy();
			Files.move(Paths.get(tmp + ".ss"), Paths.get(file + ".ss"), StandardCopyOption.REPLACE_EXISTING);
			Files.move(Paths.get(tmp), Paths.get(file), StandardCopyOption.REPLACE_EXISTING);
			//Files.delete(Paths.get(tmp));
			Files.delete(Paths.get(tmp + ".mt"));
		} catch (Exception e) {
			System.err.println("[SentenceSimilarity.run] error running ss code.");
			e.printStackTrace();
		}
		return file + ".ss";
	}
	
	/**
	 * Run ss on two sentences
	 * @param sentence1
	 * @param sentence2
	 * @return
	 */
	public HashMap<String,Double> run (String sentence1, String sentence2) {
		
		// 1. create tmp file
		File tmp = new File("tmp.txt");

		try {
			BufferedWriter tmpFile = new BufferedWriter(new FileWriter(tmp));
			tmpFile.write(sentence1 + "\n" + sentence2 + "\n");
			tmpFile.close();
		} catch (Exception e) {
			System.err.println(e);
			e.printStackTrace();
		}
		// 2. run sentence similarity
		this.run(tmp.getAbsolutePath());
		
		// 3. process tmp file
		HashMap<String,Double> values = this.process(tmp.getAbsolutePath() + ".ss");
		
		// 4. remove tmp file
		tmp.delete();
		new File(tmp.getAbsolutePath() + ".ss");
		
		// 5. return sentence similarity (there is only one)
		return values;
	}
	
	public double runTwoSentence (String sentence1, String sentence2) {
		return run(sentence1,sentence2).get("0-1");
	}
	
	public HashMap<String,Double> process(String file) {
		
		HashMap<String,Double> values = new HashMap<String,Double>();
		
		List<String> vectors = GeneralUtils.readLines(file);
		vectors.remove(0);
		
		for (int i = 0; i < vectors.size(); i++) {
			
			String [] a = vectors.get(i).split(" ");
			
			for (int j = i+1; j < vectors.size(); j++) {
				String [] b = vectors.get(j).split(" ");
				//System.out.println((i+1) + "," + (j+1) + ": " + this.cosineSimilarity(a, b));
				values.put(i + "-" + j,SentenceSimilarity.cosineSimilarity(a, b));
			}
		}
		return values;
	}
	
	public double getSentenceSimilarity(String file, int s1, int s2) {
		List<String> vectors = GeneralUtils.readLines(file);
		return SentenceSimilarity.cosineSimilarity(vectors.get(s1).split(" "), vectors.get(s2).split(" "));
	}
	
	public double getSentenceSimilarity(String file1, String file2, int s1, int s2) {
		List<String> vectors1 = GeneralUtils.readLines(file1);
		List<String> vectors2 = GeneralUtils.readLines(file2);
		return SentenceSimilarity.cosineSimilarity(vectors1.get(s1+1).split(" "), vectors2.get(s2+1).split(" "));
	}
	
	private static double dot(String [] a, String [] b) {
		  
		double sum = 0;
		 
		for (int index = 0; index < a.length; index++)
		    sum += Double.valueOf(a[index]) * Double.valueOf(b[index]);
		return sum;
	}
		
	private static double norm(String [] a) {
	
		double sum = 0;
		
		for (int index = 0; index < a.length; index++)
		    sum += Double.valueOf(a[index]) * Double.valueOf(a[index]);
		return Math.sqrt(sum);
	}
	
	public static double cosineSimilarity(String [] a, String [] b) {
		return dot(a,b) / (norm(a) * norm(b));
	}
}
