/**
 * 
 */
package processing;

import java.util.ArrayList;
import java.util.regex.*;

/**
 * @author sara
 *
 */
public class StringProcessing {

	public static ArrayList<String> SentenceSplitter(String document) {
		
		ArrayList<String> sentences = new ArrayList<String>();
		
		Pattern p = Pattern.compile("[\\p{Space}\\p{Graph}&&[^.;?!]]*([;?!.]\\)?)*"); //Pattern.compile("[^(;|\\?|!|\\.)^\\s])*(;|\\?|!|\\.)( |^)");
		Matcher m = p.matcher(document);
	
		String sentence = "";
		
		while(m.find()) {
			
			//System.out.println("group: " + m.group());
			
			// there aren't sentences, keep it with the prev & next sentence
			if (m.group().trim().equals("")) continue;
			//else if (m.group().matches("(^|.* )(Mr|Dr|Mrs|Ms|Miss|Rev|Jan|Feb|Apr|Aug|Sept|Oct|Nov|Dec)\\.( |$)")) {
			else if (m.group().matches("(^|.* )(Sept|Inc|Mr|Dr|Mrs|Ms|Miss|Rev|Jan|Feb|Apr|Aug|Sept|Oct|Nov|Dec)\\.( |$)") 
					 || m.group().matches("([A-Z]*\\.)*")) {
				sentence += m.group();
			}
			else if (m.group().charAt(0) != ' ') {
				 //m.group().trim().indexOf(" ") == -1 || m.group().trim().length() < 3 || m.group().startsWith(",")
				//System.err.println("[StringProcessing.SentenceSplitter] One-Two word sentence, adding: " + m.group());
				sentence = (sentences.size() > 0 ? sentences.remove(sentences.size() - 1) : "") + sentence + m.group();
				sentences.add(sentence.trim());
				sentence = "";
			}
			else {
				
				//if (m.group().length() - m.group().lastIndexOf(" ") > 3 && m.group().length() - m.group().lastIndexOf(" ") < 5) {
				//	System.err.println("[StringProcessing.SentenceSplitter] possible special case: " + m.group());
				//}
				
				sentence += m.group();
				if (sentence.trim().equals("")) continue;

				int size = sentences.size();
				
				if (size > 0 && sentences.get(size-1).length() - sentences.get(size-1).lastIndexOf(" ") < 4) {
					sentences.set(size-1, sentences.get(size-1) + sentence);
				}
				else {
					sentences.add(sentence.trim());
				}
				sentence = "";
			}
		}
		
		/*for (int index = 0; index < sentences.size(); index++) {
			System.out.println("[" + (index + 1) + "] " + sentences.get(index));
		}*/
		
		return sentences;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		StringProcessing.SentenceSplitter("Mr. Vinken <3 will go to the store. Good job sir. See you later. That is so exciting!!!! Don't you think so? Woot :) I luv Mr. Vinken tonz <3 <3");
		System.out.println("---------------");
		StringProcessing.SentenceSplitter("Pierre A. Vinken, 61 years old, will join the board as a nonexecutive director Nov. 29." +
				" Mr. Vinken is chairman of Elsevier N.V., the Dutch publishing group. The U. station Inc. it is. Then Douglas R. Weeden is my friend. The U.S. is one of the best countries. "
				 + "The government's borrowing authority dropped at midnight Tuesday to $2.80 trillion from $2.87 trillion.");
		StringProcessing.SentenceSplitter("I just finished rereading The Summoning, the first Sisters of Isis book, for...oh, only the fifth or sixth time, I think., "
				+ "(Yeah, only., Lynne Ewing is my brand of author-crack!) As a result, I felt like fangirling., Me fangirling=me drawing., " + 
				"This picture didn't turn out quite how I wanted it to, and some parts still make me cringe, but hey, that's what happens when you slack off from drawing!");
		                                   
	}
}
