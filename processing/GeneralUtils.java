package processing;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class GeneralUtils {

	public static List<String> readLines(String filename) {
		BufferedReader reader = new BufferedReader(getFileReader(filename));
		List<String> lines = new ArrayList<String>();
		try {
			while (reader.ready()) {
				lines.add(reader.readLine());
			}
			reader.close();
		} catch (IOException e) {
			throw new RuntimeException("Cannot read from file " + filename, e);
		}
		return lines;
	}

	private static InputStreamReader getFileReader(String filename) {
		try {
			return new InputStreamReader(new FileInputStream(filename),"UTF-8");
		} catch (Exception e) {
			throw new RuntimeException("File not found: " + filename, e);
		}		
	}
	
	public static Properties getProperties(String config) {
		Properties prop = new Properties();
		try {
			InputStream input = new FileInputStream("config/config.properties");
			prop.load(input);
			input.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return prop;
	}
}
