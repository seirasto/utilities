package processing;

import java.io.*;
import java.util.*;

public class SwapEmoticons
{
	Hashtable<String,String> emoticonKey;
	Hashtable<String,String> codeKey;
	
	public SwapEmoticons() throws Exception {
		try {
			Properties prop = GeneralUtils.getProperties("config/config.properties");
			String directory = prop.getProperty("emoticons_directory");
			emoticonKey = readCodes(new File(directory + "/emoticons.txt.codes"), true);
			codeKey = readCodes(new File(directory + "/emoticons.txt.codes"), false);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public SwapEmoticons(String file) throws Exception {
		emoticonKey = readCodes(new File(file), true);
		codeKey = readCodes(new File(file), false);
	}
	
	public String getEmoticon(String code) {
		return codeKey.get(code);
	}
	
	public String getCode(String emoticon) {
		return emoticonKey.get(emoticon);
	}
	
	/**
	 * converts file from emoticons to text or visa versa
	 * 
	 * @param text
	 * @return
	 * @throws Exception
	 */
	public String swapEmoticons(String text, boolean swapOut) throws Exception {
		
		Hashtable<String,String> codes = swapOut ? emoticonKey : codeKey;
		String modifiedText = "";
		
		StringTokenizer tokenizer = new StringTokenizer(text," \t\n\f\r",true);
		//String [] words = text.split("\\s+");
		
		//for (String word : words) {
		while (tokenizer.hasMoreTokens()) {
			String word = tokenizer.nextToken();
			if (codes.containsKey(word)) {
				//System.out.println(word);
				modifiedText += codes.get(word);
			}
			else modifiedText += word;
		}
		return modifiedText.trim();
	}
	
	/**
	 * converts file from emoticons to text or visa versa
	 * 
	 * @param text
	 * @return
	 * @throws Exception
	 */
	public String swapChunkedEmoticons(String text) throws Exception {
		
		Hashtable<String,String> codes = codeKey;
		String modifiedText = "";
		
		StringTokenizer tokenizer = new StringTokenizer(text," \t\n\f\r",true);

		while (tokenizer.hasMoreTokens()) {
			String chunk = tokenizer.nextToken();
			
			try {
				if (chunk.indexOf("/") >= 0 && codes.containsKey(chunk.substring(0,chunk.indexOf("/")))) {
					modifiedText += codes.get(chunk.substring(0,chunk.indexOf("/"))) + chunk.substring(chunk.indexOf("/"));
				}
				else modifiedText += chunk;
			} catch (Exception e) {
				System.err.println("[SwapEmoticons.swapChunkedEmoticons] error on chunk: " + chunk);
				e.printStackTrace();
			}
		}
		return modifiedText.trim();
	}
	
	public String swapEmoticons(File fileName, boolean swapOut, boolean chunked) throws Exception {
		return swapEmoticons(fileName,fileName + ".emo",swapOut,chunked);
	}
	
	public String swapEmoticons(File fileName, String outputFile, boolean swapOut, boolean chunked) throws Exception {
		
		BufferedReader in = new BufferedReader(new FileReader(fileName));
    	
    	BufferedWriter out = new BufferedWriter(new FileWriter(outputFile));
    	
        String inputLine;
        
        while ((inputLine = in.readLine()) != null) {
        	if (chunked) out.write(this.swapChunkedEmoticons(inputLine) + "\n");
        	else out.write(this.swapEmoticons(inputLine, swapOut) + "\n");
        }
        out.close();
        in.close();
		return fileName + ".emo";
	}
	
	public static File createEmoticonCodes(File aFile) throws Exception {
	
		Hashtable<String,String> codes = new Hashtable<String,String>();
				
		int counter = 100;
		
		Scanner s = new Scanner(aFile);
		
		while(s.hasNextLine()) {
			String nextLine = s.nextLine();
			
			int index = nextLine.indexOf("\t");
			
			if (index < 0) {
				System.err.println("[SwapEmoticons.createEmoticonCodes] Invalide Code: " + nextLine);
				continue;
			}
			
			String emoticon = nextLine.substring(0,index);
			
			String code = nextLine.substring(index+1);
			//System.out.println("old code: " + code);
			code = code.replaceAll("\\s", "_");
			code = code.replaceAll("[^A-Za-z_]", "");
			code = counter + "_" + code;
			
			// to avoid duplicate codes
			if (!codes.containsKey(emoticon)) codes.put(emoticon,code);
			//System.out.println("new code: " + code);
			
			counter++;
		}
		
		s.close();
		
		File outputFile = new File(aFile + ".codes");
		FileOutputStream fos = new FileOutputStream(outputFile);
		PrintStream p;
		p = new PrintStream(fos);
		
		Iterator<String> codeIterator = codes.keySet().iterator();
		
		while(codeIterator.hasNext()) {
			Object emoticon = codeIterator.next();
			String code = codes.get(emoticon);
			
			p.println(emoticon + ", " + code);
		}
		
		p.close();
			
		return aFile;
	}
	
	public static Hashtable<String,String> readCodes(File aFile, boolean emoticonKey) throws Exception {
		
		Hashtable<String,String> codes = new Hashtable<String,String>();
		Scanner s = new Scanner(aFile);
		
		while(s.hasNextLine())
		{
			String nextLine = s.nextLine();
			int comma = nextLine.lastIndexOf(",");
			String emoticon = nextLine.substring(0, comma).trim();
			String code = nextLine.substring(comma+1).trim();
			
			if (emoticonKey) codes.put(emoticon, code);
			else codes.put(code, emoticon);
		}
		s.close();
		return codes;
	}
	
	public Enumeration<String> emoticons() {
		return emoticonKey.keys();
	}
	
	public Enumeration<String> codes() {
		return codeKey.keys();
	}
	
	public static void main(String[] args) {
	
		try {
			
			//SwapEmoticons.createEmoticonCodes(new File("/proj/nlp/users/sara/dictionaries/emoticons/emoticons.txt"));
			SwapEmoticons swapper = new SwapEmoticons();
			//String sentence = "(-D Awesome :) ;) dude! \n:-O\n Thanks for doing that ^_^ (I <3 U!)";
			String sentence = "a huge delay for the movie again :/, was so close!";
			String swapped = swapper.swapEmoticons(sentence,true);
			System.out.println("SWAPPED IN:\n" + swapped);
			String swappedout = swapper.swapEmoticons(swapped, false);
			System.out.println("SWAPPED OUT:\n " + swappedout);
			System.out.println("SUCCESS? " + sentence.equals(swappedout));
			//System.out.println("CHUNK TEST\n" + swapper.swapChunkedEmoticons("101_Happy_midget_smiley/CD/B-NP best/JJ/B-NP friend/NN/I-NP"));
		} catch (Exception e) {
			System.err.println("[SwapEmoticons.main] " + e);
			e.printStackTrace();
		}
	}

 }
