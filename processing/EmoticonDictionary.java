/**
 * 
 */
package processing;

import java.util.*;
import java.io.*;

/**
 * @author sara
 *
 */
public class EmoticonDictionary {
	
	//public static String DICTIONARY = "/proj/nlp/users/sara/dictionaries/emoticons/emoticon_dictionary.txt";
	private Hashtable<String,String> emoticons;
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	
	public EmoticonDictionary() throws Exception {
		Properties prop = GeneralUtils.getProperties("config/config.properties");
		String directory = prop.getProperty("emoticons_directory");
		load(directory + "/emoticon_dictionary.txt");
	}
	
	public EmoticonDictionary(String dictionaryFile) {
		load(dictionaryFile);
	}
	
	public void load(String dictionaryFile) {	
		emoticons = new Hashtable<String,String>();
		
		try {
			BufferedReader in = new BufferedReader(new FileReader(dictionaryFile));
			
			String s = null;
			
			while((s = in.readLine()) != null)
			{
				String [] emoticon = s.split("\t");
				emoticons.put(emoticon[0], emoticon[1]);
			}
			in.close();
		} catch (Exception e) {
			System.err.println("[EmoticonDictionary] " + e);
			e.printStackTrace();
		}
	}
	
	public boolean hasEmoticon(String emoticon) {
		return emoticons.containsKey(emoticon);
	}
	
	public String getDefinition(String emoticon) {
		return emoticons.get(emoticon);
	}

}
