package processing;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.io.*;

import edu.stanford.nlp.ling.Word;
//import edu.stanford.nlp.process.DocumentPreprocessor;
import edu.stanford.nlp.process.*;

/**
 * @author Or Biran
 * 
 * Provides various NLP-related utility methods.
 */
public class NlpUtils {

	public static final String SPACE = " ";
	
	public static final String[] ENCLOSURES = {"(", ")", "{", "}", "[", "]", "\""};
	public static final String[] GARBAGE = {"\\", "/"};
	
	public static String preprocess(String text) {
		
		String cleanText = "";
		for (Word word : getStanfordWords(text)) {
			cleanText += word + SPACE;
		}
		cleanText = cleanText.trim();

		return cleanText;
	}
	
	public static String preprocessing(String text) {
		try {
		 text = new SwapEmoticons().swapEmoticons(text, true);
		 text = convertBrackets(text);
		 Properties prop = GeneralUtils.getProperties("config/config.properties");
		 text = new Contractions(
				 prop.getProperty("contractions_directory") + "/contractions.txt").swapContractions(text);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return text;
	}
	
	public static String postprocessing(String text) {
		try {
		 text = revertBrackets(new SwapEmoticons().swapEmoticons(text, false));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return text;
	}
	
	public static void convertBracketsFile(String file) throws Exception {
		List<String> lines = GeneralUtils.readLines(file);
    	
    	BufferedWriter out = new BufferedWriter(new FileWriter(file));
    	
        for (String line : lines ) {
        	out.write(convertBrackets(convertSlash(line)) + "\n");
        }
        out.close();
	}
	
	public static void revertBracketsFile(String file) throws Exception {
		List<String> lines = GeneralUtils.readLines(file);
    	
    	BufferedWriter out = new BufferedWriter(new FileWriter(file));
    	
        for (String line : lines ) {
        	out.write(revertBrackets(revertSlash(line)) + "\n");
        }
        out.close();
	}
		
	public static String convertSlash(String string) {
		string = string.replace("/","FSSLASHH");
		string = string.replace("\\","BSSLASHH");
		string = string.replace("'","SQQUOTEE");
		string = string.replace("\"","DQQUOTEE");
		string = string.replace("`","LQQUOTEE");

		string = string.replace("@", "AAATTT");
		string = string.replace("...", "EEELIPSESSS");
		return string;
	}
	
	public static String revertSlash(String string) {
		string = string.replace("FSSLASHH","/");
		string = string.replace("BSSLASHH","\\");
		string = string.replace("SQQUOTEE","'");
		string = string.replace("DQQUOTEE","\"");
		string = string.replace("LQQUOTEE","`");
		
		string = string.replace("AAATTT", "@");
		string = string.replace("EEELIPSESSS", "...");
		return string;
	}
	
	public static String convertBrackets(String string) {
		string = string.replace("(", "BRACKETRRP");
		string = string.replace("[", "BRACKETRRB");
		string = string.replace("{", "BRACKETRRC");
		string = string.replace("<", "BRACKETRRL");
		string = string.replace(")", "BRACKETLRP");
		string = string.replace("]", "BRACKETLRB");
		string = string.replace("}", "BRACKETLRC");
		string = string.replace(">", "BRACKETLRL");
		return string;
	}
	
	public static String revertBrackets(String string) {
		string = string.replace("BRACKETRRP", "(");
		string = string.replace("BRACKETRRB", "[");
		string = string.replace("BRACKETRRC", "{");
		string = string.replace("BRACKETRRL", "<");
		string = string.replace("BRACKETLRP", ")");
		string = string.replace("BRACKETLRB", "]");
		string = string.replace("BRACKETLRC", "}");
		string = string.replace("BRACKETLRL", ">");
		return string;
	}
	
	public static List<String> splitSentences(String text) {
		
		List<Word> words = getStanfordWords(text);
		
		WordToSentenceProcessor<Word> processor = new WordToSentenceProcessor<Word>();
		List<List<Word>> sents = processor.process(words);

		List<String> sentences = new ArrayList<String>();
		
		for (List<Word> sent : sents) {
			String sentence = "";
			for (Word w : sent) {
				sentence += w + SPACE;
			}
			sentence = sentence.trim();
			if (sentence.matches("\\p{Punct}+") && !sentences.isEmpty())
				sentences.set(sentences.size()-1,sentences.get(sentences.size()-1)+sentence);
			else
				sentences.add(sentence);
		}
		
		return sentences;
	}
	
	public static List<Word> getStanfordWords(String text) {
		return PTBTokenizer.newPTBTokenizer(new StringReader(text)).tokenize();
		//DocumentPreprocessor preprocessor = new DocumentPreprocessor(new StringReader(text));
		//return preprocessor.getWordsFromString(text);
	}

	public static List<String> getWords(String text) {
		List<String> stringWords = new ArrayList<String>();
		
		for (Word word : getStanfordWords(text)) {
			stringWords.add(word.word());
		}
		
		return stringWords;
	}
	
	/*public static String stem(String word) {
		return Porter.stripAffixes(word);
	}*/

	public static String normalizeLetters(String word) {
		String w = word;
		
		w = w.replace("���", "a");
		w = w.replace("��", "e");
		w = w.replace("��", "i");
		w = w.replace("��", "o");
		w = w.replace("��", "u");
		
		w = w.replace("��", "a");
		w = w.replace("��", "e");
		w = w.replace("��", "i");
		w = w.replace("��", "o");
		w = w.replace("��", "u");
		w = w.replace("��", "y");
		
		w = w.replace("���", "a");
		w = w.replace("��", "e");
		w = w.replace("��", "i");
		w = w.replace("��", "o");
		w = w.replace("��", "u");
		
		w = w.replace("���", "a");
		w = w.replace("��", "e");
		w = w.replace("��", "i");
		w = w.replace("��", "o");
		w = w.replace("��", "u");

		w = w.replace("���", "a");
		w = w.replace("��", "o");
		w = w.replace("��", "n");

		w = w.replace("��", "a");
		
		w = w.replace("��", "o");
		
		w = w.replace("��", "c");

		w = w.replace("��", "s");
		w = w.replace("��", "z");

		w = w.replace("��", "ae");
		w = w.replace("��", "oe");
		w = w.replace("���", "ss");
		w = w.replace("���", "th");
		w = w.replace("���", "th");

		return w;
	}

	public static String removeEnclosures(String word) {
		String proc = word;
		for (String enclosure : ENCLOSURES) {
			proc = proc.replace(enclosure, "");
		}
		return proc;
	}

	public static String removeGarbage(String word) {
		String proc = word;
		for (String garbage : GARBAGE) {
			proc = proc.replace(garbage, "");
		}
		return proc;		
	}
	
	public static void main (String [] args) {
		//System.out.println(System.getProperty("user.dir"));
		//System.out.println(NlpUtils.preprocessing("blah blah can't"));
		System.out.println(NlpUtils.splitSentences("Happy Bithday!!!!!!!"));
		System.out.println(NlpUtils.splitSentences("Actually, I already accumulated the decade... (And another third of one!) I'm actually turning 33, the coming of age for hobbits. :)"));
		System.out.println(NlpUtils.splitSentences("I'm starting a new thread to break away from the above discussion, which, for the most part, has turned from the subject. Hopefully, I've succeeded in compiling a list of all the images of abortion-related protests currently hosted on Wikipedia, so that we might pick and choose from among them. - "));
	}
}
