package processing;

import java.io.*;
import java.util.*;

public class Contractions {

	Hashtable<String,String> _contractions;
	
	public Contractions(boolean parsed) throws Exception {
		try {
			Properties prop = GeneralUtils.getProperties("config/config.properties");
			String directory = prop.getProperty("contractions_directory");
			_contractions = readCodes(new File(directory + (parsed ? "contractions-parserversion.txt" : "/contractions.txt")));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public Contractions() throws Exception {
		this(false);
	}
	
	public Contractions(String file) throws Exception {
		_contractions = readCodes(new File(file));
	}
	
	/**
	 * converts file from emoticons to text or visa versa
	 * 
	 * @param text
	 * @return
	 * @throws Exception
	 */
	public String swapContractions(String text) throws Exception {
		
		Iterator<String> it = _contractions.keySet().iterator();
		
		while (it.hasNext()) {
			
			String key = (String)it.next();
			String value = _contractions.get(key);
			
			// lowercase
			text = text.replaceAll(key, value);
			// capital
			text = text.replaceAll(key.substring(0,1).toUpperCase() + key.substring(1), 
					value.substring(0,1).toUpperCase() + value.substring(1));
		}
 		
		return text.trim();
	}
	
	public String swapContractions(File fileName, String outputFile) throws Exception {
		
		BufferedReader in = new BufferedReader(new FileReader(fileName));
    	
    	BufferedWriter out = new BufferedWriter(new FileWriter(outputFile));
    	
        String inputLine;
        
        while ((inputLine = in.readLine()) != null) {
        	out.write(this.swapContractions(inputLine) + "\n");
        }
        out.close();
        in.close();
		return fileName + ".con";
	}
	
	public String swapContractions(File fileName) throws Exception {
		return swapContractions(fileName,fileName + ".con");
	}
	
	public static Hashtable<String,String> readCodes(File aFile) throws Exception {
		
		Hashtable<String,String> codes = new Hashtable<String,String>();
		Scanner s = new Scanner(aFile);
		
		while(s.hasNextLine())
		{
			String nextLine = s.nextLine();
			int tab = nextLine.lastIndexOf("\t");
			String contraction = nextLine.substring(0, tab).trim();
			String fullPhrase = nextLine.substring(tab+1).trim();
			fullPhrase = fullPhrase.indexOf(";") >= 0 ? fullPhrase.substring(0,fullPhrase.indexOf(";")) : fullPhrase;
			
			codes.put(contraction,fullPhrase);
		}
		
		return codes;
	}
	
	public static void main(String[] args) {
		try {
			String sentence = "Hi, I ca n't come today... You 're the best though - he's ok";
			System.out.println("ORIGINAL:\n " + sentence);
			String file = "/proj/nlp/users/sara/dictionaries/contractions/contractions-parserversion.txt";
			Contractions c = new Contractions(file);
			String swapped = c.swapContractions(sentence);
			System.out.println("SWAPPED:\n" + swapped);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
