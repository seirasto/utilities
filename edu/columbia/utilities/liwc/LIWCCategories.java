/**
 * 
 */
package edu.columbia.utilities.liwc;

import java.util.*;

import processing.GeneralUtils;

/**
 * @author sara
 *
 */
public class LIWCCategories {

	HashMap<String,String> _categories;
	
	public LIWCCategories(String directory) {
		_categories = new HashMap<String,String>();
		List<String> lines = GeneralUtils.readLines(directory + "/liwccat2007.txt");
		
		for (int index = 1; index < lines.size()-1; index++) {
			String [] cat = lines.get(index).split("\t");
			_categories.put(cat[0], cat[1]);
		}
	}
	
	public String getCategory(String c) {
		return _categories.get(c);
	}
	
	public Collection<String> getCategories() {
		return _categories.values();
	}
}
