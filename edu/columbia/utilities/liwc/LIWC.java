package edu.columbia.utilities.liwc;

import java.util.*;

import processing.NlpUtils;

public class LIWC {

	LIWCCategories _categories;
	LIWCDictionary _dictionary;
	
	public LIWC(String directory) {
		_categories = new LIWCCategories(directory);
		_dictionary = new LIWCDictionary(directory);
	}
	
	public HashMap<String,Double> generateFeatureCounts(String text, String prefix) {
		
		HashMap<String,Double> features = new HashMap<String,Double>();
		
		for (String category : _categories.getCategories()) {
			features.put(prefix + category,0.0);
		}
		
		List<String> words = NlpUtils.getWords(text);
		
		for (String word : words) {
			String [] categories = _dictionary.getWord(word);
			
			if (categories == null) continue;
			
			for (String category : categories) {
				if (!features.containsKey(prefix + _categories.getCategory(category))) {
					if (_categories.getCategory(category) != null) System.err.println("Missing Category: " + _categories.getCategory(category));
					continue;
				}
				features.put(prefix + _categories.getCategory(category), features.get(prefix + _categories.getCategory(category))+1);
			}
		}
		return features;
	}
	
	public HashMap<String,Double> generateFeatureCounts(String text) {
		return generateFeatureCounts(text,"");
	}
	
	public HashSet <String> getCategories(String prefix) {
		HashSet<String> categories = new HashSet<String>();
		
		for (String category : _categories.getCategories()) 
			categories.add(prefix + category);
		return categories;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String text = "I hope so. I'm tired of people saying, \"Does a bear poop in the woods?\"; I think it's about time we changed that saying to, \"Does big-foot poo in the woods?\" But that's just me. I'm weird that way..., especially at 3 in the frigging morning when I can't fall asleep. Deal with it ;)";
		
		LIWC liwc = new LIWC("/proj/speech/tools/LIWC2007.app/Contents/Resources");
		HashMap<String,Double> counts = liwc.generateFeatureCounts(text);
		
		for (String feature : counts.keySet()) {
			System.out.println(feature + ": " + counts.get(feature));
		}
	}
}
