package edu.columbia.utilities.liwc;

import java.util.*;

import processing.GeneralUtils;


/**
 * Interface to the LIWC dictionary
 *  
 * @author Sara Rosenthal
 * @version 1.00
 */
public class LIWCDictionary {
	
	HashMap<String,String []> _dictionary;
	HashMap<String,String []> _dictionaryPatterns;
	ArrayList<String> _alphabeticPatterns;

	public LIWCDictionary(String directory) {
		
		_dictionary = new HashMap<String,String []>();
		_dictionaryPatterns = new HashMap<String,String []>();
		_alphabeticPatterns = new ArrayList<String>();
		
		List<String> lines = GeneralUtils.readLines(directory + "/liwcdic2007.dic");
		
		for (String line : lines) {
			String word = line.substring(0,line.indexOf("\t")).trim();
			String [] categories = line.substring(line.indexOf("\t")).trim().
					replaceAll("/", "\t").replaceAll("<of>", "").replaceAll("\\(02/134\\)","").split("\t");
			
			if (word.endsWith("*")) { 
				_dictionaryPatterns.put(word, categories);
				_alphabeticPatterns.add(word);
			}
			else  _dictionary.put(word, categories);
		}
	}
	
	public String [] getWord(String word) {
		if (!word.toLowerCase().matches("[a-z']+\\*?")) return null;
		if (_dictionary.containsKey(word.toLowerCase())) 
				return _dictionary.get(word.toLowerCase()); 
		
		for (String pattern : _alphabeticPatterns) {
			if (word.compareToIgnoreCase(pattern) < 0 && !pattern.endsWith("*")) return null;
			if (word.toLowerCase().matches(pattern.replace("*",".*"))) return _dictionaryPatterns.get(pattern);
		}
		
		return null;
	}
}