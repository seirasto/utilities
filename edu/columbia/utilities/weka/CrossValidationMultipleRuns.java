package edu.columbia.utilities.weka;

import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;
import weka.core.Utils;
import weka.classifiers.AbstractClassifier;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Remove;
import weka.core.converters.ArffSaver;

import java.util.Random;
//import java.text.*;
import java.io.*;
//import java.util.*;

/**
 * Performs multiple runs of cross-validation.
 *
 * Command-line parameters:
 * <ul>
 *    <li>-t filename - the dataset to use</li>
 *    <li>-x int - the number of folds to use</li>
 *    <li>-r int - the number of runs to perform</li>
 *    <li>-c int - the class index, "first" and "last" are accepted as well;
 *    "last" is used by default</li>
 *    <li>-W classifier - classname and options, enclosed by double quotes; 
 *    the classifier to cross-validate</li>
 * </ul>
 *
 * Example command-line:
 * <pre>
 * java CrossValidationMultipleRuns -t labor.arff -c last -x 10 -r 10 -W "weka.classifiers.trees.J48 -C 0.25"
 * </pre>
 *
 * @author FracPete (fracpete at waikato dot ac dot nz)
 */
public class CrossValidationMultipleRuns {

  /**
   * Performs the cross-validation. See Javadoc of class for information
   * on command-line parameters.
   *
   * @param args        the command-line parameters
   * @throws Excecption if something goes wrong
   */
  public static void main(String[] args) throws Exception {
    // loads data and set class index
    Instances data1 = DataSource.read(Utils.getOption("V1", args));
    Instances data2 = DataSource.read(Utils.getOption("V2", args));
    String clsIndex = Utils.getOption("c", args);
    if (clsIndex.length() == 0)
      clsIndex = "last";
    if (clsIndex.equals("first")) {
      data1.setClassIndex(0);
      data2.setClassIndex(1);
    }
    else if (clsIndex.equals("last")) {
      data1.setClassIndex(data1.numAttributes() - 1);
      data2.setClassIndex(data2.numAttributes() - 1);      
    }
    else {
      data1.setClassIndex(Integer.parseInt(clsIndex) - 1);
      data2.setClassIndex(Integer.parseInt(clsIndex) - 1);      
    }

    // classifier
    String[] tmpOptions;
    String classname;
    tmpOptions     = Utils.splitOptions(Utils.getOption("W", args));
    classname      = tmpOptions[0];
    tmpOptions[0]  = "";
    Classifier cls = (Classifier) Utils.forName(Classifier.class, classname, tmpOptions);

    // other options
    int runs  = Integer.parseInt(Utils.getOption("r", args));
    int folds = Integer.parseInt(Utils.getOption("x", args));
    
    CrossValidationMultipleRuns crossValidation = new CrossValidationMultipleRuns();
    Evaluation [] V1 = crossValidation.PerformCrossValidation(cls, data1, runs, folds);
    Evaluation [] V2 = crossValidation.PerformCrossValidation(cls, data2, runs, folds);
    statistics.SignificanceTests.ttest(V1, V2);
  }
  
  public static Instances removeAttribute(Instances instances, String attribute) {
	  Remove remove = new Remove();
	  Instances newInstances = null;
	  
	  try {
		  remove.setAttributeIndices(attribute);
		  remove.setInputFormat(instances);
		  newInstances = Filter.useFilter(instances, remove);
	  } catch (Exception e) {
		System.err.println("[CrossValidationMultipleRuns.removeAttribute] " + e);  
	  }

	  return newInstances; 
  }
  
  public static void writeToFile(Instances instances, String outputFile) {
	  try {
		  ArffSaver saver = new ArffSaver();
		  saver.setInstances(instances);
		  saver.setFile(new File(outputFile));
		  saver.writeBatch();
	  } catch (Exception e) {
		  System.err.println("[FastWeka.writeToFile]" + e);
		  e.printStackTrace();
	  }
  }

  public double [] PerformWekaCrossValidation(Classifier classifier, Instances data, int runs, int folds) {
	  
	  System.out.println("Classifier: " + classifier.getClass().getName()); // + " " + 
			 // Utils.joinOptions(classifier.getOptions()));
      System.out.println("Dataset: " + data.relationName());
      System.out.println("Folds: " + folds);
	  
	  double [] V = new double[runs];
	  
	  //perform cross-validation
      for (int index = 0; index < runs; index++) {
    	  
    	  try {
			  Evaluation evaluation = new Evaluation(data);
			  evaluation.crossValidateModel(classifier, data, folds, new Random(1));
			  System.out.println(evaluation.toSummaryString());
			  System.out.println(evaluation.toClassDetailsString());
		      System.out.println(evaluation.toMatrixString());
		      V[index] = evaluation.pctCorrect();
    	  } catch (Exception e) {
    		  System.err.println("[CrossValidationMultiplRuns.PerformWekaCrossValidation]"  + e);
    	  }
      }
      return V;
  }
  
  public Evaluation [] PerformCrossValidation(Classifier cls, Instances data, int runs, int folds) throws Exception {
	  
	  System.out.println("Classifier: " + cls.getClass().getName()); // + " " + Utils.joinOptions(cls.getOptions()));
      System.out.println("Dataset: " + data.relationName());
      System.out.println("Folds: " + folds);
  	
      Evaluation [] V = new Evaluation[runs];
      
     /* Hashtable<String,int []> avgSM = new Hashtable<String,int []>();
      avgSM.put("TP", new int [] {0,0,0,0,0,0,0,0,0,0,0,0});
      avgSM.put("TN", new int [] {0,0,0,0,0,0,0,0,0,0,0,0});
      avgSM.put("FP", new int [] {0,0,0,0,0,0,0,0,0,0,0,0});
      avgSM.put("FN", new int [] {0,0,0,0,0,0,0,0,0,0,0,0}); */
      
      //Hashtable<String,String []> stats = readSMStats();
      
      // perform cross-validation
      for (int index = 0; index < runs; index++) {
    	  System.out.print("Starting run " + (index+1) + "...");
	      // randomize data
	      int seed = index + 1;
	      Random rand = new Random(seed);
	      Instances randData = new Instances(data);
	      randData.randomize(rand);
	      if (randData.classAttribute().isNominal())
	        randData.stratify(folds);

	      Evaluation eval = new Evaluation(randData);
	      
	     /* Hashtable<String,int []> sm = new Hashtable<String,int []>();
	        sm.put("TP", new int [] {0,0,0,0,0,0,0,0,0,0,0,0});
	        sm.put("TN", new int [] {0,0,0,0,0,0,0,0,0,0,0,0});
	        sm.put("FP", new int [] {0,0,0,0,0,0,0,0,0,0,0,0});
	        sm.put("FN", new int [] {0,0,0,0,0,0,0,0,0,0,0,0}); */
	      
	      for (int n = 0; n < folds; n++) {
	
			  Instances train = randData.trainCV(folds, n, rand);
			  Instances test = randData.testCV(folds, n);

			  // the above code is used by the StratifiedRemoveFolds filter, the
			  // code below by the Explorer/Experimenter:
			  // Instances train = randData.trainCV(folds, n, rand);

		      // build and evaluate classifier
		      Classifier clsCopy = AbstractClassifier.makeCopy(cls);
		      clsCopy.buildClassifier(train);
		      //double [] predictions = 
		    	  eval.evaluateModel(clsCopy, test);
		        
		      //analyze(test,index,n,predictions,stats,sm,avgSM);
		        
		      System.out.print(n + "..");
	      }
	      //System.err.println();
	      System.out.println("Done");
	      
	      /*System.out.println("TP: " + printArray(sm.get("TP"), false));
	      System.out.println("TN: " + printArray(sm.get("TN"), false));
	      System.out.println("FP: " + printArray(sm.get("FP"), false));
	      System.out.println("FN: " + printArray(sm.get("FN"), false));*/
	      
	      // output evaluation
	      //System.out.println();
	      //System.out.println("=== Setup run " + (i+1) + " ===");
	      //System.out.println("Seed: " + seed);
	      //System.out.println();
	      //DecimalFormat df = new DecimalFormat("##.##");
	      //System.out.println("Precision: " + df.format(eval.precision(0)) + " " + df.format(eval.precision(1)));
	      //System.out.print(df.format(eval.pctCorrect()) + ", ");
	      System.out.println(eval.toSummaryString());
	      System.out.println(eval.toClassDetailsString());
	      System.out.println(eval.toMatrixString());
	      //V[index] = eval.pctCorrect();
	      V[index] = eval;
	      
	      //System.out.println(eval.toSummaryString("=== " + folds + "-fold Cross-validation run " + (i+1) + "=== ", false));
	   }
      System.out.println();
      
    /*  System.out.println("TP: " + printArray(avgSM.get("TP"), true));
      System.out.println("TN: " + printArray(avgSM.get("TN"), true));
      System.out.println("FP: " + printArray(avgSM.get("FP"), true));
      System.out.println("FN: " + printArray(avgSM.get("FN"), true)); */
      
      return V;
  }
  
 /* private String printArray(int [] array, boolean avg) {
	  
	  String string = "";
	  
	  for (int a : array) {
		  if (avg) string += a/10 + ",%,";
		  else string += a + ",%,";
	  }
	  return string;
  }
  
  private Hashtable<String, String []> readSMStats() {
	  
	  Hashtable<String,String []> stats = new Hashtable<String,String []>();
	  
	  try {
		  
		  BufferedReader in = new BufferedReader(new FileReader("/proj/nlp/users/sara/sentiment/naacl/subjectivity/sm-stats/cur.txt"));
		  
		  String data = "";
		  
		  while ((data = in.readLine()) != null) {
			  String [] parts = data.split(":");
			  stats.put(parts[0],parts[1].split(","));
		  }
	  } catch (Exception e) {
		  e.printStackTrace();
	  }
	  return stats;
  }
  
private void getStatInfo(Instances test, int run, int fold) {
	  
	  try {

		  BufferedWriter out = new BufferedWriter(new FileWriter("/proj/nlp/users/sara/sentiment/naacl/subjectivity/twitter.txt",true));
		  
		  if (test.attribute("emoticons") != null) {
		        for (int i = 0; i < test.numInstances(); i++) {
		        	out.write(run + "-" + fold + "-" + i + ":"); 
		        	for (int j = 6; j < 18; j++) {
		        		if (test.instance(i).value(j) == 0) 
		        			out.write("N,");
		        		else
		        			out.write("Y,");
		        	}
		        	out.write("\n");
		        }
	      }
		  out.close();
	  }
	  catch (Exception e) {
		  e.printStackTrace();
	  }
  }
  
  private void analyze(Instances test, int run, int fold, double [] predictions, Hashtable<String,String []> stats, Hashtable<String, int []> sm, Hashtable<String, int []> avgSM) {
	    for (int i = 0; i < test.numInstances(); i++) {	
	    	
	    	String [] hasSM = stats.get(run + "-" + fold + "-" + i);
	    	
	    	for (int j = 0; j < hasSM.length; j++) {
	    		String type = "FN";
	    		
	    		if (hasSM[j].equals("N")) continue;
	    		
	    		if (test.instance(i).value(test.instance(i).classIndex()) == 0 && predictions[i] == 0) type = "TP";
	    		if (test.instance(i).value(test.instance(i).classIndex()) == 1 && predictions[i] == 1) type = "TN";
	    		if (test.instance(i).value(test.instance(i).classIndex()) == 1 && predictions[i] == 0) type = "FP";
	    		
	    		int [] features = sm.get(type);
	    		int [] avgFeatures = avgSM.get(type);
	  			features[j]++;
	  			avgFeatures[j]++;
	  			sm.put(type, features);
	  			avgSM.put(type, avgFeatures);
	    	}
	    }
  }*/
}

