package edu.columbia.utilities.weka;

import java.util.*;

import weka.core.Attribute;
import weka.core.Instances;

public class DomainAdaptation {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	
	/**
	 * Keep initial instance as general add source and target with only one having a value
	 * daume07easyadapt.pdf Frustratingly Easy Domain Adaptation
	 * two sources
	 * requires domain attribute
	 * @param instances
	 * @param targetType
	 * @return
	 */
	public static Instances easyDomainAdaptation(Instances instances, String domain) {
		
		for (int index = 0; index < instances.numAttributes(); index++) {
			
			Attribute general = instances.attribute(index);
			Attribute source = new Attribute(general.name() + "_source");
			Attribute target = new Attribute(general.name() + "_target");
			
			instances.insertAttributeAt(source, instances.numAttributes()-2);
			instances.insertAttributeAt(target, instances.numAttributes()-2);
			
			for (int instanceIndex = 0; instanceIndex < instances.numInstances(); instanceIndex++) {
				String domainValue = instances.instance(instanceIndex).stringValue(instances.attribute("domain"));
				instances.instance(instanceIndex).setValue(!domainValue.equals(domain) ? source : target, 
						instances.instance(instanceIndex).value(general));
				instances.instance(instanceIndex).setValue(domainValue.equals(domain) ? source : target, 0);
			}
		}
		
		return instances;
	}
	
	/**
	 * Keep initial instance as general add source and target with only one having a value
	 * daume07easyadapt.pdf Frustratingly Easy Domain Adaptation
	 * two sources
	 * requires domain attribute
	 * @param instances
	 * @param targetType
	 * @return
	 */
	public static Instances easyDomainAdaptation(Instances instances, ArrayList<String> sources, HashSet<String> exclude) {
		
		int original = instances.numAttributes();
		
		for (int index = 0; index < original; index++) {
			
			Attribute general = instances.attribute(index);
			
			if (exclude.contains(general.name())) continue;
			
			ArrayList<Attribute> attributeSources = new ArrayList<Attribute>();
			
			for (String s : sources) {
				Attribute source = new Attribute(general.name() + "_" + s);
				instances.insertAttributeAt(source, instances.numAttributes()-1);
				attributeSources.add(instances.attribute(general.name() + "_" + s));
			}			
			//Attribute target = new Attribute(general.name() + "_target");
			//instances.insertAttributeAt(target, instances.numAttributes()-2);
			
			for (int instanceIndex = 0; instanceIndex < instances.numInstances(); instanceIndex++) {
				String domainValue = instances.instance(instanceIndex).stringValue(instances.attribute("domain"));
				
				for (Attribute s : attributeSources) {
					instances.instance(instanceIndex).setValue(s,s.name().equals(general.name() + "_" + domainValue) ? 
							instances.instance(instanceIndex).value(general) : 0);				
				}
			}
		}
		
		return instances;
	}

}
