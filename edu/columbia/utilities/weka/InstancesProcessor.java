package edu.columbia.utilities.weka;

import java.util.*;

import weka.core.Attribute;
import weka.core.Instances;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.NumericToBinary;
import weka.filters.unsupervised.attribute.Remove;
import weka.filters.unsupervised.attribute.Reorder;

public class InstancesProcessor {

	public static Attribute nominalAtt(String attName, String[] nominals) {
        ArrayList<String> nominalsVector = new ArrayList<String>();
        for (String nominal : nominals) nominalsVector.add(nominal);
        return new Attribute(attName, nominalsVector);
	}

	public static Attribute numericAtt(String attName) {
		return new Attribute(attName);
	}

	public static Attribute stringAtt(String attName) {
		return new Attribute(attName, (ArrayList<String>)null);
	}
	
	
	
	public static Instances reorder(Instances instances) throws Exception {
        return reorder(instances,1);
	}
	
	public static Instances reorder(Instances instances, int classSpot) throws Exception {
        Reorder reorderFilt = new Reorder();
        if (classSpot == 1) reorderFilt.setAttributeIndices("2-last,1");
        else if (classSpot+1 >= instances.numAttributes()) return instances;
        else reorderFilt.setAttributeIndices("first-" + (classSpot-1) + "," + (classSpot+1) + "-last," + classSpot);

        reorderFilt.setInputFormat(instances);
        return Filter.useFilter(instances, reorderFilt);
	}
	
	
	public static Instances subset(Instances instances, HashSet<String> experiments, ArrayList<String> featureNames) {
		
		try {
			for (String name : featureNames) {
				if (experiments.contains(name)) continue;
				instances = removeByName(instances, name);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return instances;
	}

	public static Instances removeByName(Instances instances, String name) throws Exception {
		
		String attributes = "";
		
		//System.out.println(instances.attribute(start) + " " + instances.attribute(end-1));
		
		for (int index = 0; index < instances.numAttributes(); index++) {
			if (instances.attribute(index).name().startsWith(name)) {
				attributes += "," + (index+1);
			}
		}
		
		Remove remove = new Remove();
        remove.setAttributeIndices(attributes);
        remove.setInputFormat(instances);
        remove.setInvertSelection(false);
        return Filter.useFilter(instances, remove);
	}

	public static Instances removeList(Instances instances, String list) throws Exception {
		return removeList(instances, list, false);
	}
	
	public static Instances removeList(Instances instances, String list, boolean invert) throws Exception {
		
		Remove remove = new Remove();
        remove.setAttributeIndices(list);
        remove.setInvertSelection(invert);
        remove.setInputFormat(instances);
        return Filter.useFilter(instances, remove);
	}
	
	public static Instances toBinary(Instances instances) throws Exception {
		NumericToBinary n2b = new NumericToBinary();
        n2b.setInputFormat(instances);
        return Filter.useFilter(instances, n2b);
	}
	
	public static Instances removeRange(Instances instances, int start, int end) throws Exception {
		
		String attributes = "";
		
		for (int index = start; index <= end; index++) {
			attributes+= (index+1) + ",";
		}
		return removeList(instances,attributes);
	}

}
