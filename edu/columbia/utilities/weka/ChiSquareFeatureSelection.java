/**
 * 
 */
package edu.columbia.utilities.weka;

import java.util.*;

import weka.core.Attribute;
import weka.core.Instances;
import weka.core.SelectedTag;
import weka.filters.Filter;
import weka.attributeSelection.ChiSquaredAttributeEval;
import weka.core.tokenizers.NGramTokenizer;
import weka.filters.unsupervised.attribute.StringToWordVector;
import weka.filters.unsupervised.instance.RemoveWithValues;

/**
 * @author sara
 *
 */
public class ChiSquareFeatureSelection {

	/**
	 * 
	 */
	public ChiSquareFeatureSelection() {
		// TODO Auto-generated constructor stub
	}

	public static StringToWordVector getCounterFilter(Instances instances, int attIndex) throws Exception {
		StringToWordVector counterFilter = new StringToWordVector();
        counterFilter.setInputFormat(instances);
        counterFilter.setAttributeIndicesArray(new int[] {attIndex});
        counterFilter.setOutputWordCounts(true);
        return counterFilter;
	}

	public static StringToWordVector getNgramFilter(Instances instances, int attIndex, int wordsToKeep, 
			int ngramMinSize, int ngramMaxSize, boolean normalize, boolean newDoNotOperateOnPerClassBasis) throws Exception {
        StringToWordVector ngramFilt = new StringToWordVector();
        ngramFilt.setLowerCaseTokens(true);
        
        NGramTokenizer tok = new NGramTokenizer();
        tok.setDelimiters(" \r\n\t");
        tok.setNGramMinSize(ngramMinSize);
        tok.setNGramMaxSize(ngramMaxSize);
        ngramFilt.setTokenizer(tok);
        ngramFilt.setDoNotOperateOnPerClassBasis(newDoNotOperateOnPerClassBasis);
        ngramFilt.setInputFormat(instances);
        ngramFilt.setAttributeIndicesArray(new int[] {attIndex});
        ngramFilt.setWordsToKeep(wordsToKeep);
        if (normalize) ngramFilt.setNormalizeDocLength(new SelectedTag(StringToWordVector.FILTER_NORMALIZE_ALL, StringToWordVector.TAGS_FILTER));
     	return ngramFilt;
	}
		
	public static Instances chiSquareTest(Instances instances, HashSet<String> grams, int start, String classAttribute) throws Exception {

		String attributes = "";
		
		for (int index = start; index < instances.numAttributes(); index++) {
			if (!grams.contains(instances.attribute(index).name())) {
				attributes += (instances.attribute(index).index() + 1) + ",";
			}
		}
		
		instances = InstancesProcessor.removeList(instances, attributes);
        instances = InstancesProcessor.reorder(instances,instances.attribute(classAttribute).index()+1);
		return instances;
	}
	
	public static Instances chiSquare(Instances instances, int start, int count) throws Exception {
		// 1. Copy instances in list
		
        // set class to be included
        Instances subset = InstancesProcessor.removeList(instances, "first," + start + "-" + count, true);
        subset.setClassIndex(0);
		// 2. Perform Chi Square Filter
        ChiSquaredAttributeEval eval = new ChiSquaredAttributeEval();
		eval.buildEvaluator(subset);
		
		// 3. remove attributes from original instances that do not pass threshold
		int deleted = 0;
		int kept = 0;
		
		String delete = "";
		
		for (int index = 1; index < subset.numAttributes(); index++) {
			if (eval.evaluateAttribute(index) == 0.0) {
				delete += "," + (index+1);
				deleted++;
			}
			else {
				kept++;
				System.out.print(subset.attribute(index).name() + ",");
			}
		}

		instances = InstancesProcessor.removeList(instances, start + "-" + count, true);
		subset = InstancesProcessor.removeList(subset, "first," + delete.substring(1));
        
        instances = Instances.mergeInstances(instances, subset);
		
		System.out.println("\n[WekaBuilder.chiSquare] removed " + deleted + " attributes. Kept " + kept + " attributes. "+ instances.numAttributes() + " left.");
		
		return instances;
	}

	
	public static HashSet<String> setNgrams(Instances instances, int start, int end) {
		HashSet<String> grams = new HashSet<String>();
		
		for (int index = start; index < end; index++) {
			grams.add(instances.attribute(index).name());
		}
		return grams;
	}

	public static List<Object> adjustNames(Instances trainInsts, StringToWordVector ngramFilt, StringToWordVector filter, 
			int [] attributes, String type, int min, int max, boolean normalize, String classAttribute) throws Exception{
		NGramTokenizer tok = new NGramTokenizer();
        tok.setNGramMinSize(min);
        tok.setNGramMaxSize(max);
        ngramFilt.setTokenizer(tok);
        ngramFilt.setAttributeNamePrefix(type + "_");
        if (normalize) ngramFilt.setNormalizeDocLength(new SelectedTag(StringToWordVector.FILTER_NORMALIZE_ALL, StringToWordVector.TAGS_FILTER));

    	//int oldNumAttributes = trainInsts.numAttributes();
        trainInsts.setClassIndex(trainInsts.attribute(classAttribute).index());
        ngramFilt.setAttributeIndicesArray(attributes);
        
        ngramFilt.setInputFormat(trainInsts);
        //filter = (StringToWordVector)Filter.makeCopy(ngramFilt);
        trainInsts = Filter.useFilter(trainInsts, ngramFilt);
      
        trainInsts = InstancesProcessor.reorder(trainInsts,trainInsts.attribute(classAttribute).index()+1);
        // adjustAttributeNames(type,trainInsts, oldNumAttributes - 1, trainInsts.numAttributes());
        
        List<Object> modified = new ArrayList<Object>();
        modified.add(trainInsts);
        modified.add(ngramFilt);
        return modified;
	}
	
	public static Instances applyNonWekaFilter(Instances trainInsts, 
			int [] attributes, String type, int min, int max, int size, boolean normalize, String classAttribute, ArrayList<String> domains) {
		
		HashSet<String> ngrams = new HashSet<String>();
		
		if (domains != null) {
			for (String domain : domains) {
				
				/*Attribute duplicate = new Attribute(type + "_" + domain, (FastVector) null);
				trainInsts.insertAttributeAt(duplicate,trainInsts.numAttributes()-1);
				
				for (int i = 0; i < trainInsts.numInstances(); i++) {
					trainInsts.instance(i).setValue(trainInsts.attribute(type + "_" + domain),
							trainInsts.instance(i).stringValue(trainInsts.attribute(type)));
				}*/
				/*Copy copy = new Copy();
				copy.setAttributeIndices(String.valueOf(trainInsts.attribute(type).index()+1));
				copy.setInputFormat(trainInsts);
				trainInsts = Filter.useFilter(trainInsts, copy);
				trainInsts.renameAttribute(trainInsts.attribute(trainInsts.numAttributes()-1), type + "_" + domain);*/
				
				/*RemoveWithValues domainFilter = new RemoveWithValues();
				domainFilter.setAttributeIndex(String.valueOf(trainInsts.attribute("domain").index()+1));
				domainFilter.setNominalIndices(String.valueOf(trainInsts.attribute("domain").indexOfValue(domain)));
				domainFilter.setInputFormat(trainInsts);
				Instances domainSubset = Filter.useFilter(trainInsts, domainFilter);*/
				ngrams.addAll(computeNGrams(trainInsts, type, domain, size, min, max, normalize));
			}
		}
		else {
			ngrams.addAll(computeNGrams(trainInsts, type, null, size, min, max, normalize));
		}
		trainInsts = computeFeatures(trainInsts,type,ngrams, min, max);
		return trainInsts;
	}
	
	private static Instances computeFeatures(Instances instances, String type, HashSet<String> ngrams, int min, int max) {
		// 1. add new attributes
		for (String ngram : ngrams) {
			Attribute att = new Attribute(type + "_" + ngram);
			instances.insertAttributeAt(att, instances.numAttributes()-1);
			
			// initialize to 0
			for (int index = 0; index < instances.numInstances(); index++) {
				instances.instance(index).setValue(instances.attribute(type + "_" + ngram), 0.0);
			}
		}
		
		// 2. compute value for each attribute per instance
		for (int index = 0; index < instances.numInstances(); index++) {
			String text = instances.instance(index).stringValue(instances.attribute(type));
			HashMap<String,Integer> gs = computeNGrams(text,min,max);
			
			for (String g : gs.keySet()) {
				Attribute att = instances.attribute(type + "_" + g);
				if (att != null) instances.instance(index).setValue(att, gs.get(g));
			}
		}
		
		// don't need original anymore
		instances.deleteAttributeAt(instances.attribute(type).index());
		return instances;
	}
	
	public static List<Object> applyFilter(Instances trainInsts, String type, String classAttribute) throws Exception {
		return applyFilter(trainInsts, type, classAttribute, null, false);
	}
	
	public static List<Object> applyFilter(Instances trainInsts, String type, String classAttribute, ArrayList<String> domains) throws Exception {
		return applyFilter(trainInsts, type, classAttribute, domains, false);
	}
	
	public static List<Object> applyFilter(Instances trainInsts, String type, String classAttribute, ArrayList<String> domains, boolean printStats) throws Exception{
		trainInsts = chiSquareByName(trainInsts,type + "_", classAttribute, domains, printStats);
        HashSet<String> ngrams = setNgramsByName(trainInsts,type + "_");
        
        List<Object> modified = new ArrayList<Object>();
        modified.add(trainInsts);
        modified.add(ngrams);
        return modified;
    }
    
    public static Instances applyTestFilter(Instances testInsts, StringToWordVector filter, HashSet<String> ngrams, String type, String classAttribute) throws Exception {
    	//int size = testInsts.numAttributes();
   	 	
   	 	testInsts = Filter.useFilter(testInsts, filter);
   	 	testInsts = InstancesProcessor.reorder(testInsts,testInsts.attribute(classAttribute).index()+1);
   	 	//testInsts = chiSquareTest(testInsts,ngrams,size-1);
   	 	//adjustAttributeNames(type,testInsts, size - 1, testInsts.numAttributes());
   	 	testInsts = chiSquareTestByName(testInsts,ngrams,type + "_");
   	 	
   	 	return testInsts;
    }
    
    /**
     * At this point we already have the filtered list of n-grams post chi-square. Domain does not matter as well.
     * @param testInsts
     * @param ngrams
     * @param type
     * @param classAttribute
     * @param domains
     * @param min
     * @param max
     * @return
     * @throws Exception
     */
    public static Instances applyNonWekaTestFilter(Instances testInsts, HashSet<String> ngrams, 
    		String type, String classAttribute, int min, int max) throws Exception {
    	
    	testInsts = computeFeatures(testInsts,type,ngrams, min, max);
    	return testInsts;
    }
    
	public static Instances chiSquareByName(Instances instances, String name, String classAttribute) throws Exception {
		return chiSquareByName(instances, name, classAttribute, null, false);
	}
	
	public static Instances chiSquareByName(Instances instances, String name, String classAttribute, ArrayList<String> domains) throws Exception {
		return chiSquareByName(instances, name, classAttribute, domains, false);
	}
    
	public static Instances chiSquareByName(Instances instances, String name, String classAttribute, ArrayList<String> domains, boolean printStats) throws Exception {

		// 1. Copy instances in list
		String indices = String.valueOf(instances.attribute(classAttribute).index()+1);
		if (domains != null) indices += "," + String.valueOf(instances.attribute("domain").index()+1);
		
		for (int index = 0; index < instances.numAttributes(); index++) {
			if (instances.attribute(index).name().startsWith(name)) {
				indices += "," + (index+1);
			}
		}
		Instances subset = InstancesProcessor.removeList(instances, indices, true);
		subset.setClass(subset.attribute(classAttribute));
		
		ArrayList<ChiSquaredAttributeEval> evaluators = new ArrayList<ChiSquaredAttributeEval>();
		   
		// 2. Compute per domain if applicable
		if (domains != null) {
			
			for (String domain : domains) {
				try {
					RemoveWithValues domainFilter = new RemoveWithValues();
					domainFilter.setAttributeIndex(String.valueOf(subset.attribute("domain").index()+1));
					domainFilter.setNominalIndices(String.valueOf(subset.attribute("domain").indexOfValue(domain)+1));
					domainFilter.setInputFormat(subset);
					Instances domainSubset = Filter.useFilter(subset, domainFilter);
					
					// 2. Perform Chi Square Filter
			        ChiSquaredAttributeEval eval = new ChiSquaredAttributeEval();
			        eval.buildEvaluator(domainSubset);
			        evaluators.add(eval);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		else {
			// 2. Perform Chi Square Filter
	        ChiSquaredAttributeEval eval = new ChiSquaredAttributeEval();
	        eval.buildEvaluator(subset);
	        evaluators.add(eval);
		}
		
		instances.setClass(instances.attribute(classAttribute));
		// 3. remove attributes from original instances that do not pass threshold
		int deleted = 0;
		int kept = 0;
		String remove = "";
		for (int index = 1; index < subset.numAttributes(); index++) {
			if (maxEval(evaluators,index) == 0.0) {
				remove += "," + (instances.attribute(subset.attribute(index).name()).index()+1);
				deleted++;
			}
			else {
				if (printStats) System.err.println(subset.attribute(index) + ": " + evaluators.get(0).evaluateAttribute(index) + ", " + 
						Arrays.toString(count(instances,instances.attribute(subset.attribute(index).name()).index())));
				kept++;
			}
		}
		
		if (remove.length() > 1)
			instances = InstancesProcessor.removeList(instances, remove.substring(1));
		
		if (printStats)
			System.out.println("[ChiSquareFeatureSelection.chiSquare] " + name + ": removed " + deleted + " attributes. Kept " + kept + " attributes. "+ instances.numAttributes() + " left.");
		return instances;
	}
	
	private static double maxEval(ArrayList<ChiSquaredAttributeEval> evals, int index) {
		double max = 0.0;
		
		for (ChiSquaredAttributeEval eval : evals) {
			try {
				if (eval.evaluateAttribute(index) > max) max = eval.evaluateAttribute(index);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return max;
	}
	
	private static double [] count(Instances instances, int attributeIndex) { //double [] array) {
		double [] count = new double[instances.classAttribute().numValues()];
		
		Attribute attr = instances.attribute(attributeIndex);
		
		for (int index = 0; index < instances.numInstances(); index++) {
			count[(int)instances.instance(index).classValue()] += instances.instance(index).value(attr);
		}
		
		/*for (double value : array) {
			count += value;
		}*/
		return count;
	}
	
	public static Instances chiSquareTestByName(Instances instances, HashSet<String> grams, String name) throws Exception {
		
		String remove = "";
		
		for (int index = 0; index < instances.numAttributes(); index++) {
			if (instances.attribute(index).name().startsWith(name) && !grams.contains(instances.attribute(index).name())) {
				remove += "," + (index + 1);
			}
		}
		
		if (remove.length() > 0)
			instances = InstancesProcessor.removeList(instances, remove.substring(1));
		
		for (String gram : grams) {
			if (instances.attribute(gram) == null) {
				Attribute att = new Attribute(gram);
				instances.insertAttributeAt(att, instances.numAttributes()-2);
				
				for (int i = 0; i < instances.numInstances(); i++) {
					instances.instance(i).setValue(att, 0);
				}
			}
		}
		return instances;
	}
	
	public static HashSet<String> setNgramsByName(Instances instances, String name) {
		return setNgramsByName(instances, name, false);
	}
	
	public static HashSet<String> setNgramsByName(Instances instances, String name, boolean printStats) {
		HashSet<String> grams = new HashSet<String>();
		
		for (int index = 0; index < instances.numAttributes(); index++) {
			if (instances.attribute(index).name().startsWith(name)) {
				grams.add(instances.attribute(index).name());
				if (printStats) System.err.println(instances.attribute(index) + ": " + Arrays.toString(count(instances,index)));
			}
		}
		return grams;
	}
	
	public static Instances adjustAttributeNames(String name, Instances instances, int start, int count) {
		for (int index = start; index < count; index++) {
			instances.renameAttribute(index, name + "_" + instances.attribute(index).name());
		}
		return instances;
	}
	
	public static HashMap<String,Integer> computeNGrams(String text, int min, int max) {
		HashMap<String,Integer> ngrams = new HashMap<String,Integer>();
		String [] words = text.toLowerCase().replaceAll("\\s+", " ").split("\\s+");
					
		for (int i = 0; i < words.length; i++) {
			String ngram = "";
			
			int count = 0;
			while (count < min && (count+i) < words.length) {
				ngram += words[i+count] + " ";
				count++;
			}
			if ((count+i) >= words.length) continue;
			do {
				ngrams.put(ngram, ngrams.containsKey(ngram) ? ngrams.get(ngram)+1 : 1);
				ngram += words[i+count] + " ";
				count++;
			} while (count <= max && (count+i) < words.length);
		}
		return ngrams;
	}
	
	public static ArrayList<String> computeNGrams(Instances instances, String attributeName, String domain, int size, int min, int max, boolean normalize) {
		final HashMap<String,Integer> ngrams = new HashMap<String,Integer>();
		
		Attribute att = instances.attribute(attributeName);
		
		// generate counts for all ngrams
		for (int index = 0; index < instances.numInstances(); index++) {
			if (domain != null && !domain.equals(instances.instance(index).stringValue(instances.attribute("domain")))) continue;
			String text = instances.instance(index).stringValue(att);
			HashMap<String,Integer> grams = computeNGrams(text,min,max);
			
			for (String g : grams.keySet()) {
				if (normalize) ngrams.put(g, ngrams.containsKey(g) ? ngrams.get(g) + 1 : 1);
				else ngrams.put(g, ngrams.containsKey(g) ? ngrams.get(g) + grams.get(g) : grams.get(g));
			}
		}
		
		// now we need to sort to only keep top "size"
		Comparator<String> topTermComparator 
			= new Comparator<String>() {

				public int compare(String w1,String w2) {
			
					Integer value1 = ngrams.get(w1);
					Integer value2 = ngrams.get(w2);
					
					//descending order
					return value2.compareTo(value1);
				}
		};
		
		ArrayList<String> words = new ArrayList<String>(ngrams.keySet());
			
		Collections.sort(words, topTermComparator);
		
		if (size == -1 || size >= words.size()) return words;
		return new ArrayList<String>(words.subList(0, size));
	}
}
