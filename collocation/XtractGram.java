/**
 * 
 */
package collocation;

import java.util.*;

/**
 * @author sara
 *
 */
public class XtractGram {

	int [] associations = {0,0,0,0,0,0,0,0,0,0};
	int frequency = 0;
	int documentFrequency = 0;
	int count; // number of times this word occurs in a bigram
	HashSet<String> bigrams;
	String word;
	
	public XtractGram(String w) {
		word = w;
		bigrams = new HashSet<String>();
	}
	
	public String getWord() {
		return word;
	}
	
	public int getFrequency() {
		return frequency;
	}
	
	public int getDocumentFrequency() {
		return documentFrequency;
	}
	
	public double getTfIdf(int numWords, int numDocuments) {
		if (documentFrequency == 0) return 0;
		return ((double)frequency/numWords) * ((double)numDocuments/documentFrequency);
	}
	
	public double getAverageFrequency() {
		return (double)count/bigrams.size();
	}
	
	/**
	 * If index true return position in array based on index, otherwise return based on distance from word
	 * 
	 * @param position
	 * @param index
	 * @return
	 */
	public int getAssociation(int position, boolean index) {
		if (index) {
			return associations[position];
		}
		
		if (position >= 0) 
			return associations[(associations.length/2 - 1) + position];
		else 
			return associations[associations.length/2 + position];
	}
	
	public void increment() {
		frequency++;
	}
	
	public void incrementDF() {
		documentFrequency++;
	}
	
	public void increment(int position) {
		//frequency++;
		
		if (position >= 0) 
			associations[(associations.length/2 - 1) + position]++;
		else 
			associations[associations.length/2 + position]++;
	}
	
	public void addBigram(String w_1) {
		bigrams.add(w_1);
		count++;
	}
	
	public int compareTo(XtractGram g, String type) {
		
		if (type.equals("df")) {
			return Integer.valueOf(this.documentFrequency).compareTo(g.getDocumentFrequency());
		}
		return Integer.valueOf(this.frequency).compareTo(g.getFrequency());
	}
	
	public int compareTo(XtractGram g) {
		return ((Integer)this.frequency).compareTo(g.getFrequency());
	}
	
	public int tfidfcompareTo(XtractGram g, int numWords, int numDocuments) {
		return ((Double)this.getTfIdf(numWords,numDocuments)).compareTo(g.getTfIdf(numWords,numDocuments));
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
