/**
 * 
 */
package collocation;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Hashtable;
import java.util.Iterator;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

/**
 * @author sara
 * This is for a sentence in a corpus. Used in Xtract. 
 */
public class Sentence {

	String [] words;
	String [] pos;
	Hashtable<String,Dependency> dependencies;
	String sentenceText;
	String sentencePOS;
	String id;
	
	public class Dependency {
		
		String relation;
		String [] w = new String [2];
		int [] position = new int [2];
		
    	// format: nsubj(w_0-1,w_1-2)		
		public Dependency(String dependency) {
			try {
				int open = dependency.indexOf("(");
		    	
				relation = open > -1 ? dependency.substring(0,open).trim() : "";
		    	dependency = dependency.substring(open+1,dependency.length()-1);
				Pattern p = Pattern.compile("\\p{Graph}*-[0-9][0-9]?");
		    	Matcher m = p.matcher(dependency);
		    	
		    	int i = 0;
		    	while(m.find()) {
		    		String word = m.group();
		    		
		    		int dash = word.lastIndexOf("-");
		    		w[i] = word.substring(0,dash);
		    		position[i] = Integer.valueOf(word.substring(dash+1));
		    		i++;
		    	}
				
			} catch (Exception e) {
				System.err.println("[Sentence.Dependency] error parsing dependency string " + dependency + ": " + e);
				e.printStackTrace();
			}
		}
		
		public Dependency(String reln, String w_0, String w_1, int w_0pos, int w_1pos) {
			relation = reln;
			w[0] = w_0;
			w[1] = w_1;
			position[0] = w_0pos;
			position[1] = w_1pos;
		}
		
		public String getRelationship() {
			return relation;
		}
		
		public String getWord(int index) {
			if (!w[index].matches("[\\p{Punct}0-9]+")) return w[index].replaceAll("\\p{Punct}+","");
			return w[index];
		}
		
		public int getWordPosition(int index) {
			return position[index];
		}
		
		public String getBigram() {
			return w[0] + " " + w[1];
		}
		
		public String toString() {
			return relation + "(" + w[0] + "-" + position[0] + ", " + w[1] + "-" + position[1] + ")";
		}
	}
	
	public Sentence(String text, String posIn, ArrayList<String> dependenciesIn, String idIn) {
		id = idIn;
		sentenceText = text;
		words = text.split(" ");
		sentencePOS = posIn;
		pos = posIn.split(" ");
		dependencies = new Hashtable<String,Dependency>();
		
		for (int index = 0; dependenciesIn != null && index < dependenciesIn.size(); index++) {
			Dependency d = new Dependency(dependenciesIn.get(index));
			dependencies.put(d.getBigram(),d);
		}
	}

	public Sentence(String sentence, ArrayList<String> dependenciesIn, String idIn) {
		if (sentence == null || sentence.equals("")) return;
		
		sentence = sentence.trim().replaceAll("\\s+", " ");
		
		id = idIn;
		String [] data = sentence.split(" ");
		sentenceText = "";
		sentencePOS = "";
		words = new String[data.length];
		pos = new String[data.length];
		dependencies = new Hashtable<String, Dependency>();
		
		for (int index = 0; dependenciesIn != null && index < dependenciesIn.size(); index++) {
			Dependency d = new Dependency(dependenciesIn.get(index));
			dependencies.put(d.getBigram(),d);
		}
		
		// check if pos is available "/" in old stanford parser, "_" in new stanford parser
		if (sentence.split("/").length >= data.length/2 || sentence.split("_").length >= data.length/2) {
			
			for (int index = 0; index < data.length; index++) {
				if (data[index].equals("")) continue;
				
				int split = -1;

				if (data[index].lastIndexOf("_") > data[index].lastIndexOf("/"))
					split = data[index].lastIndexOf("_");
				else 
					split = data[index].lastIndexOf("/");
				
				if (split == -1) {
					words[index] = data[index];
					sentenceText += data[index];  
				}
				else {
					words[index] = data[index].substring(0,split);
					pos[index] = data[index].substring(split+1);
					sentenceText += words[index] + " ";
					sentencePOS += pos[index] + " ";
				}
			}
			sentenceText.trim();
			sentencePOS.trim();
		}
		else {
			words = data;
			sentenceText = sentence;
		}
	}
	
	public static Sentence processSentence(Element sentenceIn, String idIn) {

		
		try {
	    	Node stringTag = sentenceIn.getElementsByTagName("string").item(0).getFirstChild();
	    	String string = stringTag != null ? stringTag.getNodeValue() : "";
	    	
	    	Node posTag = sentenceIn.getElementsByTagName("pos").item(0).getFirstChild();
	    	String pos = stringTag != null ? posTag.getNodeValue() : "";
	    	
	    	Node dependenciesTag = sentenceIn.getElementsByTagName("dependencies").item(0).getFirstChild();
	    		    	
	    	Pattern p = Pattern.compile("[a-z]*\\(\\p{Graph}*-[0-9]*, \\p{Graph}*-[0-9]*\\)");
	    	Matcher m = p.matcher(dependenciesTag != null ? dependenciesTag.getNodeValue() : "");
	    	
	    	ArrayList<String> dependencies = new ArrayList<String>();
	    	
	    	while (m.find()) {
	    		dependencies.add(m.group());
	    	}
			
	    	return new Sentence(string,pos,dependencies, idIn);
	    	
		} catch (Exception e) {
			System.err.println("Error occurred in Entry.processEntry: " + e);
			e.printStackTrace();
		}

		return null;
	}
	
	public static Sentence retrieveSentence(String sentenceFromFile) {
		String [] data = sentenceFromFile.split("\\|\\|XTRACT\\|\\|");
		String id = data[0];
		String sentenceText = data[1];
		String sentencePOS = data[2];
		ArrayList<String> dependencies = new ArrayList<String>();
    	Pattern p = Pattern.compile("[a-z]*\\(\\p{Graph}*-[0-9]*, \\p{Graph}*-[0-9]*\\)");
    	Matcher m = p.matcher(data[3]);
    	
    	while (m.find()) {
    		dependencies.add(m.group());
    	}
    	return new Sentence(sentenceText,sentencePOS,dependencies,id);
	}
	
	public String getId() {
		return id;
	}
	
	public String getText() {
		return sentenceText;
	}
	
	public String getSentencePOS() {
		return sentencePOS;
	}
	
	public String getWord(int index) {
		if (words == null) return null;
		try {
			if (!words[index].matches("[\\p{Punct}0-9]+")) return words[index].replaceAll("\\p{Punct}+","");
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println(sentenceText + " " + index);
		}
		return words[index];
	}
	
	public String getPOS(int index) {
		if (pos == null) return null;
		if (index >= pos.length) {
			//System.err.println("[Sentence.getPOS] missing pos for " + words[index] + " at position " + index + " in " + sentenceText);
			return null; 
		}
		
		return pos[index];
	}
	
	public String getTruncatedPOS(int index) {
		if (pos == null || pos[index] == null) return null;
		if (index >= pos.length) {
			//System.err.println("[Sentence.getPOS] missing pos for " + words[index] + " at position " + index + " in " + sentenceText);
			return null; 
		}
		
		if (pos[index].startsWith("VB")) return "VB"; //("VB|VBZ|VBN|VBD|VBG|VBP")) return "VB";
		else if (pos[index].startsWith("NN")) return "NN"; //.matches("NNS|NN|NNP|NNPS")) return "NN";
		else if (pos[index].indexOf("DT") >= 0) return "DT"; //.matches("DT|PDT|WDT")) return "DT";
		else if (pos[index].indexOf("RB") >= 0) return "RB"; //matches("WRB|RB|RBR|RBS")) return "RB";
		else if (pos[index].indexOf("WP") >= 0 || pos[index].indexOf("PRP") >= 0) return "PRP"; //.matches("WP|WP\\$|PRP|PRP\\$")) return "PRP";
		else if (pos[index].startsWith("JJ")) return "JJ"; //.matches("JJ|JJR|JJS")) return "JJ";
		else return pos[index];
	}
	
	public Hashtable<String,Dependency> getDependencies() {
		return dependencies;
	}
	
	public Dependency getDependency(String w_0, String w_1) {
		if (dependencies.containsKey(w_0 + " " + w_1)) 
			return dependencies.get(w_0 + " " + w_1);
		if (dependencies.containsKey(w_1 + " " + w_0)) 
			return dependencies.get(w_1 + " " + w_0);
		return null;
	}
	
	public int length() {
		if (words == null) return 0;
		return words.length;
	}

	public String toString() {
		return id + "||XTRACT||" + sentenceText + "||XTRACT||" + sentencePOS + "||XTRACT||" + dependencies.toString();
	}

	
	public String toXML() {
		String xml = "<string>" + (sentenceText == null ? "" : web.HTML.convertToValidXML(sentenceText)) + "</string><pos>" + sentencePOS + "</pos><dependencies>";
		
		Iterator<String> it = dependencies.keySet().iterator();
		
		while (it.hasNext()) {
			xml += dependencies.get(it.next()).toString() + ", ";
		}
		
		xml += "</dependencies>\n";
		return xml;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Sentence s = new Sentence("","",null,"");
		System.out.println(s.new Dependency("nsubj(4-1-1,4-1-2)").toString());
	}
}
