package collocation;

//import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;

//import document.*;

public class General {

	/*public static ArrayList<Word> getNGrams(Sentence sentence, int wordIndex, int window) {
		
		ArrayList<Word> nGrams = new ArrayList<Word>();
		
		// add words before pivot (wordIndex)
		for (int index = wordIndex-window; index < wordIndex; index++) {
			nGrams.add(sentence.getWord((index)));
		}
		
		// add pivot and words after pivot
		for (int index = wordIndex; index < window; index++) {
			nGrams.add(sentence.getWord((index)));
		}
		
		return nGrams;
	}*/
	
	/**
	 * Compute the normalized expectation for the given string
	 * see "Multiword Unit Hybrid Extraction". Gael Dias, 2003
	 * @param string
	 * @return
	 */
	public static double NE(String string, Hashtable<String,Integer> terms) {
		
		double NE = 0;
		String [] words = string.split(" ");
		int n = words.length;
		
		int pos = 0;
		int denominator = 0;
		
		for (int index = 0; index < words.length; index++) {
			String newString = (string.substring(0, pos) + string.substring(pos + words[index].length())).trim();
			//System.out.println(newString + "," + General.frequency(newString, terms));
			pos += words[index].length() + 1;
			denominator += General.frequency(newString, terms);
		}
		
		//System.out.println(General.frequency(string, terms) + "/ 1/" + n + "*" + denominator);
		//System.out.println(((float)1/n) * denominator);
		
		NE = General.frequency(string, terms) /
				(((double)1/n) * denominator );
		
		return NE;
	}

	/**
	 * Compute the mutual expectation for the given string
	 * see "Multiword Unit Hybrid Extraction". Gael Dias, 2003
	 * @param string
	 * @return
	 */
	public static double ME(String string, Hashtable<String,Integer> terms) {
		double ME = General.frequency(string, terms) * General.NE(string, terms);
		return ME;
	}
	 
	/**
	 * Compute the combined association measure
	 * 
	 * @param textString
	 * @param textTerms
	 * @param posString
	 * @param posTerms
	 * @param alpha parameter for tuning focus to POS or Words
	 * @return
	 */
	public static double CAM(String textString, Hashtable<String, Integer> textTerms, 
			String posString, Hashtable<String,Integer> posTerms, double alpha) {
		
		double textScore = Math.pow(General.ME(textString, textTerms),alpha);
		double posScore = Math.pow(General.ME(posString, posTerms), 1-alpha); 
		return textScore * posScore; 
	}
	
	public static int frequency(String string, Hashtable<String,Integer> terms) {
		if (terms.containsKey(string)) {
			return terms.get(string);
		}
		else if (terms.containsKey(string.toLowerCase())) {
			return terms.get(string.toLowerCase());
		}
		else if (terms.containsKey(string.toUpperCase())) {
			return terms.get(string.toUpperCase());
		}
		return 1;
	}
	
	public static Hashtable<String,Double> averageFrequency(Hashtable<String,XtractBigram> bigrams) {
		Hashtable<String,Integer[]> frequencies = new Hashtable<String, Integer[]>();
		
		Iterator<String> it = bigrams.keySet().iterator();
		
		while (it.hasNext()) {
			String phrase = (String)it.next();
			
			String [] words =  phrase.split(" ");
			
			// keep track of all frequencies
			for (int index = 0; index < words.length; index++) {
				
				Integer [] array;
				
				if (frequencies.containsKey(words[index])) {
					array = frequencies.get(words[index]);
					array[0] += bigrams.get(phrase).getFrequency();
					array[1]++;
				}
				else {
					array = new Integer[2];
					array[0] = bigrams.get(phrase).getFrequency();
					array[1] = 1;
				}
				frequencies.put(words[index], array);
			}
		}
		
		// compute averages and return this hashtable
		Hashtable<String,Double> averageFrequencies = new Hashtable<String,Double>();
		
		it = frequencies.keySet().iterator();
		
		while (it.hasNext()) {
			String word = (String)it.next();
			Integer [] array = frequencies.get(word);
			averageFrequencies.put(word, (double)array[0]/array[1]);
		}
		return averageFrequencies;
	}
	
	/*public static Hashtable<String,Double> standardDeviation(Hashtable<String,Integer[]> associations, Hashtable<String,Double> averageFrequencies) {
	    
		Hashtable<String,Double> standardDeviations = new Hashtable<String,Double>();
		
		Iterator it = associations.keySet().iterator();
		 
		while (it.hasNext()) {
			String bigram = (String)it.next();
			
			try {
			 
				Integer [] array = associations.get(bigram);
	
				double sum = 0;
				for (int index = 1; index < array.length; index++) {
					 String w_i = bigram.split(" ")[0];
					 final double v = (array[index] == null ? 0 : array[index]) - averageFrequencies.get(w_i);
					 sum += v * v;
		        }
				standardDeviations.put(bigram, Math.sqrt( sum / ( array.length - 1 ) ));
			} catch (Exception e) {
				System.err.println("Error occurred in General.standardDeviation on " + bigram + ": " + e);
			}
	     }

		return standardDeviations;
	}
	
	/**
	 * represents average p_i
	 * @return
	 */
	/*public static Hashtable<String,Double> averageAssociations(Hashtable<String,Integer []> associations) {
		Hashtable<String,Double> averageAssociations = new Hashtable<String,Double>();
		
		Iterator it = associations.keySet().iterator();
		
		while (it.hasNext()) {
			String bigram = (String)it.next();
			
			Integer [] array = associations.get(bigram);
			
			if (array == null) {
				System.err.println("[General.averageAssociations] no data distributions available for bigram " + bigram);
				continue;
			}
			
			double sum = 0;
			
			// keep track of all frequencies
			for (int index = 1; index < array.length; index++) {
				try {
					sum += array[index];
				} catch (Exception e) {
					System.err.println("Error occurred in General.averageAssociations on adding sum at array[" 
							+ index + "] to sum (" + sum + ") for bigram " + bigram + ": " + e);
				}
			}
			
			averageAssociations.put(bigram, sum == 0 ? 0 : (double)sum/array.length);
		}
		
		return averageAssociations;
	}
	
	/**
	 * Compute the variance U_i around associations average p_i.
	 * U_i characterizes the shape of the association histogram.
	 * If U_i is small then the histogram will tend to be flat, 
	 * which means w_i can be used equivalently in almost any position around w.
	 * If U_i is large the there will be peaks.
	 * @param associations
	 * @param averageAssociations
	 * @return
	 */
	public static Hashtable<String,Double> varianceAssociations(
			Hashtable<String,Integer []> associations, Hashtable<String,Double> averageAssociations) {
		Hashtable<String,Double> variances = new Hashtable<String,Double>();
		
		Iterator<String> it = associations.keySet().iterator();
		
		while (it.hasNext()) {
			String bigram = (String)it.next();
			Integer [] array = associations.get(bigram);
			
			double sum = 0;
			
			for (int index = 1; index < array.length; index++) {
				double x = array[index] - averageAssociations.get(bigram);
				sum = x*x;
			}
			variances.put(bigram, (sum*sum)/array.length-1);
		}
		
		return variances;
	}
	
	/**
	 * Compute the z score for the word-pair. 
	 * z score is known as the strength of the word-pair
	 * @param associations
	 * @param averageFrequencies
	 * @param standardDeviations
	 * @return
	 */
	/*public static Hashtable<String,Double> zScore(Hashtable<String,Integer[]> associations, 
			Hashtable<String,Double> averageFrequencies, Hashtable<String,Double> standardDeviations) {

		Hashtable<String,Double> zScores = new Hashtable<String,Double>();
		
		Iterator it = associations.keySet().iterator();
		 
		while (it.hasNext()) {
			String bigram = (String)it.next();
			Integer [] array = associations.get(bigram);
			String w = bigram.split(" ")[0];
			
			if (array == null) {
				System.err.println("[General.zScore] no data distributions available for bigram " + bigram);
				continue;
			}
			if (averageFrequencies.get(w) == null) {
				System.err.println("[General.zScore] no average frequency available for word " + w);
				continue;
			}
			if (standardDeviations.get(bigram) == null) {
				System.err.println("[General.zScore] no standard deviation available for bigram " + bigram);
				continue;
			}
			
			
			double k_i = (array[0] - averageFrequencies.get(w)) == 0 || standardDeviations.get(bigram) == 0 ?
					0 : (array[0] - averageFrequencies.get(w)) / standardDeviations.get(bigram);
			zScores.put(bigram, k_i); 
		}
		
		return zScores;
	}
	
	/*public static int frequency(FilterIndexReader reader, Analyzer analyzer, String string) {
		
		QueryParser parser = new QueryParser(null,analyzer);
		Query query = parser.parse("dob:[" + minimumAge + " TO " + maximumAge + "] AND country:(" + countries + ") ");
		TopDocs d = searcher.search(query,1000000);
		totalHits = d.totalHits;
		ScoreDoc [] docs = d.scoreDocs;
		return 0;
	}*/
	
	public static void main(String[] args) {
	
		Hashtable<String, Integer> terms = new Hashtable<String, Integer>();
				
		terms.put("brown fox jumped", 3);
		terms.put("the boy jumped", 2);
		terms.put("boy jumped over", 1);
		terms.put("fox jumped", 3);
		terms.put("brown fox", 3);
		terms.put("boy jumped", 3);
		terms.put("the boy", 2);
		terms.put("jumped over", 1);
		
		Hashtable<String, Integer> posTerms = new Hashtable<String, Integer>();
		
		posTerms.put("JJ NN VB", 3);
		posTerms.put("DET NN VB", 2);
		posTerms.put("NN VB JJ", 1);
		posTerms.put("NN VB", 6);
		posTerms.put("JJ NN", 3);
		posTerms.put("DET NN", 2);
		posTerms.put("VB JJ", 1);
		
		System.out.println(General.CAM("the boy jumped", terms, "DET NN VB", posTerms, 1));
		System.out.println(General.CAM("boy jumped over", terms, "NN VB JJ", posTerms, 1));
		System.out.println(General.CAM("brown fox jumped", terms, "JJ NN VB", posTerms, 1));
	}
}
