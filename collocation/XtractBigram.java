package collocation;

public class XtractBigram {
	
	String bigram;
	String w_0;
	String w_1;
	boolean w_0isPOS;
	boolean w_1isPOS;
	int [] associations = {0,0,0,0,0,0,0,0,0,0};
	//int distance = -1;
	int frequency = 0;
	int documentFrequency = 0;
	double standardDeviation = -1;
	double averageAssociation = -1;
	double strength = -1;
	double spread = -1;
	
	
	// for stage 3
	String label;
	
	XtractBigram(String w_0In, String w_1In, boolean w_0POS, boolean w_1POS) {
		bigram = w_0In + " " + w_1In;
		w_0 = w_0In;
		w_0isPOS = w_0POS;
		w_1 = w_1In;
		w_1isPOS = w_1POS;
		frequency = 0;
	}
	
	XtractBigram(String w_0In, String w_1In, boolean w_0POS, boolean w_1POS, String labelIn) {
		this(w_0In, w_1In, w_0POS, w_1POS);
		label = labelIn;
	}
	
	public String getBigram() {
		return bigram;
	}
	
	public String getBigramAsRegularExpression() {
		String bgRegEX  = "";
		
		if (associations[0] > 0) {
			bgRegEX += w_1 + " \\p{Graph}+ \\p{Graph}+ \\p{Graph}+ \\p{Graph}+ " + w_0 + "|";
		}
		if (associations[1] > 0) {
			bgRegEX += w_1 + " \\p{Graph}+ \\p{Graph}+ \\p{Graph}+ " + w_0 + "|";
		}
		if (associations[2] > 0) {
			bgRegEX += w_1 + " \\p{Graph}+ \\p{Graph}+ " + w_0 + "|";
		}
		if (associations[3] > 0) {
			bgRegEX += w_1 + " \\p{Graph}+ " + w_0 + "|";
		}
		if (associations[4] > 0) {
			bgRegEX += w_1 + " " + w_0 + "|";
		}
		if (associations[5] > 0) {
			bgRegEX += w_0 + " " + w_1 + "|";
		}
		if (associations[6] > 0) {
			bgRegEX += w_0 + " \\p{Graph}+ " + w_1 + "|";
		}
		if (associations[7] > 0) {
			bgRegEX += w_0 + " \\p{Graph}+ \\p{Graph}+ " + w_1 + "|";
		}
		if (associations[8] > 0) {
			bgRegEX += w_0 + " \\p{Graph}+ \\p{Graph}+ \\p{Graph}+ " + w_1 + "|";
		}
		if (associations[9] > 0) {
			bgRegEX += w_0 + " \\p{Graph}+ \\p{Graph}+ \\p{Graph}+ \\p{Graph}+ " + w_1 + "|";
		}
		
		if (bgRegEX.length() == 0) return bgRegEX;
		return bgRegEX.substring(0,bgRegEX.length()-1);
	}
	
	public String getBigramAsPOSRegularExpression() {
		String bgRegEX  = "";
		
		String w0 = w_0isPOS ? "\\p{Graph}+/" + w_0 : w_0 + "/\\p{Graph}+";
		String w1 = w_1isPOS ? "\\p{Graph}+/" + w_1 : w_1 + "/\\p{Graph}+"; 
		
		if (associations[0] > 0) {
			bgRegEX += w1 + " \\p{Graph}+ \\p{Graph}+ \\p{Graph}+ \\p{Graph}+ " + w0 + "|";
		}
		if (associations[1] > 0) {
			bgRegEX += w1 + " \\p{Graph}+ \\p{Graph}+ \\p{Graph}+ " + w0 + "|";
		}
		if (associations[2] > 0) {
			bgRegEX += w1 + " \\p{Graph}+ \\p{Graph}+ " + w0 + "|";
		}
		if (associations[3] > 0) {
			bgRegEX += w1 + " \\p{Graph}+ " + w0 + "|";
		}
		if (associations[4] > 0) {
			bgRegEX += w1 + " " + w0 + "|";
		}
		if (associations[5] > 0) {
			bgRegEX += w0 + " " + w1 + "|";
		}
		if (associations[6] > 0) {
			bgRegEX += w0 + " \\p{Graph}+ " + w1 + "|";
		}
		if (associations[7] > 0) {
			bgRegEX += w0 + " \\p{Graph}+ \\p{Graph}+ " + w1 + "|";
		}
		if (associations[8] > 0) {
			bgRegEX += w0 + " \\p{Graph}+ \\p{Graph}+ \\p{Graph}+ " + w1 + "|";
		}
		if (associations[9] > 0) {
			bgRegEX += w0 + " \\p{Graph}+ \\p{Graph}+ \\p{Graph}+ \\p{Graph}+ " + w1 + "|";
		}
		
		if (bgRegEX.length() == 0) return bgRegEX;
		return bgRegEX.substring(0,bgRegEX.length()-1);
	}
	
	public boolean containsPOS() {
		return (w_0isPOS || w_1isPOS);
	}
	
	public String getW0() {
		return w_0;
	}
	
	public String getW1() {
		return w_1;
	}
	
	public String getLabel() {
		return label;
	}
	
	public int getFrequency() {
		return frequency;
	}
	
	public int getDocumentFrequency() {
		return documentFrequency;
	}
	
	public double getTfIdf(int numWords, int numDocuments) {
		if (documentFrequency == 0) return 0;
		return ((double)frequency/numWords) * ((double)numDocuments/documentFrequency);
	}
	
	public int getAssociation(int position, boolean index) {
		if (index) {
			return associations[position];
		}
		
		if (position >= 0) 
			return associations[(associations.length/2 - 1) + position];
		else 
			return associations[associations.length/2 + position];
	}
	
	/*public int [] getAssociations() {
		return associations;
	}*/
	
	public void clearAssociation(int position, boolean index) {
		if (index) {
			associations[position] = -1;
			return;
		}
		
		if (position >= 0) 
			associations[(associations.length/2 - 1) + position] = -1;
		else 
			associations[associations.length/2 + position] = -1;
	}
	
	/**
	 * Distance or Association can be populated. Not both!
	 * @return
	 */
	/*public int getDistance() {
		return distance;
	}
	
	public void setDistance(int d) {
		distance = d;
	}*/
	
	public double getStandardDeviation(double averageFrequency) {
		if (standardDeviation == -1) {
			double sum = 0;
			for (int index = 1; index < associations.length; index++) {
				 final double v = associations[index] - averageFrequency;
				 sum += v * v;
	        }
			standardDeviation = Math.sqrt( sum / ( associations.length - 1 ));
		}
		return standardDeviation;
	}
	
	public double getAverageAssociation() {
		if (averageAssociation == -1) {
			double sum = 0;
			
			// keep track of all frequencies
			for (int index = 1; index < associations.length; index++) {
				try {
					sum += associations[index];
				} catch (Exception e) {
					System.err.println("Error occurred in General.averageAssociations on adding sum at array[" 
							+ index + "] to sum (" + sum + ") for bigram " + bigram + ": " + e);
				}
			}
			
			averageAssociation = sum == 0 ? 0 : (double)sum/associations.length;
			
		}
		return averageAssociation;
	}
	
	public double getStrength(double averageFrequency) {
		if (strength == -1) {
			double k_i = (frequency - averageFrequency) == 0 || this.getStandardDeviation(averageFrequency) == 0 ?
					0 : (frequency - averageFrequency) / this.getStandardDeviation(averageFrequency);
			strength = k_i; 
		}
		return strength;
	}
	
	public double getSpread() {
		if (spread == -1) {
			
			double sum = 0;
			
			for (int index = 0; index < associations.length; index++) {
				double x = associations[index] - this.getAverageAssociation();
				sum += x*x;
			}
			spread = sum/associations.length;
		}
		return spread;
	}
	
	public void setSpread(double spreadIn) {
		spread = spreadIn;
	}
	
	public void setStrength(double strengthIn) {
		strength = strengthIn;
	}
	
	public void incrementDF() {
		documentFrequency++;
	}
		
	/*public void addFrequency(int frequencyIn) {
		frequency += frequencyIn;
	}
	
	public void addAssociation(int index, int associationIn) {
		associations[index] += associationIn;
	}*/
	
	public void increment(int position) {
		frequency++;
		
		if (position >= 0) 
			associations[(associations.length/2 - 1) + position]++;
		else 
			associations[associations.length/2 + position]++;
	}
	
	/*public void setDF(int df) {
		documentFrequency = df;
	}*/
	
	public int compareTo(XtractBigram bg, String type) {
		
		if (type.equals("ef")) {
			return Integer.valueOf(this.documentFrequency).compareTo(bg.getDocumentFrequency());
		}
		return Integer.valueOf(this.frequency).compareTo(bg.getFrequency());
	}
	
	public int compareTo(XtractBigram bg) {
		return ((Integer)this.frequency).compareTo(bg.getFrequency());
	}
	
	public int tfidfcompareTo(XtractBigram g, int numWords, int numDocuments) {
		return ((Double)this.getTfIdf(numWords,numDocuments)).compareTo(g.getTfIdf(numWords,numDocuments));
	}
	
	public String associationToString(int index, double averageFrequency) {
		return bigram + " " + (Math.abs(5-index)-1) + " " + this.getStrength(averageFrequency) + " " + this.getSpread();
	}
	
	public String toString(int numDocuments, int numWords) {
		String string = bigram + " " + frequency + " " + documentFrequency + " " + this.getTfIdf(numDocuments,numWords) + " ";
		
		for (int index = 0; index < associations.length; index++) {
			string += associations[index] + " ";
		}
		
		if (label != null) string += label;
		
		return string;
	}
}
