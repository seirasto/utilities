package collocation;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.*;

import org.apache.xerces.parsers.DOMParser;
import org.xml.sax.InputSource;
import org.w3c.dom.*;

import edu.stanford.nlp.trees.*;
import edu.stanford.nlp.parser.lexparser.LexicalizedParser;

/**
 * The program will first set up a directories with all the data 
 * containing POS and syntax relationships, unless it is already 
 * available, or the user indicates to ignore POS and syntax 
 * relationships
 * @author sara
 *
 */
public class Xtract {

	// thresholds
	private double k_0 = 1;
	private double k_1 = 1;
	private double U_0 = 10;
	private double T = .75;
    
	String parser = null;
	String outputDirectory;
	LexicalizedParser lexicalizedParser;
	
	// list of bigrams & grams
	Hashtable<String,XtractBigram> bigrams;
	Hashtable<String,XtractGram> grams;
	Hashtable<String,XtractBigram> filteredBigrams;
	Hashtable<String,XtractBigram> syntaxBigrams;
	
	//ArrayList<Sentence> corpora;
	int numDocuments = 0;
	int numWords = 0;
	
	Hashtable<String,Integer> nGrams;
	
	String comparer;
	String stage3labels = null;
	
	File _corpusFile = null;

	/**
	 * Setup - add documents one on at a time.
	 * Print all output to console
	 * @param taggerIn
	 */
	public Xtract(String parserIn) {
		this(parserIn,null,null,-1,-1,-1,-1);
	}
	
	/**
	 * Setup - add documents one at a time. Print all output to given directory
	 * @param taggerIn
	 * @param outputDirectoryIn
	 */
	public Xtract(String parserIn, String outputDirectoryIn, String stage3LabelsIn, double k_0In, double k_1In, double U_0In, double TIn) {
		try {
			_corpusFile = File.createTempFile("xtract-corpus", ".txt");
			_corpusFile.deleteOnExit();
			BufferedWriter corpusWriter = new BufferedWriter(new FileWriter(_corpusFile));
			corpusWriter.write("");
			corpusWriter.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		bigrams = new Hashtable<String,XtractBigram>();
		grams = new Hashtable<String,XtractGram>();
		syntaxBigrams = new Hashtable<String,XtractBigram>();
		//corpora = new ArrayList<Sentence>();
		outputDirectory = outputDirectoryIn;
		stage3labels = stage3LabelsIn;
		
		if (k_0In != -1) k_0 = k_0In;
		if (k_1In != -1) k_1 = k_1In;
		if (U_0In != -1) U_0 = U_0In;
		if (TIn != -1) T = TIn;

		
		try {
			parser = parserIn;
			
			if (parserIn != null) {
				// 04.26.12: Change for new stanfordparser.jar Needs to be checked.
				lexicalizedParser = LexicalizedParser.loadModel();
				//lexicalizedParser.setOptionFlags(new String[]{"-maxLength", "80", "-retainTmpSubcategories"});
			}
			
		} catch (Exception e) {
			System.err.println("Error occurred in Xtract constructor: " + e);
			e.printStackTrace();
		}
	}

	/**
	 * Read in a directory of files and process. Use POS and dependencies if parser is not null
	 * @param directoryIn
	 * @param parserIn
	 * @param outputDirectoryIn
	 * @param preProcessed
	 * @param stage3LabelsIn
	 * @param k_0In
	 * @param k_1In
	 * @param U_0In
	 * @param TIn
	 */
	public Xtract(String directoryIn, String parserIn, String outputDirectoryIn, boolean preProcessed, String stage3LabelsIn, double k_0In, double k_1In, double U_0In, double TIn) {
		this(parserIn, outputDirectoryIn, stage3LabelsIn, k_0In, k_1In, U_0In, TIn);


		if (!preProcessed) this.preprocessFiles(directoryIn, outputDirectoryIn);

		// 1. Read each file and process
		ArrayList<File> files = preProcessed ? this.getFiles(directoryIn) : this.getFiles(outputDirectoryIn);

		for (int index = 0; index < files.size(); index++) {
			this.addPreProcessedDocument(files.get(index).toString());
		}	
	}
	
	/**
	 * Get a list of all the files in the directory and subdirectories.
	 * @param directoryIn
	 * @return
	 */
	public ArrayList<File> getFiles(String directoryIn) {
		
		ArrayList<File> files = new ArrayList<File>();
		ArrayList<File> directories = new ArrayList<File>();
		
		directories.add(new File(directoryIn));
		while (directories.size() > 0) {
		
			if (directories.get(0).getName().equals("results")) {
				System.err.println("[Xtract.getFiles] skipping special file \"results\"");
			}
			else if (directories.get(0).isDirectory()) {
				directories.addAll(Arrays.asList(directories.get(0).listFiles()));
			}			
			else {
				files.add(directories.get(0));
			}
			directories.remove(0);
		}
		
		return files;
	}
	
	/**
	 * Preprocess all input documents by adding POS and Syntax Dependencies to them
	 * and storing them in xml format in an output folder
	 * 
	 * @param inputDirectory
	 * @param outputDirectory
	 */
	public void preprocessFiles(String inputDirectory, String outputDirectory) {
		
		ArrayList<File> files = this.getFiles(inputDirectory);
		
		for (int index = 0; index < files.size(); index++) {
			int id = 1;
			String output = "";
			
			String document = this.readFile(files.get(index));
			ArrayList<String> sentences = processing.StringProcessing.SentenceSplitter(document);
			
			for (int sentenceIndex = 0; sentenceIndex < sentences.size(); sentenceIndex++) {
				if (sentences.get(sentenceIndex).trim().length() == 0) continue;
				
				else {
					output += "<sentence id=\"" + id + "\">" + this.process(sentences.get(sentenceIndex), files.get(index).getName()).toXML() + "</sentence>";
					id++;
				}
			}
			
			//write to file
			output = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><document>" + output + "</document>";
			File outputFile = new File(outputDirectory + "/" + files.get(index).getName() + ".xml");
			
			try {
				new File(outputDirectory).mkdirs();
				BufferedWriter out = new BufferedWriter(new FileWriter(outputFile));
				out.write(output);
				out.close();
				System.err.println("[Xtract.preprocessFiles] completed preprocessing " + outputFile);
			} catch (Exception e) {
				System.err.println("[Xtract.preprocessFiles] error writing to file " + outputFile.toString() + ": " + e);
				e.printStackTrace();
			}
		}
	}
	
	private Sentence process(String s, String id) {
		
		if (s.trim().length() == 0) return null;
		
		// sentences greater than 80 will have trouble, so just truncate them.
		if (s.length() > 80) {
			int space = s.indexOf(" ",80);
			if (space >= 0 && space < 100)
				s = s.substring(0, s.indexOf(" ", 80));
			else if (space == -1 && s.length() > 100)  
				s = s.substring(0, 80);
		}
		
	    TreebankLanguagePack tlp = new PennTreebankLanguagePack();
	    GrammaticalStructureFactory gsf = tlp.grammaticalStructureFactory();
	
	    String dependenciesString = null;
	    String taggedSentence = null;
	    
	    try {
	    	Tree parse = (Tree) lexicalizedParser.parse(s);
	    	GrammaticalStructure gs = gsf.newGrammaticalStructure(parse);
		    
		    dependenciesString = gs.typedDependencies().toString();
		    taggedSentence = parse.taggedYield().toString(); //false);
		    
	    } catch (Exception e) {
	    	System.err.println("[Xtract.process] error parsing sentence: " + s);
	    	return null;
	    }
	    Pattern p = Pattern.compile("[a-z]*\\(\\p{Graph}*-[0-9]*, \\p{Graph}*-[0-9]*\\)");
    	Matcher m = p.matcher(dependenciesString);
    	
    	ArrayList<String> dependencies = new ArrayList<String>();
    	
    	while (m.find()) {
    		dependencies.add(m.group());
    	}
    	
	    return new Sentence(taggedSentence,dependencies, id);
	}
	
	/**
	 * Add a single document to the corpus
	 * @param document
	 */
	public void addDocument(String document, String file, boolean includePunctuation) {
		
		if (document == null) return;
		
		ArrayList<Sentence> sentences = this.getSentences(document,file, includePunctuation);
		
		HashSet<String> dfG = new HashSet<String>();
		HashSet<String> dfBG = new HashSet<String>();

		for (int index = 0; index < sentences.size(); index++) {
			this.computeFrequencyInformation(sentences.get(index),dfG,dfBG);
		}
			
		// add dfs
		this.updateDFs(dfG, dfBG);
	}
	
	public void addDocument(ArrayList<Sentence> sentences) {
		
		if (sentences == null) return;
		
		HashSet<String> dfG = new HashSet<String>();
		HashSet<String> dfBG = new HashSet<String>();
		
		try {
			BufferedWriter corpusWriter = new BufferedWriter(new FileWriter(_corpusFile,true));
	
			for (int index = 0; index < sentences.size(); index++) {
				//corpora.add(sentences.get(index));
				if (sentences.get(index) == null) continue;
				corpusWriter.write(sentences.get(index).toString() + "\n");
				this.computeFrequencyInformation(sentences.get(index),dfG,dfBG);
			}
			corpusWriter.close();
		} catch ( Exception e ) {
			e.printStackTrace();
		}
		// add dfs
		this.updateDFs(dfG, dfBG);
	}
	
	/**
	 * Read a preprocessed document into xtract
	 * @param file
	 */
	public void addPreProcessedDocument(String file) {
		DOMParser xmlParser = new DOMParser();
    	
		try {
			xmlParser.setFeature("http://xml.org/sax/features/namespaces", false);
			InputSource in = new InputSource(new FileReader(file));
			xmlParser.parse(in);
		} catch (Exception e) {
			System.err.println("Error occurred in Xtract.addPreProcessedDocument for" + file + ": " + e);
			e.printStackTrace();
			return;
		
		}
		
		Document document = xmlParser.getDocument();
		NodeList sentences = document.getElementsByTagName("sentence");
		
		HashSet<String> dfG = new HashSet<String>();
		HashSet<String> dfBG = new HashSet<String>();
		
		try {
			BufferedWriter corpusWriter = new BufferedWriter(new FileWriter(_corpusFile,true));
			
			for (int index = 0; index < sentences.getLength(); index++) {
				Sentence s = Sentence.processSentence((Element)sentences.item(index), new File(file).getName());
				corpusWriter.write(s.toString() + "\n");
				//corpora.add(s);
				this.computeFrequencyInformation(s,dfG,dfBG);
			}
			corpusWriter.close();
		} catch ( Exception e ) {
			e.printStackTrace();
		}
		// add dfs
		this.updateDFs(dfG, dfBG);
		
	}
	
	private void updateDFs(HashSet<String> dfGrams, HashSet<String> dfBigrams) {
		numDocuments++;
		Iterator<String> it = dfGrams.iterator();
		
		while (it.hasNext()) {
			grams.get(it.next()).incrementDF();
		}
		
		it = dfBigrams.iterator();
		
		while (it.hasNext()) {
			String bigram = ((String)it.next()).toLowerCase();
			bigrams.get(bigram).incrementDF();
		}
	}
	
	/**
	 * Read files to add. Used for Xtract constructor 
	 * that adds all documents in a directory
	 * @param document
	 * @return
	 */
	private String readFile(File document) {
		
		String documentText = "";
		
		try {
	        BufferedReader br = new BufferedReader(new InputStreamReader(
	        			new DataInputStream(new FileInputStream(document))));
	        
		    String strLine;
		    
		    while ((strLine = br.readLine()) != null)   {
		      documentText += strLine + " ";
		    }
		    br.close();
	    } catch (Exception e) {
	      System.err.println("Error: " + e.getMessage());
	    }
		
		return documentText;
	}
	
	/**
	 * Get an approximate list of all sentences for a file
	 * Step 1.1 (approx)
	 * @param document
	 * @return
	 */
	public ArrayList<Sentence> getSentences(String document, String file, boolean includePunctuation) {
		
		if (document == null) return null;
		
		ArrayList<Sentence> sentences = new ArrayList<Sentence>();
		
		String [] s = document.split("(;|\\?|!|\\.)( /|/)");

		try {
			BufferedWriter corpusWriter = new BufferedWriter(new FileWriter(_corpusFile,true));
			
			for (int index = 0; index < s.length; index++) {
				
				if (index + 1 < s.length && !s[index+1].trim().equals("")) {
					try {
						int end = s[index+1].indexOf(" ") == -1 ? s[index+1].length() : s[index+1].indexOf(" ");
						if (!s[index].trim().equals("")) s[index] = s[index] + "/" + s[index+1].substring(0,end);
						s[index+1] = s[index+1].replaceFirst("[A-Z$]* ", "");
					} catch (Exception e) {
						System.err.println("[Xtract.getSentences] " + s[index] + ", " + s[index+1] + ", " + e);
						e.printStackTrace();
						System.exit(0);
					}
				}
	
				String sentence = s[index].trim().toLowerCase() ;
				
				String text = sentence.replaceAll("/[a-zA-Z]*( |$)", " ");
				if (!includePunctuation) text = text.replaceAll("\\p{Punct}+", "");
				String pos = sentence.replaceAll("( |^)[a-zA-Z]*/", " ");
				if (!includePunctuation) pos = pos.replaceAll("\\p{Punct}+", "");
				Sentence newS = new Sentence(text,pos,null,file);
				sentences.add(newS);
				//corpora.add(new Sentence(text,pos,null,file));
				corpusWriter.write(newS.toString() + "\n");
			}
			corpusWriter.close();
		} catch ( Exception e ) {
				e.printStackTrace();
		}
		
		return sentences;
	}
	
	/**
	 * Run Xtract on the corpus and store all output
	 *
	 */
	public void run() {
		
		System.err.println("[Xtract.run] Document loading has completed at " + new Date());
		System.err.println("[Xtract.run] There are " + grams.size() + " tokens and " + bigrams.size() + " bigrams.");
		
		// Stage 1
		filteredBigrams = this.stage1();
		this.printStage(1,outputDirectory != null ? true : false);
		System.err.println("[Xtract.run] Stage 1 has completed at " + new Date()); 
		
		// Stage 2
		nGrams = this.stage2(filteredBigrams, false);
		this.printStage(2,outputDirectory != null ? true : false);
		System.err.println("[Xtract.run] Stage 2 has completed at " + new Date());
		
		//print stage 2 bigrams and labels
		/*if (outputDirectory == null) System.out.println(this.bigramsToString(syntaxBigrams));
		else {
			try {
				new File(outputDirectory + "/results/").mkdirs();
				BufferedWriter out = new BufferedWriter(new FileWriter(outputDirectory + "/results/stage2Bigrams-" + new SimpleDateFormat("yyyy-MM-dd").format(new Date()) + ".txt"));
				out.write(this.bigramsToString(bigrams));
				out.close();
			} catch (Exception e) {
				System.err.println("[Xtract.run] error writing bigrams: " + e);
				e.printStackTrace();
			}
		}*/
		
		nGrams = this.stage3();
		this.printStage(3,outputDirectory != null ? true : false);
		System.err.println("[Xtract.run] Stage 3 has completed at " + new Date());
		
		// print final bigrams and labels
		//if (outputDirectory == null) System.out.println(this.bigramsToString(syntaxBigrams));
		//else {
			try {
				new File(outputDirectory + "/results/").mkdirs();
				BufferedWriter out = new BufferedWriter(new FileWriter(outputDirectory + "/results/stage3Bigrams-" + new SimpleDateFormat("yyyy-MM-dd").format(new Date()) + ".txt"));
				this.bigramsToString(syntaxBigrams,out);
				out.close();
			} catch (Exception e) {
				System.err.println("[Xtract.run] error writing bigrams: " + e);
				e.printStackTrace();
			}
		//}
		_corpusFile.delete();
	}
	
	class BigramComparator implements Comparator<Object>{

		public int compare(Object t1, Object t2){
			return ((XtractBigram)t2).compareTo((XtractBigram)t1,comparer);
		}
	}
	
	class GramComparator implements Comparator<Object>{

		public int compare(Object t1, Object t2){
			return ((XtractGram)t2).compareTo((XtractGram)t1,comparer);
		}
	}
	
	class tfidfBigramComparator implements Comparator<Object>{

		public int compare(Object t1, Object t2){
			return ((XtractBigram)t2).tfidfcompareTo((XtractBigram)t1, numWords, numDocuments);
		}
	}
	
	class tfidfGramComparator implements Comparator<Object>{

		public int compare(Object t1, Object t2){
			return ((XtractGram)t2).tfidfcompareTo((XtractGram)t1, numWords, numDocuments);
		}
	}
	
	class NGramComparator implements Comparator<Object>{

		public int compare(Object t1, Object t2){
			return nGrams.get(t2).compareTo(nGrams.get(t1));
		}
	}	
	
	/**
	 * Get n sorted keys of syntax bigrams where n = quantity
	 * @param quantity
	 * @return
	 */
	public ArrayList<String> getSyntaxBigramKeys(int quantity, String comparerIn, String type) {
		if (syntaxBigrams.size() == 0) return null;
		ArrayList<XtractBigram> values = new ArrayList<XtractBigram>(syntaxBigrams.values());
		
		return this.getKeys(quantity, comparerIn, values, type);
	}
	
	public ArrayList<String> getSyntaxBigramKeys(int quantity, String comparerIn) {
		return this.getSyntaxBigramKeys(quantity, comparerIn, "all");
	}
	
	/**
	 * Get n sorted keys of bigrams where n = quantity
	 * @param quantity
	 * @return
	 */
	public ArrayList<String> getBigramKeys(int quantity, String comparerIn, String type) {
		if (bigrams.size() == 0) return null;
		ArrayList<XtractBigram> values = new ArrayList<XtractBigram>(bigrams.values());
		
		return this.getKeys(quantity, comparerIn, values, type);
	}
	
	public ArrayList<String> getBigramKeys(int quantity, String comparerIn) {
		return this.getBigramKeys(quantity, comparerIn, "all");
	}
	
	private ArrayList<String> getKeys(int quantity, String comparerIn, ArrayList<XtractBigram> values, String type) {
		comparer = comparerIn;
		if (comparer.equals("tfidf")) Collections.sort(values, new tfidfBigramComparator());  
		else Collections.sort(values, new BigramComparator());
		
		ArrayList<String> keys = new ArrayList<String>();
		
		for (int index = 0; index < values.size() && index < quantity; index++) {
			if ((values.get(index).containsPOS() && (type.equals("pos") || type.equals("all")))
					|| (!values.get(index).containsPOS() && (type.equals("text") || type.equals("all"))))
					keys.add(values.get(index).getBigram());			
		}
		return keys;
	}
	
	
	private Hashtable<String,String> getKeysRegExp(int quantity, String comparerIn, ArrayList<XtractBigram> values, String type) {
		
		comparer = comparerIn;
		if (comparer.equals("tfidf")) Collections.sort(values, new tfidfBigramComparator());  
		else Collections.sort(values, new BigramComparator());
		
		Hashtable<String,String> keys = new Hashtable<String,String>();
		
		for (int index = 0; index < values.size() && index < quantity; index++) {
			String regExp = "";
			if (type.equals("pos") && values.get(index).containsPOS())
				regExp = values.get(index).getBigramAsPOSRegularExpression();
			else if (type.equals("text") && !values.get(index).containsPOS())
				regExp = values.get(index).getBigramAsRegularExpression();
			else if (type.equals("all"))
				 regExp = values.get(index).containsPOS() ? values.get(index).getBigramAsPOSRegularExpression() : values.get(index).getBigramAsRegularExpression();
			else
				continue;
			keys.put(values.get(index).getBigram(),values.get(index).getFrequency() + "-" + regExp);			
		}
		return keys;
	}
	
	/**
	 * Get n sorted keys of syntax bigrams in regular expression form where n = quantity
	 * hash key: <bigram> hash value: <frequency>-<bigram regular expression>
	 * @param quantity
	 * @return
	 */
	public Hashtable<String,String> getSyntaxBigramKeysRegExp(int quantity, String comparerIn, String type) {
		if (syntaxBigrams.size() == 0) return null;
		ArrayList<XtractBigram> values = new ArrayList<XtractBigram>(syntaxBigrams.values());
		
		return this.getKeysRegExp(quantity, comparerIn, values, type);
	}
	
	public Hashtable<String,String> getSyntaxBigramKeysRegExp(int quantity, String comparerIn) {
		return this.getSyntaxBigramKeysRegExp(quantity, comparerIn, "all");
	}
	
	/**
	 * Get n sorted bigrams where n = quantity
	 * @param quantity
	 * @return
	 */
	public List<XtractBigram> getBigrams(int quantity, String comparerIn, String type) {
		return this.getBigrams(bigrams, quantity, comparerIn, type);
	}
	
	public List<XtractBigram> getSyntaxBigrams(int quantity, String comparerIn, String type) {
		return this.getBigrams(syntaxBigrams, quantity, comparerIn, type);
	}
	
	public List<XtractBigram> getBigrams(Hashtable<String, XtractBigram> bigramList, int quantity, String comparerIn, String type) {
		if (bigramList.size() == 0) return null;
		ArrayList<XtractBigram> values = new ArrayList<XtractBigram>(bigramList.values());
		
		comparer = comparerIn;
		if (comparer.equals("tfidf")) Collections.sort(values, new tfidfBigramComparator());  
		else Collections.sort(values, new BigramComparator());
		
		List<XtractBigram> sublist = new ArrayList<XtractBigram>();
		
		for (int index = 0; index < values.size() && (quantity == -1 || sublist.size() < quantity); index++) {
			
			if (type.equals("all") || (type.equals("pos") && values.get(index).containsPOS()) ||
					(type.equals("text") && !values.get(index).containsPOS())) {
				sublist.add(values.get(index));
			}
		}
		
		return sublist;
	}
	
	/**
	 * Get n sorted keys of bigrams in regular expression form where n = quantity
	 * hash key: <bigram> hash value: <frequency>-<bigram regular expression>
	 * @param quantity
	 * @return
	 */
	public Hashtable<String,String> getBigramKeysRegExp(int quantity, String comparerIn, String type) {
		if (bigrams.size() == 0) return null;
		ArrayList<XtractBigram> values = new ArrayList<XtractBigram>(bigrams.values());
		
		return this.getKeysRegExp(quantity, comparerIn, values, type);
	}
	
	public Hashtable<String,String> getBigramKeysRegExp(int quantity, String comparerIn) {
		return this.getBigramKeysRegExp(quantity, comparerIn, "all"); 
	}
	
	/**
	 * Get n sorted ngrams where n = quantity
	 * @param quantity
	 * @return
	 */
	public List<String> getNgrams(int quantity) {
		if (nGrams.size() == 0) return null;
		ArrayList<String> values = new ArrayList<String>(nGrams.keySet());
		Collections.sort(values, new NGramComparator());
		return quantity < values.size() ? values.subList(0, quantity) : values;
	}
	
	/**
	 * Get n sorted grams where n = quantity
	 * @param quantity
	 * @return
	 */
	public List<XtractGram> getGrams(int quantity, String comparerIn) {
		if (grams.size() == 0) return null;
		ArrayList<XtractGram> values = new ArrayList<XtractGram>(grams.values());
		
		comparer = comparerIn;
		if (comparer.equals("tfidf")) Collections.sort(values, new tfidfGramComparator());  
		else Collections.sort(values, new GramComparator());
		
		for (int index = 0; index < values.size() - 100; index+= 100) {
			if (values.get(index).getFrequency() < 50) {
				System.err.println("[Xtract.getGrams] Approximately " + index + " words are repeated more than 50 times.");
				break;
			}
		}
		
		return quantity < values.size() && quantity > -1 ? values.subList(0, quantity) : values;
	}
	
	/**
	 * Get n sorted keys of grams where n = quantity
	 * @param quantity
	 * @return
	 */
	public ArrayList<String> getGramKeys(int quantity, String comparerIn) {
		if (grams.size() == 0) return null;
		ArrayList<XtractGram> values = new ArrayList<XtractGram>(grams.values());
		
		comparer = comparerIn;
		if (comparer.equals("tfidf")) Collections.sort(values, new tfidfGramComparator());  
		else Collections.sort(values, new GramComparator());
		
		ArrayList<String> keys = new ArrayList<String>();
		
		for (int index = 0; index < values.size() && index < quantity; index++) {
			keys.add(values.get(index).getWord());
		}
		return keys;
	}
	
	public Hashtable<String,Integer> getGramsQuantities(int quantity, String comparerIn) {
		if (grams.size() == 0) return null;
		ArrayList<XtractGram> values = new ArrayList<XtractGram>(grams.values());
		
		comparer = comparerIn;
		if (comparer.equals("tfidf")) Collections.sort(values, new tfidfGramComparator());  
		else Collections.sort(values, new GramComparator());
		
		Hashtable<String,Integer> keys = new Hashtable<String,Integer>();
		
		for (int index = 0; index < values.size() && (quantity == -1 || index < quantity); index++) {
			keys.put(values.get(index).getWord(),values.get(index).getFrequency());
		}
		
		for (int index = 0; index < values.size() - 100; index+= 100) {
			if (values.get(index).getFrequency() < 50) {
				System.err.println("[Xtract.getGrams] Approximately " + index + " words are repeated more than 50 times.");
				break;
			}
		}
		return keys;
	}
	
	/**
	 * Print the stage to console or file
	 * @param stage
	 * @param file
	 */
	private void printStage(int stage, boolean file) {
		try {
			new File(outputDirectory + "/results/").mkdirs();
			BufferedWriter out = new BufferedWriter(new FileWriter(outputDirectory + "/results/stage" + stage + "-" + new SimpleDateFormat("yyyy-MM-dd").format(new Date()) + ".txt"));
			
			switch (stage) {
				case 1:
					this.stage1OutputToString(out);
					//else System.out.println(this.stage1OutputToString());
					break;
				case 2:
					this.stage2OutputToString(out);
					//else System.out.println(this.stage2OutputToString());
					break;
				case 3:
					this.stage2OutputToString(out);
					//else System.out.println(this.stage2OutputToString());
					break;
			}
			out.close();
		} catch(Exception e) {
			System.err.println("[Xtract.run] error writing stage two output to file " + outputDirectory + ": " + e);
			e.printStackTrace();
		}
	}
	
	/**
	 * Create bigrams and add frequency and association
	 * information into the full list
	 * Used for multiple stages
	 * @param sentence
	 * @param dfGram gram document frequency
	 * @param dfBigram bigram document frequency
	 */
	public void computeFrequencyInformation(Sentence sentence, HashSet<String> dfGram, HashSet<String> dfBigram) {
		
		for (int index = 0; index < sentence.length(); index++) {
					
			String [] w_0 = new String[2];
			
			w_0[0] = sentence.getWord(index).toLowerCase();
			
			if (w_0[0].matches("\\p{Punct}++")) continue;
			
			try {
				w_0[1] = sentence.getTruncatedPOS(index);
			} catch (Exception e) {
				System.err.println("[Xtract.computeFrequencyInformation] w_0 missing POS: " + w_0[0]);
			}
			
			// if it isn't all punctuation, remove the punctuation. so :) is good but can't will be changed to cant
			if (!w_0[0].matches("[\\p{Punct}0-9]+")) w_0[0] = w_0[0].replaceAll("\\p{Punct}+","");
			
			XtractGram gram = grams.containsKey(w_0[0]) ? grams.get(w_0[0]) : new XtractGram(w_0[0]);
			gram.increment();
			dfGram.add(w_0[0]);
			numWords++;
			
			// bigram
			for (int position = index +1; position < sentence.length() && position <= index + 5; position++) {
				
				String [] w_1 = new String[2];
				
				w_1[0] = sentence.getWord(position).toLowerCase();
				
				if (w_1[0].matches("\\p{Punct}++")) continue;
				
				try {
					w_1[1] = sentence.getTruncatedPOS(position);
				} catch (Exception e) {	
					System.err.println("[Xtract.computeFrequencyInformation] w_1 missing POS: " + sentence.getWord(position));					
					continue;
				}
				
				if (!w_1[0].matches("[\\p{Punct}0-9]+")) w_1[0] = w_1[0].replaceAll("\\p{Punct}+","");
								
				try {
					// only store in one: "w_0 w_1" or "w_1 w_0"
					// gram is after
					if (bigrams.containsKey(w_1[0] + " " + w_0[0])) {
						this.newBigram(w_1[0], w_0[0], false, false, index-position);
						gram.increment(index-position);
						dfBigram.add(w_1[0] + " " + w_0[0]);
						//if (!bigrams.containsKey(bigram)) gram.addBigram();
					}
					// gram is before
					else {
						this.newBigram(w_0[0], w_1[0], false, false, position-index);
						gram.increment(position-index);
						dfBigram.add(w_0[0] + " " + w_1[0]);
					}
					
					if (w_0[1] != null && w_1[1] != null) {
						this.newBigram(w_0[0], w_1[1].toUpperCase(), false, true, position-index);
						dfBigram.add(w_0[0] + " " + w_1[1]);
						this.newBigram(w_1[0], w_0[1].toUpperCase(), false, true, index-position);
						dfBigram.add(w_1[0] + " " + w_0[1]);
					}
				} catch (Exception e) {
					System.err.println("[Xtract.computeFrequencyInformation] w_0: " + sentence.getWord(index) + ", w_1: " + sentence.getWord(position));
					e.printStackTrace();
				}
				gram.addBigram(w_1[0]);
			}
			grams.put(w_0[0], gram);
			
		}
	}
	
	/**
	 * Create a new bigram from the two words.
	 * @param w_0
	 * @param w_1
	 * @param position
	 */
	private void newBigram(String w_0, String w_1, boolean w_0isPOS, boolean w_1isPOS, int position) {
		
		w_0 = w_0.toLowerCase();
		w_1 = w_1.toLowerCase();
		
		String bigram = w_0 + " " + w_1;
		XtractBigram bg = bigrams.containsKey(bigram) ? bigrams.get(bigram) : new XtractBigram(w_0,w_1,w_0isPOS,w_1isPOS);
		bg.increment(position);
		bigrams.put(bigram, bg);
	}
	
	/**
	 * Print out all the information 
	 */
	public String toString() {
		String report = "";

		report += "----- Bigrams -----\n\n";
		//report += this.bigramsToString(bigrams);
		
		// print filtered bigrams (output of stage 1)
		report += "----- Stage One -----\n\n";
		//report += this.stage1OutputToString();
		
		// print ngrams (output of stage 2/3)
		report += "----- Stage Two -----\n\n";
		//report += this.stage2OutputToString();
		
		return report;
	}
	
	public void bigramsToString(Hashtable<String,XtractBigram> bgs, BufferedWriter out) throws Exception {
		// print all bigrams		
		out.write("w_i w_j freq df tfidf p-5 p-4 p-3 p-2 p-1 p_1 p_2 p_3 p_4 p_5 label\n");
		
		Iterator<String> it = bgs.keySet().iterator();
		
		while (it.hasNext()) {
			XtractBigram bigram = bgs.get((String)it.next());
			
			out.write(bigram.toString(numDocuments,numWords) + "\n");
		}
	}
	
	/**
	 * print out from stage 1
	 * @return
	 */
	public void stage1OutputToString(BufferedWriter out) throws Exception {
		//String report = "";
		
		Iterator<String> it = filteredBigrams.keySet().iterator();
		
		if (filteredBigrams.size() > 0)
			out.write("w_i,w_j,distance,strength,spread\n");
		
		int count = 0;
		int size = filteredBigrams.size();
		
		while (it.hasNext()) {
			if (count % 500 == 0) System.out.println("[Xtract.stage1] " + count + "/" + size);
			XtractBigram bigram = filteredBigrams.get(it.next());
			
			double strength = bigram.getStrength(-1);
			double spread = bigram.getSpread();
			
			for (int index = -5; index <= 5; index++) {
				if (bigram.getAssociation(index, false) == -1) continue;
				out.write(bigram.getW0() + "," + bigram.getW1() + "," + index + "," + strength + "," + spread + "\n"); 
			}
			count++;
		}
		
		//return report;
	}

	/**
	 * print output from stage 2
	 * @return
	 */
	public void stage2OutputToString(BufferedWriter out) throws Exception {
		//String report = "";
		
		if (nGrams == null || nGrams.size() == 0) return;
		
		Iterator<String> it = nGrams.keySet().iterator();
		
		while (it.hasNext()) {
			String ngram = it.next();
			out.write(ngram + "\n");
		}
		
		//return report;
	}
	
	/**
	 * Stage One of Xtract - Filter the bigrams to get the best ones
	 * @return
	 */
	public Hashtable<String,XtractBigram> stage1() {
		Hashtable<String,XtractBigram> bigramsStage1 = new Hashtable<String,XtractBigram>();
		Iterator<String> it = bigrams.keySet().iterator();

		while (it.hasNext()) {
			XtractBigram bigram = bigrams.get((String)it.next());
	
			try {

				double averageFrequency = grams.get(bigram.getW0()).getAverageFrequency();

				// C_1
				if (bigram.getStrength(averageFrequency) < k_0) continue;
				
				double U_i = bigram.getSpread();
				
				// C_2
				if (U_i < U_0) continue;
				
				double c_3 = bigram.getAverageAssociation() + (k_1 * Math.sqrt(U_i));
				

				// C_3
				XtractBigram newBigram = bigram;
				boolean save = false;
				
				for (int index = 0; index < 10; index++) {
					
					// store all information pertaining to this bigram in file and update bigram to include only good bigram positions
					if (newBigram.getAssociation(index,true) >= c_3) {
						save = true;
					}
					else {
						newBigram.clearAssociation(index, true); 
					}
				}
				if (save) bigramsStage1.put(newBigram.getBigram(), newBigram);
			} catch (Exception e) {
				System.err.println("Error occurred in Xtract.filterBigrams on bigram " + bigram + ": " + e);
				e.printStackTrace();
			}
		}
		filteredBigrams = bigramsStage1;
		return bigramsStage1;
	}
	
	/**
	 * Run Stage 2 of Xtract - produce n-grams from relevant bigrams
	 * @param frequencies
	 */
	public Hashtable<String,Integer> stage2(Hashtable<String,XtractBigram> bigramsIn, boolean dependencies) {
		
		Hashtable<String,Integer> ngrams = new Hashtable<String,Integer>();
		
		try {
		BufferedReader corpusReader = new BufferedReader(new FileReader(_corpusFile));
		
		int index = 0;
		String text = null;
		
		while ((text = corpusReader.readLine()) != null) {
//		for (int index = 0; index < corpora.size(); index++) {
			//if (index % 500 == 0) System.out.println("[Xtract.stage2] " + index + "/" + corpora.size());
			Sentence sentence = Sentence.retrieveSentence(text); //corpora.get(index);
			
			if (sentence == null || sentence.getText() == null) {
				System.err.print("Error processing sentence: " + index + ", ");
				System.err.println("null sentence: " + sentence);
				continue;
			}
			if (sentence.getText().trim().length() == 0) continue;
					    			
			for (int i = 0; i < sentence.length(); i++) {
				
				String w = sentence.getWord(i).toLowerCase();
				if (w.matches("\\p{Punct}++")) continue;
				boolean print = false;
				String phrase = "";
				
				for (int j = i-5; j < sentence.length() && j < i+5; j++) {
					
					if (j < 0) j = 0;
					
					if (i==j) {
						phrase += w + " ";
						continue;
					}

					String pos = sentence.getTruncatedPOS(j);
					String w_1 = sentence.getWord(j).toLowerCase();
					
					if (w_1.matches("\\p{Punct}++")) continue;
					
					Sentence.Dependency d = sentence.getDependency(w,w_1);
					String label = d != null ? d.getRelationship() : null;
					
					// try adding bigram
					if (this.addWord(bigramsIn, w, w_1, i, j,label, dependencies)) {
						phrase += w_1 + " ";
						//if (label == null) || label.matches("csubj|nn|dobj|amod")) print = true;
						print = true;
					}
					// try adding pos
					else if (pos != null
							&& this.addWord(bigramsIn, w, pos.toLowerCase(), i, j, label, false)) {
						phrase += pos.toUpperCase() + " ";
					}
					else if (!phrase.isEmpty()) phrase += "\\p{Graph}+ ";
				}
				
				if (print) {
					String [] ngram = phrase.split(" ");
					
					boolean hasGram = false;
					phrase = "";
					
					for (int n = ngram.length-1; n >= 0; n--) {
						if (!ngram[n].equals("\\p{Graph}+")) hasGram = true;
						
						if (hasGram) {
							phrase = ngram[n] + " " + phrase;
						}
					}
					
					ngrams.put(phrase,ngrams.containsKey(phrase) ? ngrams.get(phrase) + 1 : 1);
				}
			}
			index++;
		}
		corpusReader.close();
		} catch ( Exception e ) {
			e.printStackTrace();
		}
		return ngrams;
	}
		
	/**
	 * run stage 3 of xtract (stage 2 + syntax dependencies)
	 * @return
	 */
	public Hashtable<String,Integer> stage3() {

		String id = ""; //corpora.get(0).getId(); 
		HashSet<String> dfbgs = new HashSet<String>();
		
		try {
		BufferedReader corpusReader = new BufferedReader(new FileReader(_corpusFile));
		
		int index = 0;
		String text = null;
		
		while ((text = corpusReader.readLine()) != null) {
		//for (int index = 0; index < sentences.size(); /*corpora.size();*/ index++) {
			//if (index % 500 == 0) System.out.println("[Xtract.stage3] " + index + "/" + corpora.size());
			//Sentence sentence = sentences.get(index); //corpora.get(index);
			
			Sentence sentence = Sentence.retrieveSentence(text);
			
			if (sentence == null || sentence.getText() == null) {
				System.err.print("Error processing sentence: " + index + ", ");
				System.err.println("null sentence: " + sentence);
				continue;
			}
			if (sentence.getText().trim().length() == 0) continue;
			
			String newId = sentence.getId();

			// increase df once per document
			if (!newId.equals(id)) {
				// increase df for each bigram
				Iterator<String> it = dfbgs.iterator();
				
				while(it.hasNext()) {
					String b = (String)it.next();
					syntaxBigrams.get(b).incrementDF();
				}
				
				// reset hash and update id
				dfbgs = new HashSet<String>();
				id = newId;
			}
			
			if (sentence.getText().trim().length() == 0) continue;
		   
			Hashtable<String, Sentence.Dependency> dependencies = sentence.getDependencies();
			
			Iterator<String> it = dependencies.keySet().iterator();
			
		    while (it.hasNext()) {
		    	Sentence.Dependency dependency = dependencies.get(it.next());
		    	int distance0 = dependency.getWordPosition(0) - dependency.getWordPosition(1);
		    	
		    	// make sure it is at most 5 away
		    	if (Math.abs(distance0) > 5) continue;
		    	
		    	String label = dependency.getRelationship();
		    	int distance1 = dependency.getWordPosition(1) - dependency.getWordPosition(0);

		    	// csubj = SV, dobj = VO, nn = NN, amod = JN ex: "csubj|nn|dobj|amod"
		    	if (stage3labels != null && !label.matches(stage3labels)) continue;

		    	String w0 = dependency.getWord(0).toLowerCase();
		    	String w1 = dependency.getWord(1).toLowerCase();

		    	String bg = this.addBG(w0, w1, false, false, w0 + " " + w1, w1 + " " + w0, label, distance0, distance1);
		    	if (bg != null) dfbgs.add(bg);
		    	
		    	try {
			    	String pos0 = sentence.getPOS(dependency.getWordPosition(0)-1).toLowerCase();
			    	String pos1 = sentence.getPOS(dependency.getWordPosition(1)-1).toLowerCase();
			    	
			    	bg = this.addBG(pos0, w1, true, false, pos0 + " " + w1, w1 + " " + pos0, label, distance0, distance1);
			    	if (bg != null) dfbgs.add(bg);
			    	bg = this.addBG(pos1, w0, true, false, pos1 + " " + w0, w0 + " " + pos1, label, distance0, distance1);
			    	if (bg != null) dfbgs.add(bg);
		    	} catch (Exception e) {
		    		System.err.println("[Xtract.stage3] sentence: " + sentence.getText() + " pos: " + sentence.getSentencePOS() + " dependency: " + dependency.toString() + ": " + e);
		    		e.printStackTrace();
		    	}
		    }
		    index++;
		}
		corpusReader.close();
		} catch( Exception e) {
			e.printStackTrace();
		}
		
		// make n-grams
		return this.stage2(filteredBigrams,true);
	}
	
	private String addBG(String w0, String w1, boolean w0POS, boolean w1POS, String bigram0,
			String bigram1, String label, int distance0, int distance1) {
		
    	// make sure it's an important bigram
    	if(!filteredBigrams.containsKey(bigram0) && !filteredBigrams.containsKey(bigram1))  return null;
		
    	XtractBigram bg = null;

    	if (syntaxBigrams.containsKey(bigram1 + " " + label)) {
    		bg = syntaxBigrams.get(bigram1 + " " + label);
    		bg.increment(distance0);
    		syntaxBigrams.put(bigram1 + " " + label, bg);
    		return bigram1 + " " + label;
    	}
    	else {
    		if (syntaxBigrams.containsKey(bigram0 + " " + label)) {
	    		bg = syntaxBigrams.get(bigram0 + " " + label);
    		}
    		else {
    			try {
	    			bg = bigrams.containsKey(bigram0) ?
	    				new XtractBigram(w0, w1, w0POS, w1POS, label)
	    				: new XtractBigram(w1, w0, w1POS, w0POS, label);
    			} catch (Exception e) {
    				System.err.println("[Xtract.stage3] Error adding bigram " + bg.getBigram() + ": " + e);
    				e.printStackTrace();
    			}
    		}
    		bg.increment(distance1);
    		syntaxBigrams.put(bg.getBigram() + " " + label, bg);
    		return bg.getBigram() + " " + label;
    	}
	}
	
	/**
	 * Check if the word should be added to the n-gram based on the stage 2 (& 3) criteria
	 * @param bigramsIn
	 * @param w_0
	 * @param w_1
	 * @param w_0Position
	 * @param w_1Position
	 * @return
	 */
	private boolean addWord(Hashtable<String,XtractBigram> bigramsIn, String w_0, String w_1, 
			int w_0Position, int w_1Position, String label, boolean stage3) {
		
		double syntaxAssociation = 0;
		double bigramAssociation = 0;
		double frequency;
		
		// stage 2 setup
		XtractBigram bigram = bigramsIn.get(w_0 + " " + w_1);
		
		if (bigram == null) {
			bigram = bigramsIn.get(w_1 + " " + w_0);
			if (bigram == null) return false;
			bigramAssociation = bigram.getAssociation(w_0Position - w_1Position, false);
			frequency = bigram.getAssociation(w_0Position - w_1Position, false);
		}
		else {
			bigramAssociation = bigram.getAssociation(w_1Position - w_0Position, false);
			frequency = bigram.getFrequency();//.getAssociation(w_1Position - w_0Position, false);
		}
		
		// stage 3 setup
		if (stage3) {
			XtractBigram syntaxBigram = syntaxBigrams.get(w_0 + " " + w_1 + " " + label);
			
			if (syntaxBigram == null) {
				syntaxBigram = syntaxBigrams.get(w_1 + " " + w_0 + " " + label);
				if (syntaxBigram == null) return false;
				syntaxAssociation = syntaxBigram.getAssociation(w_0Position - w_1Position, false);
			}
			else {
				syntaxAssociation = syntaxBigram.getAssociation(w_1Position - w_0Position, false);
			}
		}
		
		// this position is not used
		if (frequency == -1) {
			return false;
		}
		
		try {
		
			// try to add word - stage 2 check
			if (((w_0Position > w_1Position) && bigramAssociation/grams.get(w_0).getAssociation(w_0Position - w_1Position, false) > T) ||
					((w_0Position < w_1Position) && bigramAssociation/grams.get(w_0).getAssociation(w_1Position - w_0Position, false) > T)) {

				// stage 3 check			
				if (!stage3 || syntaxAssociation/bigramAssociation > T) {
					return true;
				}
				
				return false;
			}
		} catch (Exception e) {
			System.err.println("[Xtract.addWord] error in checking if \"" + bigram.getBigram() + "\" bigram can be added: " + e);
			e.printStackTrace();			
		}
		return false;
	}
	
	/**
	 * Search the corpus for the regular expression phrase
	 * @param phrase
	 * @return
	 */
	/*public ArrayList<String> searchCorpus(String phrase) {
		
		ArrayList<String> phrases = new ArrayList<String>();
		
		for (int index = 0; index < corpora.size(); index++) {
			//if (corpora.get(index).getText().indexOf("executive") >= 0
			//		&& corpora.get(index).getText().indexOf("president") >= 0) 
			//	System.out.println(corpora.get(index).getText());
			Pattern p = Pattern.compile("(^| )" + phrase + "($| )");
			Matcher m = p.matcher(corpora.get(index).getText());

			if (m.find())
				phrases.add(corpora.get(index).getText());
		}
		return phrases;
	}*/
		
	/**
	 * run xtract from cmd line
	 */	
	public static void main(String[] args) {
		
			String cmd = "java Xtract -i \"input direcory\" -o \"output directory\" (-p \"parser directory\" -l \"labels\" -pp -k0 \"value\" -k1 \"value\" -u0 \"value\" -t \"value\")" + 
						 "\n-l = regular expression of labels eg: csubj|nn|dobj|amod, default is all labels\n-pp = preprocessed is true";
			
			String inputDirectory = "";
			String parser = "/proj/nlp/users/sara/java/input/englishPCFG.ser.gz";
			String outputDirectory = "";
			boolean preProcessed = false;
			String labels = null;
			double k_0 = -1;
			double k_1 = -1;
			double U_0 = -1;
			double T = -1;
			int i = 0;
			
			/*Xtract xtract = new Xtract("/proj/nlp/users/sara/corpora/blogs/livejournal/subcorpus/1940-1968/", parser, outputDirectory, true, labels, k_0, k_1, U_0, T);
			ArrayList<String> results = xtract.searchCorpus("because \\p{Graph}+ \\p{Graph}+ run");
			for (int index = 0 ; index < results.size(); index++) {
				System.out.println(index + ". " + results.get(index));
			}
			System.exit(0);*/
			
			while (i < args.length) {
				if (args[i].equals("-i")) {
					inputDirectory = args[++i];
				}
				else if (args[i].equals("-o")) {
					outputDirectory = args[++i];
				}
				else if (args[i].equals("-p")) {
					parser = args[++i];
				}
				else if (args[i].equals("-pp")) {
					preProcessed = true;
				}
				else if (args[i].equals("-h")) {
					System.out.println(cmd);
					System.exit(0);
				}
				else if (args[i].equals("-l")) {
					labels = args[++i];
				}
				else if (args[i].equals("-k0")) {
					k_0 = Double.valueOf(args[++i]);
				}
				else if (args[i].equals("-k1")) {
					k_1 = Double.valueOf(args[++i]);
				}
				else if (args[i].equals("-u0")) {
					U_0 = Double.valueOf(args[++i]);
				}
				else if (args[i].equals("-t")) {
					T = Double.valueOf(args[++i]);
				}
				i++;
			}

			if (inputDirectory.equals("") || outputDirectory.equals("")) {
				System.out.println("Missing required option input/output directory");
				System.out.println(cmd);
				System.exit(0);
			}

			Xtract xtract = new Xtract(inputDirectory, parser, outputDirectory, preProcessed, labels, k_0, k_1, U_0, T);
			xtract.run();
		}
}
