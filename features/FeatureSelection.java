/**
 * 
 */
package features;

import java.util.*;

import processing.NlpUtils;

/**
 * @author sara
 *
 */
public class FeatureSelection {

	public static Hashtable<String, Hashtable<String, Double>> tf_idf(Hashtable<String, Hashtable<String, Integer>> tf, Hashtable<String, Hashtable<String, Integer>> df) {
		
		Hashtable<String, Hashtable<String, Double>> tfidf = new Hashtable<String, Hashtable<String, Double>>();
		
		Iterator<String> documentsIterator = tf.keySet().iterator();
		
		// set up tfidf hash
		while (documentsIterator.hasNext()) {
			String document = (String)documentsIterator.next();
			tfidf.put(document, new Hashtable<String,Double>());
		}
		
		documentsIterator = tf.keySet().iterator();
		
		//compute tfidf
		while (documentsIterator.hasNext()) {
			Object document = documentsIterator.next();
			Hashtable<String,Integer> documentTF = tf.get(document);
			
			Iterator<String> iterator = documentTF.keySet().iterator();
			
			while (iterator.hasNext()) {
				String term = (String)iterator.next();
				
				tfidf.get(document).put(term, (double)tf.get(document).get(term) * (1/df.get(document).get(term)));
				
			}
		}
		
		return tfidf;
	}
	
	public static HashMap<String,List<String>> chiSquare(HashMap<String,List<String>> sentences, int ngram, int size, double threshold) {
		return chiSquare(sentences, ngram, size, threshold, false);
	}
	
	public static HashMap<String,List<String>> chiSquare(HashMap<String,List<String>> sentences, int ngram, int size, double threshold, boolean seperateClasses) {
		
		HashMap<String,HashMap<String,Double>> wordFrequencies = computeWordFrequencies(sentences, ngram);
		HashMap<String,HashMap<String,Double>> chiSquareFrequencies = new HashMap<String,HashMap<String,Double>>();
		
		for (String cl : sentences.keySet()) {
			chiSquareFrequencies.put(cl, new HashMap<String,Double>());
		}
		
		double n = 0; 
		// per class
		HashMap<String,Integer> wordCount = new HashMap<String,Integer>();
		
		String [] classes = new String [wordFrequencies.keySet().size()];
		
		int in = 0;
		for (String c : wordFrequencies.keySet()) {
			n += sentences.get(c).size();
			wordCount.put(c, wordFrequencies.get(c).size());
			classes[in] = c;
			in++;
		}
		
		for (int i = 0; i < classes.length; i++) {
			for (String word : wordFrequencies.get(classes[i]).keySet()) {
					
				// class and word
				double n11 = wordFrequencies.get(classes[i]).get(word);
				// class and not word
				double n10 = sentences.get(classes[i]).size() - n11;
				// not class and word
				double n01 = count(wordFrequencies,classes[i],word);
				// not class and not word
				double n00 = (n-sentences.get(classes[i]).size()) - n01;
				
				double chi2 = (n * (((n11*n00) - (n10*n01) - .5 ) * ((n11*n00) - (n10*n01) - .5))) / 
						((n11+n01) * (n11+n10) * (n10+n00) * (n01+n00));
				/*if (word.equals("?") || word.equals("is") || word.equals("email") || word.equals(".") || word.indexOf("comment") >= 0) {
					System.out.println(word + "(" + c + "): " + chi2 + "( " + n11 + "," + n10 + "," + n01 + "," + n00 + ")");
				}*/

				if (chi2 >= threshold) {
					if (wordFrequencies.keySet().size() == 2 && seperateClasses) {
						if (n11/(n11+n10) > n01/(n01+n00)) chiSquareFrequencies.get(classes[i]).put(word, chi2);
						else chiSquareFrequencies.get(i == 0 ? classes[1] : classes[0]).put(word, chi2);
					}
					else {
						chiSquareFrequencies.get(classes[i]).put(word, chi2);
				
					}
				}
			}
		}
		
		HashMap<String,List<String>> topWords = new HashMap<String,List<String>>();
		
		for (String c : chiSquareFrequencies.keySet()) {
			
			final HashMap<String,Double> frequencies = chiSquareFrequencies.get(c);
			
			Comparator<String> chiSquareComparator 
			= new Comparator<String>() {

				public int compare(String w1,String w2) {
			
					Double value1 = frequencies.get(w1);
					Double value2 = frequencies.get(w2);
					
					//descending order
					return value2.compareTo(value1);
				}
			};
			
			List<String> words = new ArrayList<String>(frequencies.keySet());
			
			Collections.sort(words, chiSquareComparator);
			topWords.put(c, words.size() < size ? words : new ArrayList<String>(words.subList(0, size)));
		}
		return topWords;
	}
	
	
	
	private static int count(HashMap<String,HashMap<String,Double>> wordFrequencies, String exclude, String word) {
		int count = 0;
		
		for (String cl : wordFrequencies.keySet()) {
			if (cl.equals(exclude) || !wordFrequencies.get(cl).containsKey(word)) continue;
			count += wordFrequencies.get(cl).get(word);
		}
		return count;
	}

	private static HashMap<String,HashMap<String,Double>> computeWordFrequencies(HashMap<String,List<String>> sentences, int ngram) {
		HashMap<String,HashMap<String,Double>> featureWords = new HashMap<String,HashMap<String,Double>>();
		
		for (String cl : sentences.keySet()) {
			featureWords.put(cl, new HashMap<String,Double>());
		}
		
		for (String cl : sentences.keySet()) {
			for (String sentence : sentences.get(cl)) {
				List<String> words = uniqueWords(NlpUtils.getWords(sentence.toLowerCase()),ngram);
				
				for (String word : words) {
					featureWords.get(cl).put(word, 
							wordCount(featureWords.get(cl), word));
				}
			}
		}
		return featureWords;
	}
	
	private static List<String> uniqueWords(List<String> words, int ngram) {
		HashSet<String> unique = new HashSet<String>();
		
		for (int index = 0; index < words.size(); index++) {
			if (ngram >= 1) unique.add(words.get(index));
			if (ngram >= 2 && index +1 < words.size()) unique.add(words.get(index) + " " + words.get(index+1));
			if (ngram >= 3 && index+2 < words.size()) unique.add(words.get(index) + " " + words.get(index+1) + " " + words.get(index+2));
		}
		return new ArrayList<String>(unique);
	}
		
	private static double wordCount(HashMap<String,Double> words, String word) {
		double count = 0;
		if (words.containsKey(word))
			count = words.get(word);
		return count+1; 
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
